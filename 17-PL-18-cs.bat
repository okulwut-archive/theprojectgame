@echo off
set sciptPath=%~dp0
set "verbose="
set "conf="
:initial
if "%1"=="--port" goto portValue
if "%1"=="--conf" goto confValue
if "%1"=="--verbose" goto verboseValue
if "%1"=="" goto done
goto error

:portValue
shift
set port=%1
shift
goto initial

:confValue
shift
set conf=%1
shift
goto initial

:verboseValue
set verbose=true
shift
goto initial

:error
echo %0 usage error
echo 17-PL-18-cs --port [port] --conf [configuration file path] --verbose (optional)
goto eof

:done
set conf=%conf:"=%
(echo %conf%|findstr /R "^[A-Z]:.*$" || set conf=%scriptPath%%conf%)>NUL
start "" "%sciptPath%ProjectGame\ProjectGame.CommunicationServer\bin\Release\ProjectGame.CommunicationServer.exe" port=%port%^|conf=%conf%^|verbose=%verbose%

:eof