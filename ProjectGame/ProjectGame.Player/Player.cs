﻿using System;
using System.Threading;
using ProjectGame.Common;
using ProjectGame.Common.Messages;
using ProjectGame.Common.Logging;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using ProjectGame.Common.Configurations;
using ProjectGame.Player.Agents;
using ProjectGame.Player.Strategies;
using Location = ProjectGame.Common.Messages.Location;
using TeamColour = ProjectGame.Common.Messages.TeamColour;

namespace ProjectGame.Player
{
    public class Player : IPlayer
    {
        /// <summary>
        ///     Communication client used to send messages.
        /// </summary>
        private CommunicationClient _communicationClient;

		/// <summary>
		/// Queue with messages to process
		/// </summary>
		private readonly BlockingCollection<Message> _incomingQueue;

		/// <summary>
		/// Token to interrupt blocking on queues
		/// </summary>
		private readonly CancellationTokenSource _cancellationTokenSource;

		/// <summary>
		/// Informs player whether expected response was received
		/// </summary>
		private bool receivedResponse = false;

		/// <summary>
		/// Holds status of current game seen by this player
		/// </summary>
		private Board board;

		/// <summary>
		/// Decides and makes next move
		/// </summary>
		private StandardPlayerStrategy strategy;

		/// <summary>
		/// Holds current status of a game
		/// </summary>
		private bool gameFinished;

		/// <summary>
		///     Constructor.
		/// </summary>
		/// <param name="cancellationTokenSource"></param>
		public Player(CancellationTokenSource cancellationTokenSource)
		{
			_incomingQueue = new BlockingCollection<Message>();
		    _cancellationTokenSource = cancellationTokenSource;
		}

        /// <summary>
        ///     Do not use. For testing purposes ONLY.
        /// </summary>
        public Player(CommunicationClient client=null, string gameName="TEST",
                      PlayerType type = PlayerType.leader,TeamColour col=TeamColour.blue, 
                      Board board=null, List<OtherPlayer> players=null, string guid=null, int gameId=0)
        {
            _communicationClient = client;
            GameName = gameName;
            Type = type;
            Team = col;
            this.board = board;
            _playersField = players;
            Id = 123123;
            Guid = guid;
            GameId = (ulong)gameId;
            strategy = new StandardPlayerStrategy(board);
        }



        /// <summary>
        ///     Sends a <see cref="Message" /> to the given agent.
        /// </summary>
        /// <param name="message"><see cref="Message" /> object to send.</param>
        virtual public void SendMessage(Message message)
        {
            _communicationClient.SendMessage(message);
        }

        /// <summary>
        ///     ID of the agent.
        /// </summary>
        public virtual ulong Id { get; set; }

        /// <summary>
        ///     Secret guid of the player.
        /// </summary>
        public string Guid { get; private set; }

        /// <summary>
        ///     ID of the current game.
        /// </summary>
        public ulong GameId { get; private set; }

        public IAgent GameMaster
        {
            get { return _gameMasterField; }
        }

        public IEnumerable<IAgent> Players
        {
            get { return _playersField; }
        }

        public string GameName { get; private set; }

        public TeamColour Team { get; private set; }

        public PlayerType Type { get; private set; }

        private Agents.GameMaster _gameMasterField;

        public List<Agents.OtherPlayer> _playersField = new List<Agents.OtherPlayer>();

        /// <summary>
        ///     Connects to a Communication Server
        /// </summary>
        /// /// <param name="hostname">Host name under which the server is available.</param>
        /// <param name="port">Port of the server instance.</param>
        public void Connect(string hostname, int port)
        {
            _communicationClient = new CommunicationClient(hostname, port, PlayerConfiguration.KeepAliveInterval);
            _communicationClient.StartReading();
			Task.Run(() => ReadMessages(), _cancellationTokenSource.Token);
		}

        /// <summary>
        ///     Registers player with server
        /// </summary>
        public void GetGames()
        {
            Log.Game.Info("Getting Games...");
            var message = new GetGames();
            _communicationClient.SendMessage(message);
        }

        /// <summary>
        ///     Registers player with server
        /// </summary>
        public void Register()
        {
            Log.Game.Info("Sending registering message...");
            var message = new JoinGame
            {
                gameName = GameName,
                preferredRole = Type,
                preferredTeam = Team,
            };
            _communicationClient.SendMessage(message);
        }

        /// <summary>
        ///     Main loop - receives and handles messages
        /// </summary>
        public void Start()
        {
			gameFinished = receivedResponse = false;
            try
            {
                while (!gameFinished && !_cancellationTokenSource.IsCancellationRequested)
                {
					var message = _incomingQueue.Take(_cancellationTokenSource.Token);
					message.Handle(this);
					if (receivedResponse && board?.AcceptedExchange == false)
					{
						receivedResponse = false;
                            gameFinished = strategy.MakeNextMove();
					}
                }
            }
            catch (OperationCanceledException)
            {
                _communicationClient.EndReading();
                Log.Info("Cancellation requested, stopping execution");
            }
        }

        /// <summary>
        ///     Sets the Player's current game
        /// </summary>
        /// <param name="playerId">The ID assigned to the Player</param>
        /// <param name="gameId">The Id of the game</param>
        /// <param name="guid">Player's new secret guid</param>
        public void SetGame(ulong playerId, ulong gameId, string guid, TeamColour team, PlayerType type)
        {
            Id = playerId;
            Guid = guid;
            GameId = gameId;
            Team = team;
            Type = type;

            _gameMasterField = new Agents.GameMaster(this, gameId);
        }

        public void AddPlayer(Common.Messages.Player p)
        {
            
            _playersField.Add(new Agents.OtherPlayer(this, p));
        }

        public void SetExpectations(string gameName, string team, string type)
        {
            GameName = gameName;

            try
            {
                Team = (TeamColour)Enum.Parse(typeof(TeamColour), team, true);
            }
            catch
            {
                Log.Game.Fatal("Invalid team colour. Expected `blue` or `red`.");
                throw;
            }

            try
            {
                Type = (PlayerType)Enum.Parse(typeof(PlayerType), type, true);
            }
            catch
            {
                Log.Game.Fatal("Invalid player type. Expected `member` or `leader`.");
                throw;
            }
        }
        public void CreateBoard(uint tasksHeight, uint goalsHeight, uint width, Location location)
        {
            Log.Game.Info($"New location: {location.x}, {location.y}");
            Log.Game.Info($"New board: {tasksHeight}/{goalsHeight}/{width}");

			board = new Board(GameMaster, _playersField, location, tasksHeight, goalsHeight, width, Team, Id, Guid, GameId);
			strategy = PlayerConfiguration.StrategyFactory.Create(board);
		}

        public async void RetryGetGames()
        {
			try
			{
				await Task.Delay((int)PlayerConfiguration.RetryJoinGameInterval, _cancellationTokenSource.Token);
				GetGames();
			}
			catch (OperationCanceledException)
			{
				Log.Info("Cancellation requested, stopping execution");
			}
		}

		/// <summary>
		/// Informs player, that response for sent request was received
		/// </summary>
		public void ResponseReceived()
		{
			if(this.receivedResponse)
			{
				board.KnowledgeExchangeComplete();
			}
			this.receivedResponse = true;
		}

		/// <summary>
		/// Updates board with response data
		/// </summary>
		/// <param name="data">Data received from Game Master or another Player</param>
		public void UpdateBoard(Data data)
		{
#warning An ugly solution without checking why it happened. Might be related to finishing the game while too many moves
            if (board != null)
			    board.Update(data);
		}

        /// <summary>
        /// Returns a Data message to be sent to another player
        /// </summary>
        public Data SendBoard(ulong targetId)
        {
            return board.Send(targetId);
        }

        /// <summary>
        /// Returns a message with rejection of a knowledge exchange.
        /// </summary>
        public RejectKnowledgeExchange RejectKnowledgeExchange(ulong targetId)
        {
            RejectKnowledgeExchange message = new RejectKnowledgeExchange();
            message.playerId = targetId;
            message.senderPlayerId = Id;
            message.permanent = false;
            return message;
        }

        /// <summary>
        /// Returns a message with authorization of a knowledge exchange.
        /// </summary>
        public AuthorizeKnowledgeExchange AuthorizeKnowledgeExchange(ulong targetId)
        {
            AuthorizeKnowledgeExchange message = new AuthorizeKnowledgeExchange();
            message.gameId = GameId;
            message.playerGuid = Guid;
            message.withPlayerId = targetId;
            return message;
        }

        /// <summary>
        /// Takes required actions, after the game finishes.
        /// </summary>
        public void GameFinished()
		{
			//TODO Log some kind information, about ending game?
		    if (gameFinished) return;
			gameFinished = true;
			board?.DisplayBoard();
			_gameMasterField = null;
		    board = null;
			_playersField.Clear();
		}

        /// <summary>
		/// Reads incoming messsages and puts them in a queue
		/// </summary>
		private void ReadMessages()
		{
			while (!_cancellationTokenSource.IsCancellationRequested)
			{
				_incomingQueue.Add(_communicationClient.GetMessage());
			}
		}

		/// <summary>
		/// Decides whether player should accept knowledge exchange request or not
		/// </summary>
		/// <param name="playerId">Id of player requesting exchange</param>
		/// <returns>Information, whether exchange knowledge or not</returns>
		public bool ShouldAcceptKnowledgeExchange(ulong playerId)
		{
			if (board.IsAnyLeaderOfTeam(playerId))
			{
				return true;
			}

			return strategy.ShouldAcceptKnowledgeExchange(playerId);
		}

		/// <summary>
		/// Saves information, that player accepted knowledge exchange request
		/// </summary>
		public void AcceptKnowledgeExchange()
		{
			board.AcceptKnowledgeExchange();
		}
	}
}
