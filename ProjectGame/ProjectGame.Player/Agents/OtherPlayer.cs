﻿using System;
using ProjectGame.Common;
using ProjectGame.Common.Messages;
using ProjectGame.Common.Logging;

namespace ProjectGame.Player.Agents
{
    public class OtherPlayer : IAgent
    {
        private Player _localPlayer;

        public OtherPlayer(Player _local, Common.Messages.Player player)
        {
            Id = player.id;
            team = player.team;
            type = player.type;
            _localPlayer = _local;
        }

        public virtual ulong Id  { get; set; }
        private TeamColour team { get; set; }
        private PlayerType type { get; set; }

        public void Dispose() { }

		public void SendMessage(Message message)
        {
            if (! (message is BetweenPlayersMessage))
            {
                Log.Protocol.Error($"Cannot send {message} to a player.");
                throw new NotSupportedException();
            }

            BetweenPlayersMessage betweenMessage = (BetweenPlayersMessage)message;
            betweenMessage.playerId = Id;
            betweenMessage.senderPlayerId = _localPlayer.Id;
            _localPlayer.SendMessage(betweenMessage);
        }

		public virtual bool IsLeader()
		{
			return type == PlayerType.leader;
		}

		public virtual bool IsFromTeam(TeamColour team)
		{
			return this.team == team;
		}
    }
}
