﻿using System;
using ProjectGame.Common;
using ProjectGame.Common.Messages;
using ProjectGame.Common.Logging;

namespace ProjectGame.Player.Agents
{
    public class GameMaster : IAgent
    {
        private Player _localPlayer;

        public GameMaster(Player local, ulong id)
        {
            Id = id;
            _localPlayer = local;
        }

        public ulong Id { get; set; }

		public void Dispose() { }

		public void SendMessage(Message message)
        {
            if (!(message is GameMessage))
            {
                Log.Protocol.Error($"Cannot send {message} to a game master.");
                throw new NotSupportedException();
            }

            GameMessage gameMessage = (GameMessage)message;
            gameMessage.playerGuid = _localPlayer.Guid;
            gameMessage.gameId = Id;
            _localPlayer.SendMessage(gameMessage);
        }
    }
}
