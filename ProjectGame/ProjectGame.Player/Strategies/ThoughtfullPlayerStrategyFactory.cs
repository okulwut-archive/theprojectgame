﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGame.Player.Strategies
{
    public class ThoughtfullPlayerStrategyFactory : IStrategyFactory
    {
        public StandardPlayerStrategy Create(Board board)
        {
            return new ThoughtfullPlayerStrategy(board);
        }
    }
}
