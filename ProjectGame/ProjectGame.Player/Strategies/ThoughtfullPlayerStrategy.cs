﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectGame.Common.Messages;

namespace ProjectGame.Player.Strategies
{
    /// <summary>
    /// Choosing closest goals
    /// Communication with random after successful placement
    /// </summary>
    public class ThoughtfullPlayerStrategy : QuickOnlyWithCommunicationPlayerStrategy
    {
        public ThoughtfullPlayerStrategy(Board board) : base(board)
        {
        }

        protected override bool ExecuteActionForHoldingPieceState()
        {
            //that approach is not optimal - it would be better to move to the goals area first
            if (board.IsPlayerHoldingUnknown)
            {
                TestPiece();
                return false;
            }
            else
            {
                return base.ExecuteActionForHoldingPieceState();
            }
        }

    }
}
