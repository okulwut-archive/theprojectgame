﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGame.Player.Strategies
{
    public class QuickOnlyWithCommunicationPlayerStrategy : QuickPlayerStrategy
    {
        public QuickOnlyWithCommunicationPlayerStrategy(Board board) : base(board)
        {
        }

        public override bool ShouldAcceptKnowledgeExchange(ulong playerId)
        {
            return board.IsFromMyTeam(playerId);
        }

        protected override void ConsiderCommunicationOnSuccefullGoalDiscovery()
        {
            communicationQueue.Add(board.MyTeammates[rand.Next(board.MyTeammates.Count)]);
        }

    }
}
