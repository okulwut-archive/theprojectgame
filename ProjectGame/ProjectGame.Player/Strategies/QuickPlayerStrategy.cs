﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectGame.Common.Messages;

namespace ProjectGame.Player.Strategies
{
    /// <summary>
    /// Choosing random goals
    /// No communication
    /// </summary>
    public class QuickPlayerStrategy : StandardPlayerStrategy
    {
        private Location selectedGoalField;

        public QuickPlayerStrategy(Board board) : base(board)
        {
        }

        /// <summary>
        /// Chooses random unknown field from the goal types
        /// </summary>
        /// <returns></returns>
        protected override Location ChooseGoalFieldToCheck()
        {
            var rowCheckingOrder = GetRowCheckingOrder();
            //TODO: possibly we may use it in game state
            List<uint[]> unknownGoalFields = new List<uint[]>();
            foreach (uint i in rowCheckingOrder)
            {
                for (uint j = 0; j < board.BoardWidth; j++)
                {
                    if (IsUnknownGoalField(j, i))
                    {
                        unknownGoalFields.Add(new uint[] { j, i});
                    }
                }
            }
            if (unknownGoalFields.Count > 0)
            {
                int choice = rand.Next(unknownGoalFields.Count);
                return new Location { x = unknownGoalFields[choice][0], y = unknownGoalFields[choice][1] };
            }
            else
                return null;
        }

        protected override bool ExecuteActionForHoldingPieceState()
        {
            if (board.IsPlayerHoldingSham)
            {
                //Definitely next version should be better at holding knowledge
                //or maybe having a vector of distances at least
                //or maybe ability to destroy piece while on the task board
                if (IsPlayerInGoalArea())
                {
                    PlacePiece();
                }
                else
                {
                    MoveTowardsGoalArea();
                }
            }
                else
            {
                if (selectedGoalField == null || !IsUnknownGoalField(selectedGoalField.x, selectedGoalField.y))
                    selectedGoalField = ChooseGoalFieldToCheck();
                if ((board.PlayerLocation.x == selectedGoalField.x && board.PlayerLocation.y == selectedGoalField.y))
                {
                    PlacePiece();
                }
                else
                {
                    MoveTowardsLocation(selectedGoalField);
                }
            }
            return false;
        }

        public override bool ShouldAcceptKnowledgeExchange(ulong playerId)
        {
            return false;
        }

        protected override void ConsiderCommunicationOnSuccefullGoalDiscovery()
        {
            //this strategy never communicates
            communicationQueue.Clear();
        }
    }
}
