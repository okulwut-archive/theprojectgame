﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGame.Player.Strategies
{
    public interface IStrategyFactory
    {
        StandardPlayerStrategy Create(Board board);
    }
}
