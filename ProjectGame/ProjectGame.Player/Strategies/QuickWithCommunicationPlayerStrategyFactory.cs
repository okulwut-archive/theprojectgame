﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGame.Player.Strategies
{
    public class QuickWithCommunicationPlayerStrategyFactory : IStrategyFactory
    {
        public StandardPlayerStrategy Create(Board board)
        {
            return new QuickWithCommunicationPlayerStrategy(board);
        }
    }
}
