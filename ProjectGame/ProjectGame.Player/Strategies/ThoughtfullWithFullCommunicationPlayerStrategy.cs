﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGame.Player.Strategies
{
    /// <summary>
    /// Choosing closest goals
    /// Communication with all after successful placement
    /// </summary>
    public class ThoughtfullWithFullCommunicationPlayerStrategy : ThoughtfullPlayerStrategy
    {
        public ThoughtfullWithFullCommunicationPlayerStrategy(Board board) : base(board)
        {
        }

        protected override void ConsiderCommunicationOnSuccefullGoalDiscovery()
        {
            communicationQueue.AddRange(board.MyTeammates);
        }

    }
}
