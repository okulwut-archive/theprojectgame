﻿using ProjectGame.Common.Messages;
using ProjectGame.Common;
using System.Collections.Generic;
using System;
using System.Linq;

namespace ProjectGame.Player.Strategies
{
    public class StandardPlayerStrategy
    {
        private const uint piecesNeededToKnowledgeExchange = 2;
        /// <summary>
        /// Game seen by a player
        /// </summary>
        protected Board board;

        /// <summary>
        /// Indicates whether player should discover fields in his next move
        /// </summary>
        protected int movesTillDiscover = 0;

        /// <summary>
        /// Whether or not the last action was Move
        /// </summary>
        protected bool moved = false;

        /// <summary>
        /// Saved position
        /// </summary>
        protected Location lastLocation;

        /// <summary>
        /// Number of moves since last random move
        /// </summary>
        protected int movesCount = 0;

        /// <summary>
        /// Informs player whether he tried to pick up piece at his field. This prevents getting stuck on no longer existing piece
        /// </summary>
        protected bool sentPickup = false;

        protected bool waitForData = false;

        protected uint succesfullyPlacedPiecesCounter = 0;

        /// <summary>
        /// List of all possible move directions
        /// </summary>
        protected List<MoveType> moveDirections;
        protected readonly Random rand;
        protected List<ulong> communicationQueue;

        protected bool IsStuck
        {
            get
            {
                return moved && Equals(lastLocation, board.PlayerLocation);
            }
        }

        public StandardPlayerStrategy(Board board)
        {
            rand = new Random();
            this.board = board;
            lastLocation = board.PlayerLocation;
            moveDirections = Enum.GetValues(typeof(MoveType)).Cast<MoveType>().ToList();
            board.Updated += OnDataReceived;
            communicationQueue = new List<ulong>();
        }

        /// <summary>
        /// Decides whether to accept knowledge exchange request or not
        /// </summary>
        /// <param name="playerId">Id of player requesting exchange</param>
        /// <returns>Information, whether player should accept request or not</returns>
        public virtual bool ShouldAcceptKnowledgeExchange(ulong playerId)
        {
            //TODO change when implementing strategy
            return false;
        }

        /// <summary>
        /// Decides and makes the next move
        /// </summary>
        /// <returns>Information whether the game is finished or not</returns>
        public virtual bool MakeNextMove()
        {
            if (board.GameFinished)
            {
                return ClearStartegyStateOnGameEnd();
            }
            else if (communicationQueue.Any())
            {
                moved = false;
                return KnowledgeExchangeWithFirstInQueue();
            }
            else if (IsStuck)
            {
                return ExecuteStuckResponse();
            }
            moved = false;
            if (!board.IsPlayerHoldingPiece)
            {
                return ExecuteActionForNotHoldingPieceState();
            }
            else
            {
                return ExecuteActionForHoldingPieceState();
            }
        }

        private bool ClearStartegyStateOnGameEnd()
        {
            succesfullyPlacedPiecesCounter = 0;
            waitForData = false;
            moved = false;
            communicationQueue.Clear();
            return true;
        }

        private bool ExecuteStuckResponse()
        {
            RandomMove();
            return false;
        }

        protected virtual bool ExecuteActionForHoldingPieceState()
        {
            sentPickup = false;
            if (IsPlayerInGoalArea())
            {
                if (IsUnknownGoalField(board.PlayerLocation.x, board.PlayerLocation.y) || board.IsPlayerHoldingSham)
                {
                    PlacePiece();
                }
                else
                {
                    var newField = ChooseGoalFieldToCheck();
                    if (newField == null)
                    {
                        return false;
                    }
                    MoveTowardsLocation(newField);
                }
            }
            else
            {
                MoveTowardsGoalArea();
            }
            return false;
        }

        protected bool IsUnknownGoalField(uint x, uint y)
        {
            var field = board.Fields[y, x] as GoalField;
            return (field == null || field.type == GoalFieldType.unknown);
        }

        protected bool ExecuteActionForNotHoldingPieceState()
        {
            if (IsPlayerInGoalArea())
            {
                MoveTowardsTaskField();
                return false;
            }
            var taskField = board.Fields[board.PlayerLocation.y, board.PlayerLocation.x] as TaskField;
            if (taskField?.distanceToPiece == 0 && !sentPickup)
            {
                PickUpPiece();
            }
            else
            {
                FindClosestPiece();
            }
            return false;
        }

        private void OnDataReceived(object sender, BoardUpdatedArgs e)
        {
            if (!waitForData)
            {
                return;
            }
            waitForData = false;
            if (e.data.playerId == board.getLeaderId())
            {
                return;
            }
            if (e.data.Pieces == null)
            {
                var field = e.data.GoalFields?.Where(gf => gf.x == board.PlayerLocation.x &&
                                                          gf.y == board.PlayerLocation.y).SingleOrDefault();
                if (field != null && field.type != GoalFieldType.unknown)
                {
                    ++succesfullyPlacedPiecesCounter;
                    ConsiderCommunicationOnSuccefullGoalDiscovery();
                }
            }
        }

        protected virtual void ConsiderCommunicationOnSuccefullGoalDiscovery()
        {
            if ((succesfullyPlacedPiecesCounter % piecesNeededToKnowledgeExchange) == 0 && !board.IsLeader)
                communicationQueue.Add(board.getLeaderId());
        }


        /// <summary>
        /// Places piece held by player
        /// </summary>
        protected void PlacePiece()
        {
            board.PlacePiece();
            movesTillDiscover = 0;
            waitForData = true;
        }

        /// <summary>
        /// Picks up piece at current player location.
        /// </summary>
        protected void PickUpPiece()
        {
            board.PickUpPiece();
            movesTillDiscover = 0;
            sentPickup = true;
        }

        protected void TestPiece()
        {
            board.TestPiece();
        }

        /// <summary>
        /// Moves player towards goal area.
        /// </summary>
        protected void MoveTowardsGoalArea()
        {
            Move(board.GoalsDirection);
        }

        /// <summary>
        /// Moves to neighbor with smallest distance to piece, or discovers neighbors if there is not enough data to make decision.
        /// </summary>
        protected void FindClosestPiece()
        {
            var newLocation = GetNeighborCloseToPiece();
            if (movesTillDiscover <= 0 || newLocation == null)
            {
                board.Discover();
                movesTillDiscover = 2;
            }
            else
            {
                MoveTowardsLocation(newLocation);
                movesTillDiscover -= 1;
            }
            sentPickup = false;
        }

        /// <summary>
        /// Moves player towards task field.
        /// </summary>
        protected void MoveTowardsTaskField()
        {
            var direction = board.GoalsDirection.Opposite();
            Move(direction);
        }

        protected void Move(MoveType direction)
        {
            moved = true;
            lastLocation = board.PlayerLocation;
            board.Move(direction);
        }

        /// <summary>
        /// Moves player towards specified location.
        /// </summary>
        /// <param name="newLocation">Destination to reach</param>
        protected void MoveTowardsLocation(Location newLocation)
        {
            MoveType direction;
            if (newLocation.x != board.PlayerLocation.x)
            {
                direction = newLocation.x > board.PlayerLocation.x ? MoveType.right : MoveType.left;
            }
            else
            {
                direction = newLocation.y > board.PlayerLocation.y ? MoveType.up : MoveType.down;
            }
            Move(direction);
        }

        /// <summary>
        /// Finds first field in Goal Area that was not discovered yet
        /// </summary>
        /// <returns>Field at which player should place piece</returns>
        protected virtual Location ChooseGoalFieldToCheck()
        {
            var rowCheckingOrder = GetRowCheckingOrder();
            foreach (uint i in rowCheckingOrder)
            {
                for (uint j = 0; j < board.BoardWidth; j++)
                {
                    if (IsUnknownGoalField(j, i))
                    {
                        return new Location { x = j, y = i };
                    }
                }
            }
            return null;
        }

        protected IEnumerable<uint> GetRowCheckingOrder()
        {
            uint i, iMax;
            List<uint> rowPriority = new List<uint>();
            if (board.GoalsDirection == MoveType.up)
            {
                i = board.BoardHeight - board.GoalsHeight;
                iMax = board.BoardHeight;
                for (uint k = i; k < iMax; ++k)
                {
                    rowPriority.Add(k);
                }
            }
            else
            {
                i = 0;
                iMax = board.GoalsHeight;
                for (uint k = i; k < iMax; ++k)
                {
                    rowPriority.Add(k);
                }
                rowPriority.Reverse();
            }
            return rowPriority;
        }

        /// <summary>
        /// Information about player's position on board
        /// </summary>
        /// <returns>True when player is in Goal Area, false otherwise</returns>
        protected bool IsPlayerInGoalArea()
        {
            if (board.GoalsDirection == MoveType.up)
            {
                return board.PlayerLocation.y >= (board.BoardHeight - board.GoalsHeight);
            }
            return board.PlayerLocation.y < board.GoalsHeight;
        }

        /// <summary>
        /// Finds neighbor with smallest distance to piece
        /// </summary>
        /// <returns>Location of neighbor with smallest distance to piece, or null if any of neighbors is not discovered</returns>
        protected Location GetNeighborCloseToPiece()
        {
            List<Location> neighborLocations = null;
            if (board.GoalsDirection == MoveType.up)
            {
                neighborLocations = new List<Location>
                    {
                        new Location {x = board.PlayerLocation.x - 1, y = board.PlayerLocation.y },
                        new Location {x = board.PlayerLocation.x + 1, y = board.PlayerLocation.y },
                        new Location {x = board.PlayerLocation.x, y = board.PlayerLocation.y - 1 },
                        new Location {x = board.PlayerLocation.x, y = board.PlayerLocation.y + 1 },
                    };
            }
            else
            {
                neighborLocations = new List<Location>
                    {
                        new Location {x = board.PlayerLocation.x - 1, y = board.PlayerLocation.y },
                        new Location {x = board.PlayerLocation.x + 1, y = board.PlayerLocation.y },
                        new Location {x = board.PlayerLocation.x, y = board.PlayerLocation.y + 1 },
                        new Location {x = board.PlayerLocation.x, y = board.PlayerLocation.y - 1 },
                    };
            }
            Location closestNeighbor = null;
            int distance = int.MaxValue;

            int unknownCount = 0;
            Location unknown = null;

            foreach (var neighbor in neighborLocations)
            {
                if (neighbor.x >= 0 && neighbor.x < board.BoardWidth && neighbor.y >= 0 && neighbor.y < board.BoardHeight)
                {
                    if (board.Fields[neighbor.y, neighbor.x] is GoalField)
                    {
                        continue;
                    }
                    if (!(board.Fields[neighbor.y, neighbor.x] is TaskField))
                    {
                        unknownCount++;
                        unknown = neighbor;
                        continue;
                    }
                    var taskField = board.Fields[neighbor.y, neighbor.x] as TaskField;
                    if (taskField.distanceToPiece < 0)
                    {
                        unknownCount++;
                        unknown = neighbor;
                    }
                    else if (taskField.distanceToPiece < distance)
                    {
                        distance = taskField.distanceToPiece;
                        closestNeighbor = neighbor;
                    }
                }
            }

            TaskField playerField = board.Fields[board.PlayerLocation.y, board.PlayerLocation.x] as TaskField;
            if (playerField != null && playerField.distanceToPiece < distance && unknownCount == 1)
            {
                closestNeighbor = unknown;
            }

            return closestNeighbor;
        }

        protected bool KnowledgeExchangeWithFirstInQueue()
        {
            board.KnowledgeExchange(communicationQueue[0]);
            communicationQueue.RemoveAt(0);
            return false;
        }

        protected void RandomMove()
        {
            int direction = rand.Next(moveDirections.Count);
            Move(moveDirections[direction]);
        }

    }
}
