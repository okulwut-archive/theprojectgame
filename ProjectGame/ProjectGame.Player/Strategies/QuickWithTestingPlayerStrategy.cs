﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectGame.Player.Strategies
{
    public class QuickWithTestingPlayerStrategy : QuickPlayerStrategy
    {
        public QuickWithTestingPlayerStrategy(Board board) : base(board)
        {
        }

        protected override bool ExecuteActionForHoldingPieceState()
        {
            //that approach is not optimal - it would be better to move to the goals area first
            if (board.IsPlayerHoldingUnknown)
            {
                TestPiece();
                return false;
            }
            else
            {
                return base.ExecuteActionForHoldingPieceState();
            }
        }

    }
}
