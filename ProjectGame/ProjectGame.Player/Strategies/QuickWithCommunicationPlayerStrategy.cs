﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectGame.Common.Messages;

namespace ProjectGame.Player.Strategies
{
    /// <summary>
    /// Choosing closest goals
    /// Communication with random after successful placement
    /// </summary>
    public class QuickWithCommunicationPlayerStrategy : QuickOnlyWithCommunicationPlayerStrategy
    {
        public QuickWithCommunicationPlayerStrategy(Board board) : base(board)
        {
        }
        /// <summary>
        /// Chooses closest unknown field
        /// </summary>
        /// <returns></returns>
        protected override Location ChooseGoalFieldToCheck()
        {
            var rowCheckingOrder = GetRowCheckingOrder();
            //TODO: possibly we may use it in game state
            uint[] closestLocation = null;
            long smallestDistance = long.MaxValue;
            foreach (uint i in rowCheckingOrder)
            {
                for (uint j = 0; j < board.BoardWidth; j++)
                {
                    if (IsUnknownGoalField(j, i) && Math.Abs((int)j - (int)board.PlayerLocation.x) + Math.Abs((int)i - (int)board.PlayerLocation.y) < smallestDistance)
                    {
                        smallestDistance = Math.Abs((int)j - (int)board.PlayerLocation.x) + Math.Abs((int)i - (int)board.PlayerLocation.y);
                        closestLocation = new uint[] {j, i };
                    }
                }
            }
            if (closestLocation != null)
            {
                return new Location { x = closestLocation[0], y = closestLocation[1] };
            }
            else
                return null;
        }
        
    }
}
