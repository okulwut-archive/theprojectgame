﻿using ProjectGame.Common;
using ProjectGame.Common.Messages;
using ProjectGame.Player.Agents;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectGame.Player
{
    public class BoardUpdatedArgs: EventArgs { public Data data; }

    public delegate void BoardUpdatedHandler(object sender, BoardUpdatedArgs e);

    public class Board
	{
		/// <summary>
		/// Reference to Game Master of current game
		/// </summary>
		private IAgent gameMaster;

		/// <summary>
		/// Collection of other players in the same game
		/// </summary>
		public Dictionary<ulong, OtherPlayer> otherPlayers { get; private set; }

        /// <summary>
        /// Height of goals area
        /// </summary>
        virtual public uint GoalsHeight { get; private set; }

        /// <summary>
        /// Total height of board
        /// </summary>
        virtual public uint BoardHeight { get; private set; }

        /// <summary>
        /// Width of board
        /// </summary>
        virtual public uint BoardWidth { get; private set; }

        /// <summary>
        /// Board seen by a player
        /// </summary>
        virtual public Field[,] Fields { get; private set; }

        /// <summary>
        /// Current player location
        /// </summary>
        virtual public Location PlayerLocation { get; private set; }

		/// <summary>
		/// Collection of pieces seen by a player in current game
		/// </summary>
		virtual public Dictionary<ulong, Piece> Pieces { get; private set; }

		/// <summary>
		/// Logical information about player holding piece
		/// </summary>
		virtual public bool IsPlayerHoldingPiece { get { return PlayersPiece != null; } }
        virtual public bool IsPlayerHoldingSham { get { return PlayersPiece != null && PlayersPiece.type == PieceType.sham; } }
        virtual public bool IsPlayerHoldingUnknown { get { return PlayersPiece != null && PlayersPiece.type == PieceType.unknown; } }
        virtual protected Piece PlayersPiece { get; private set; }

        /// <summary>
        /// Logical information about game status
        /// </summary>
        virtual public bool GameFinished { get; private set; }

        /// <summary>
        /// Information where is the goal area located on board
        /// </summary>
        virtual public MoveType GoalsDirection { get; private set; }

		/// <summary>
		/// Color of a team, that this player belongs to
		/// </summary>
		public TeamColour colour { get; private set; }

        public event BoardUpdatedHandler Updated;

        public bool AcceptedExchange { get; private set; } = false;

        public bool IsLeader { get { return id == getLeaderId(); } }

		private ulong id;

		private string guid;

		private ulong gameid;

		public Board(IAgent gameMaster, IEnumerable<OtherPlayer> otherPlayers, Location location, uint tasksHeight, uint goalsHeight, uint width, TeamColour color, ulong id, string guid,
			ulong gameId)
		{
			this.gameMaster = gameMaster;
			this.otherPlayers = new Dictionary<ulong, OtherPlayer>();
			foreach(var player in otherPlayers)
			{
				this.otherPlayers.Add(player.Id, player);
			}
			this.id = id;
			this.colour = color;
			PlayerLocation = location;
			GoalsHeight = goalsHeight;
			BoardHeight = tasksHeight + 2*goalsHeight;
			BoardWidth = width;
			Fields = new Field[BoardHeight, BoardWidth];
			GoalsDirection = color == TeamColour.red ? MoveType.up : MoveType.down;
			Pieces = new Dictionary<ulong, Piece>();
			this.guid = guid;
			gameid = gameId;
		}

        /// <summary>
        ///     Do not use. For testing purposes ONLY.
        /// </summary>
        public Board(IEnumerable<OtherPlayer> otherPlayers=null,  Field[,] fields=null,    IAgent gameMaster=null,  
                     Location location=null,                 Dictionary<ulong, Piece> pieces=null,
                     uint tasksHeight=0,                     uint goalsHeight=0,      uint width=0,
                     TeamColour color=TeamColour.blue,       ulong id=0)
        {
            this.gameMaster = gameMaster;
			this.otherPlayers = new Dictionary<ulong, OtherPlayer>();
			foreach (var player in otherPlayers)
			{
				this.otherPlayers.Add(player.Id, player);
			}
			this.id = id;
			this.colour = color;
            PlayerLocation = location;
            GoalsHeight = goalsHeight;
            BoardHeight = tasksHeight + 2 * goalsHeight;
            BoardWidth = width;
            Fields = fields;
            GoalsDirection = color == TeamColour.red ? MoveType.up : MoveType.down;
            Pieces = pieces;
        }

        /// <summary>
        /// Sends request to move player in specified direction
        /// </summary>
        /// <param name="direction">Direction in which player should be moved</param>
        public virtual void Move(MoveType direction)
		{
			gameMaster.SendMessage(new Move() { directionSpecified = true, direction = direction });
		}

        /// <summary>
        /// Sends discover request
        /// </summary>
        public virtual void Discover()
		{
			gameMaster.SendMessage(new Discover());
		}

        /// <summary>
        /// Sends TestPiece request
        /// </summary>
        public virtual void TestPiece()
		{
			gameMaster.SendMessage(new TestPiece());
		}

        /// <summary>
        /// Sends PickUpPiece request
        /// </summary>
        public virtual void PickUpPiece()
		{
			gameMaster.SendMessage(new PickUpPiece());
		}

        /// <summary>
        /// Sends PlacePiece request
        /// </summary>
        public virtual void PlacePiece()
		{
			gameMaster.SendMessage(new PlacePiece());
            PlayersPiece = null;
		}

		/// <summary>
		/// Starts knowledge exchange with given player
		/// </summary>
		/// <param name="playerId">Id of player to start knowledge exchange with</param>
		public virtual void KnowledgeExchange(ulong playerId)
		{
			gameMaster.SendMessage(new AuthorizeKnowledgeExchange()
			{
				playerGuid = this.guid,
				withPlayerId = playerId,
				gameId = gameid
			});
		}

		/// <summary>
		/// Indicates that player accepted knowledge exchange and should wait for response
		/// </summary>
		public virtual void AcceptKnowledgeExchange()
		{
			this.AcceptedExchange = true;
		}

		/// <summary>
		/// Indicates that player completed knowledge exchange sequence and can make next move
		/// </summary>
		public virtual void KnowledgeExchangeComplete()
		{
			this.AcceptedExchange = false;
		}

		/// <summary>
		/// Informs player, whether given or this player is leader of his team
		/// </summary>
		/// <param name="playerId">Id of player to check</param>
		/// <returns>Information whether given player is a leader or not</returns>
		public virtual bool IsAnyLeaderOfTeam(ulong playerId)
		{
			var player = otherPlayers[playerId];
			return player.IsFromTeam(this.colour) && (player.IsLeader() || otherPlayers[this.id].IsLeader());
		}

        public virtual bool IsFromMyTeam(ulong playerId)
        {
            var player = otherPlayers[playerId];
            return player.IsFromTeam(this.colour);
        }

        public virtual List<ulong> MyTeammates
        {
            get
            {
                return otherPlayers
                    .Where(pl => pl.Key != id && pl.Value.IsFromTeam(this.colour))
                    .Select(pl => pl.Key)
                    .ToList();
            }
        }

        /// <summary>
        /// Updates board with information from response
        /// </summary>
        /// <param name="data">Information received from Game Master or other Player</param>
        public virtual void Update(Data data)
		{
			GameFinished = data.gameFinished;
			if (data.PlayerLocation != null)
			{
				PlayerLocation = data.PlayerLocation;
			}
			if(data.Pieces != null)
			{
				foreach(var piece in data.Pieces)
				{
					if(piece.playerId == id)
					{
						PlayersPiece = piece;
					}
					if(!Pieces.ContainsKey(piece.id))
					{
						Pieces.Add(piece.id, piece);
					}
					else
					{
						if (piece.timestamp.CompareTo(Pieces[piece.id].timestamp) >= 0 && Pieces[piece.id].type == PieceType.unknown && piece.type != PieceType.unknown)
						{
							Pieces[piece.id] = piece;
						}
					}
				}
			}

			if(data.TaskFields != null)
			{
				foreach (var taskField in data.TaskFields)
				{
					if (ShouldUpdateTaskField(taskField.timestamp, taskField.x, taskField.y))
					{
						Fields[taskField.y, taskField.x] = taskField;
					}
				}
			}
			if(data.GoalFields != null)
			{
				foreach (var goalField in data.GoalFields)
				{
					if (ShouldUpdateGoalField(goalField))
					{
						Fields[goalField.y, goalField.x] = goalField;
					}
				}
			}

            OnBoardUpdate(new BoardUpdatedArgs() { data = data});

		}

        /// <summary>
        /// Returns a Data message filled with known information about the Board.
        /// </summary>
        public Data Send(ulong targetId)
        {
            Data data = new Data();
            data.gameFinished = GameFinished;
            data.playerId = targetId;
            data.Pieces = Pieces.Values.ToList().ToArray();
            data.TaskFields = Fields.OfType<TaskField>().ToArray();
            data.GoalFields = Fields.OfType<GoalField>().ToArray();
            return data;
        }

        public virtual void DisplayBoard()
		{
			Console.Out.WriteLine("Board status: ");
			for(uint i = 0; i < BoardHeight; i++)
			{
				for(uint j = 0; j < BoardWidth; j++)
				{
					string val = "";
					if (Fields[i, j] is TaskField)
					{
						val = (Fields[i, j] as TaskField).distanceToPiece.ToString();
					}
					if (Fields[i,j] is GoalField)
					{
						val = (Fields[i, j] as GoalField).type == GoalFieldType.goal ? "G" : ((Fields[i, j] as GoalField).type == GoalFieldType.nongoal ? "NG" : "UN");
					}
					Console.Out.Write("|{0,-2}", val);
				}
				Console.Out.WriteLine("|");
			}
		}

		private bool ShouldUpdateTaskField(DateTime timestamp, uint x, uint y)
		{
			return Fields[y, x] == null || timestamp.CompareTo(Fields[y, x].timestamp) >= 0;
		}

        private bool ShouldUpdateGoalField(GoalField goalField)
        {
            GoalField field = Fields[goalField.y, goalField.x] as GoalField;
            return field == null || (goalField.timestamp.CompareTo(field.timestamp) >= 0 && field.type == GoalFieldType.unknown && goalField.type != GoalFieldType.unknown);
        }
        public ulong getLeaderId()
        {
            var leader = otherPlayers.Values.Where(player => player.IsLeader() && player.IsFromTeam(colour)).SingleOrDefault();
            return leader == null ? id : leader.Id;
        }

        protected virtual void OnBoardUpdate(BoardUpdatedArgs e)
        {
            Updated?.Invoke(this, e);
        }

    }
}
