﻿using ProjectGame.Common.Configurations;
using ProjectGame.Common.Logging;
using ProjectGame.Player.Strategies;
using System.IO;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ProjectGame.Player
{
	public static class PlayerConfiguration
	{
        public static IStrategyFactory StrategyFactory  { get; set; }
        public static uint KeepAliveInterval { get; set; }

		public static uint RetryJoinGameInterval { get; set; }

		public static bool ParseSettings(string filePath)
		{
			var validator = new Common.XmlSchemaValidator($"{typeof(Configuration).Namespace}.Configuration.xsd");
			var xdoc = XDocument.Load(filePath);
			try
			{
				validator.Validate(xdoc);
			}
			catch (XmlSchemaValidationException ex)
			{
				Log.Error($"{filePath} is not a valid configuration file. Details:\n${ex.Message}");
				return false;
			}

			var deserializer = new XmlSerializer(typeof(PlayerSettings));
			var reader = new StringReader(xdoc.ToString());
			var configuration = (PlayerSettings)deserializer.Deserialize(reader);

			KeepAliveInterval = configuration.KeepAliveInterval;
			RetryJoinGameInterval = configuration.RetryJoinGameInterval;
            switch (configuration.StrategyType)
            {
                case PlayerSettingsStrategyType.Standard:
                    StrategyFactory = new StandardPlayerStrategyFactory(); break;
                case PlayerSettingsStrategyType.Quick:
                    StrategyFactory = new QuickPlayerStrategyFactory(); break;
                case PlayerSettingsStrategyType.QuickTesting:
                    StrategyFactory = new QuickWithTestingPlayerStrategyFactory(); break;
                case PlayerSettingsStrategyType.QuickOnlyCommunication:
                    StrategyFactory = new QuickOnlyWithCommunicationPlayerStrategyFactory(); break;
                case PlayerSettingsStrategyType.QuickCommunication:
                    StrategyFactory = new QuickWithCommunicationPlayerStrategyFactory(); break;
                case PlayerSettingsStrategyType.Thoughtful:
                    StrategyFactory = new ThoughtfullPlayerStrategyFactory(); break;
                case PlayerSettingsStrategyType.ThoughtfulFullCommunication:
                    StrategyFactory = new ThoughtfullWithFullCommunicationPlayerStrategyFactory(); break;
                default:
                    StrategyFactory = new StandardPlayerStrategyFactory(); break;
            }


            return true;
		}
	}
}
