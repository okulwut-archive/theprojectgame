﻿using System;
using System.Threading;
using ProjectGame.Common;
using ProjectGame.Common.Logging;

namespace ProjectGame.Player
{
    /// <summary>
    ///     Main class for the player.
    /// </summary>
    public class Program
    {
        /// <summary>
        ///     List of parameters required by the player.
        /// </summary>
        private static readonly string[] RequiredParameters = {"host", "port", "game", "team", "role", "conf"};

        private static CancellationTokenSource _cancellationTokenSource;

        /// <summary>
        ///     Entry point method for the player.
        /// </summary>
        /// <param name="args">Arguments passed to the program in the command line, as a string array.</param>
        public static void Main(string[] args)
        {
            var parsedArgs = ArgumentParser.Parse(args, RequiredParameters);
            var port = ArgumentParser.GetPort(parsedArgs["port"]);
            Log.Verbose = ArgumentParser.IsVerbose(parsedArgs);

            if (!PlayerConfiguration.ParseSettings(parsedArgs["conf"]))
            {
                return;
            }

            Log.Info("Starting");
            _cancellationTokenSource = new CancellationTokenSource();
            var player = new Player(_cancellationTokenSource);

            Console.CancelKeyPress += Console_CancelKeyPress;
            player.SetExpectations(parsedArgs["game"], parsedArgs["team"], parsedArgs["role"]);
            player.Connect(parsedArgs["host"], port);
            while (!_cancellationTokenSource.IsCancellationRequested)
            {
                player.GetGames();
                player.Start();
            }
        }

        /// <summary>
        ///     Event handler that releases resources when program is cancelled
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event parameters</param>
        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            _cancellationTokenSource.Cancel();
            e.Cancel = true;
        }
    }
}