﻿using ProjectGame.Common.Messages;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace ProjectGame.Common
{
	public static class XmlSerializerFactory
	{
		public static XmlSerializer GetSerializer(Message message)
		{
			return new XmlSerializer(message.GetType());
		}

		public static XmlSerializer GetDeserializer(string message)
		{
			XmlSerializer serializer;
			using (var reader = XmlReader.Create(new StringReader(message)))
			{
				reader.MoveToContent();
				var type = Type.GetType(typeof(Message).Namespace + "." + reader.Name);
				if(type == null)
				{
					throw new NotSupportedException($"{reader.Name} type of message is not supported");
				}
				serializer = new XmlSerializer(type);
			}
			return serializer;
		}
	}
}
