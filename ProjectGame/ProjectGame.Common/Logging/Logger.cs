﻿namespace ProjectGame.Common.Logging
{
	public class Logger
	{
		/// <summary>
		///     Internal logging client.
		/// </summary>
		private readonly log4net.ILog _log;

		public Logger(string name)
		{
			_log = log4net.LogManager.GetLogger(name);
		}

		/// <summary>
		///     Logs message with severity DEBUG.
		/// </summary>
		public void Debug(object message)
		{
		    if (Log.Verbose)
		    {
		        _log.Debug(message);
		    }
		}

		/// <summary>
		///     Logs message with severity INFO.
		/// </summary>
		public void Info(object message)
		{
			_log.Info(message);
		}

		/// <summary>
		///     Logs message with severity WARN.
		/// </summary>
		public void Warn(object message)
		{
			_log.Warn(message);
		}

		/// <summary>
		///     Logs message with severity ERROR.
		/// </summary>
		public void Error(object message)
		{
			_log.Error(message);
		}

		/// <summary>
		///     Logs message with severity FATAL.
		/// </summary>
		public void Fatal(object message)
		{
			_log.Fatal(message);
		}


	}
}
