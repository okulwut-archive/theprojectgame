﻿[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace ProjectGame.Common.Logging
{
	public static class Log
	{
		/// <summary>
		///     Internal logging client.
		/// </summary>
		private static readonly log4net.ILog _log = log4net.LogManager.GetLogger("General");

        public static bool Verbose { get; set; }

		/// <summary>
		///     Logs message with severity DEBUG.
		/// </summary>
		public static void Debug(object message)
		{
		    if (Verbose)
		    {
		        _log.Debug(message);
		    }
		}

		/// <summary>
		///     Logs message with severity INFO.
		/// </summary>
		public static void Info(object message)
		{
			_log.Info(message);
		}

		/// <summary>
		///     Logs message with severity WARN.
		/// </summary>
		public static void Warn(object message)
		{
			_log.Warn(message);
		}

		/// <summary>
		///     Logs message with severity ERROR.
		/// </summary>
		public static void Error(object message)
		{
			_log.Error(message);
		}

		/// <summary>
		///     Logs message with severity FATAL.
		/// </summary>
		public static void Fatal(object message)
		{
			_log.Fatal(message);
		}

		/// <summary>
		///     Protocol specific logging.
		/// </summary>
		public static readonly Logger Protocol = new Logger("Protocol");

		/// <summary>
		///     Network specific logging.
		/// </summary>
		public static readonly Logger Network = new Logger("Network");

		/// <summary>
		///     Game specific logging.
		/// </summary>
		public static readonly Logger Game = new Logger("Game");

		/// <summary>
		///     AI specific logging.
		/// </summary>
		public static readonly Logger AI = new Logger("AI");
	}
}
