﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectGame.Common.Logging;

namespace ProjectGame.Common
{
    public class ArgumentParser
    {
        /// <summary>
        ///     The delimiter used for separating keys and values in the input arguments..
        /// </summary>
        private const char Delimiter = '=';

        private const string VerboseKey = "verbose";

        /// <summary>
        ///     Parses the incoming arguments in the format <code>key=value</code> into a dictionary and checks whether all
        ///     required arguments have been supplied.
        /// </summary>
        /// <param name="args">List of command-line arguments supplied to the component.</param>
        /// <param name="requiredKeys">An enumerable of required keys as strings.</param>
        /// <returns>A dictionary containing the input arguments.</returns>
        /// <exception cref="ArgumentException">Thrown when a required key is missing from the arguments.</exception>
        public static Dictionary<string, string> Parse(string[] args, IEnumerable<string> requiredKeys)
        {
			var joinedArgs = string.Join(" ", args);
			args = joinedArgs.Split('|');
            var dict = args.Where(arg => arg.Contains(Delimiter))
                .Select(arg => arg.Split(Delimiter))
                .ToDictionary(keyvalue => keyvalue[0], keyvalue => keyvalue[1]);
            var missing = requiredKeys.Except(dict.Keys).ToList();
            if (missing.Count > 0)
            {
				Log.Fatal($"The following input arguments are missing: {string.Join(", ", missing)}");
				throw new ArgumentException($"The following input arguments are missing: {string.Join(", ", missing)}");
            }
            return dict;
        }

        /// <summary>
        ///     Extracts a port number from a string.
        /// </summary>
        /// <param name="portString">Port number as a string.</param>
        /// <returns>Port number as an integer.</returns>
        /// <exception cref="ArgumentException">Thrown if the supplied string is not a number, or is a number from the wrong range.</exception>
        public static int GetPort(string portString)
        {
            int result;
            var parsed = int.TryParse(portString, out result);
            if (!parsed)
            {
				Log.Network.Fatal($"Invalid port \"{portString}\" supplied");
				throw new ArgumentException($"Invalid port \"{portString}\" supplied");
            }
            if (result <= 0 || result >= 65536)
            {
				Log.Network.Fatal($"The supplied port {portString} is out of the port number bounds");
				throw new ArgumentException($"The supplied port {portString} is out of the port number bounds");
            }
            return result;
        }

        public static bool IsVerbose(Dictionary<string, string> parsedArgs)
        {
            return parsedArgs.ContainsKey(VerboseKey) && !string.IsNullOrEmpty(parsedArgs[VerboseKey]);
        }
    }
}