﻿using System;
using System.Collections.Concurrent;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using ProjectGame.Common.Logging;
using ProjectGame.Common.Messages;

namespace ProjectGame.Common
{
    /// <summary>
    ///     Class used to send messages to the communication server.
    /// </summary>
    public class CommunicationClient : IDisposable
    {
        /// <summary>
        /// Stream wrapper used to send serialized messages over the socket.
        /// </summary>
        private readonly CommunicationStream _communicationStream;

        /// <summary>
        ///     The underlying <see cref="TcpClient" /> used to send data.
        /// </summary>
        private readonly TcpClient _tcpClient;

        /// <summary>
        /// Queue for incoming messages.
        /// </summary>
        private readonly BlockingCollection<Message> _incomingQueue;
        
        /// <summary>
        /// Source of a cancellation token used to cancel async operations.
        /// </summary>
        private readonly CancellationTokenSource _cancellationTokenSource;

        /// <summary>
        ///     Constructor.
        /// </summary>
        /// <param name="hostname">Host name under which the server is available.</param>
        /// <param name="port">Port of the server instance.</param>
        /// <param name="keepAliveDelay">Delay in miliseconds between keep-alive bytes sent.</param>
        public CommunicationClient(string hostname, int port, uint keepAliveDelay = 5000)
        {
			Log.Network.Info($"Connecting new TCP client to {hostname}:{port}");
            _tcpClient = new TcpClient(hostname, port);
            _tcpClient.NoDelay = true;
            _incomingQueue = new BlockingCollection<Message>();
            _communicationStream = new CommunicationStream(_tcpClient.GetStream(), true, keepAliveDelay);
            _cancellationTokenSource = new CancellationTokenSource();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hostname">Host name under which the server is available.</param>
        /// <param name="port">Port of the server instance.</param>
        /// <param name="incomingQueue">Queue to be used for storing incoming messages</param>
        /// <param name="keepAliveDelay">Delay in miliseconds between keep-alive bytes sent.</param>
        public CommunicationClient(string hostname, int port, BlockingCollection<Message> incomingQueue, uint keepAliveDelay = 5000)
		{
			_tcpClient = new TcpClient(hostname, port);
			_incomingQueue = incomingQueue;
			_communicationStream = new CommunicationStream(_tcpClient.GetStream(), true, keepAliveDelay);
			_cancellationTokenSource = new CancellationTokenSource();
		}

        /// <summary>
        ///     Do not use. For testing purposes ONLY.
        /// </summary>
        public CommunicationClient(BlockingCollection<Message> incomingQueue=null,
                                   CancellationTokenSource source=null,
                                   CommunicationStream stream = null)
        {
            _incomingQueue = incomingQueue;
            _cancellationTokenSource = source;
            _communicationStream = stream;
        }

        /// <summary>
        /// Returns the last received message.
        /// </summary>
        /// <returns>Message object</returns>
        public Message GetMessage()
        {
            var message = _incomingQueue.Take(_cancellationTokenSource.Token);
            Log.Protocol.Debug($"Received {message}");
            return message;
        }

        public Message TryGetMessage(TimeSpan timeout)
        {
            var tempToken = new CancellationTokenSource();
            var linkedTokens = CancellationTokenSource.CreateLinkedTokenSource(tempToken.Token, _cancellationTokenSource.Token);
            try
            {
                tempToken.CancelAfter(timeout);
                var message = _incomingQueue.Take(linkedTokens.Token);
                Log.Protocol.Debug($"Received {message}");
                return message;
            }
            catch (OperationCanceledException)
            {
                if (_cancellationTokenSource.IsCancellationRequested) throw;
                Log.Protocol.Debug("Read timeout expired");
                return null;
            }
        }

        virtual public void SendMessage(Message message)
        {
            Log.Protocol.Debug($"Sending message: {message}");
            _communicationStream.SendMessage(message);
        }

        public void StartReading()
        {
            ReadingTask?.Wait(_cancellationTokenSource.Token);
            ReadingTask = _communicationStream.ReadMessages(_cancellationTokenSource.Token, (message, token) => _incomingQueue.Add(message, token));
        }

        public void EndReading()
        {
            _cancellationTokenSource.Cancel();
        }

        public void Dispose()
        {
            _tcpClient?.Close();
            _communicationStream?.Dispose();
            ((IDisposable) _tcpClient)?.Dispose();
        }

        public Task ReadingTask { get; private set; }
    }
}