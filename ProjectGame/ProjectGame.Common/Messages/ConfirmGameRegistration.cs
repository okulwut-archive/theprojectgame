﻿using ProjectGame.Common.Logging;

namespace ProjectGame.Common.Messages
{
    public partial class ConfirmGameRegistration : Message
    {
		public override void Handle(IGameMaster gameMaster)
		{
            Log.Game.Info($"Registered with id {gameId}");
            gameMaster.CreateGame(gameId);
		}
	}
}