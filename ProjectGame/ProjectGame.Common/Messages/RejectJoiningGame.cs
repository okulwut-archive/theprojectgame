﻿using ProjectGame.Common.Logging;

namespace ProjectGame.Common.Messages
{
    public partial class RejectJoiningGame
    {
        public override void Handle(ICommunicationServer communicationServer, IAgent sender)
        {
            communicationServer.ForwardToPlayer(this);
        }

        public override void Handle(IPlayer player)
        {
            Log.Game.Info("We've been rejected. Not giving up.");
			player.RetryGetGames();
        }
    }
}