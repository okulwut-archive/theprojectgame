using System;

namespace ProjectGame.Common.Messages
{
    public partial class AuthorizeKnowledgeExchange
    {
		/// <summary>
		/// Delayed used by Game Master when handling message
		/// </summary>
		public static uint Delay { get; set; }

		public override void Handle(ICommunicationServer communicationServer, IAgent sender)
        {
            communicationServer.ForwardToGameMaster(this);
        }

		public override void Handle(IGameMaster gameMaster)
		{
            gameMaster.LogEvent("AuthorizeKnowledgeExchange", playerGuid);
            var response = GenerateResponse(gameMaster);
            if (response is RejectKnowledgeExchange)
            {
                gameMaster.SendMessage(response);
            }
            else
            {
                gameMaster.ScheduleJob(response, Delay);
            }
        }

        protected override Message GenerateResponse(IGameMaster gameMaster)
		{
            return gameMaster.PlayerAuthorizeKnowledgeExchange(playerGuid, withPlayerId);
        }
	}
}