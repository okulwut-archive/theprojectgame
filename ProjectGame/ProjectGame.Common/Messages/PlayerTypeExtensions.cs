﻿namespace ProjectGame.Common.Messages
{
    public static class PlayerTypeExtensions
    {
        public static PlayerType Other(this PlayerType type)
        {
            if (type == PlayerType.leader)
            {
                return PlayerType.member;
            }
            else
            {
                return PlayerType.leader;
            }
        }
    }
}
