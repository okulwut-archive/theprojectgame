namespace ProjectGame.Common.Messages
{
    public partial class GetGames : Message
    {
        public override void Handle(ICommunicationServer communicationServer, IAgent sender)
        {
            var response = GenerateResponse(communicationServer, sender);
            sender.SendMessage(response);
        }

        protected override Message GenerateResponse(ICommunicationServer communicationServer, IAgent sender)
        {
            var games = communicationServer.GetGames();
            return games;
        }
    }
}