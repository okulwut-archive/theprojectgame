using System.Linq;

namespace ProjectGame.Common.Messages
{
    public partial class AcceptExchangeRequest
    {
        public override void Handle(ICommunicationServer communicationServer, IAgent sender)
        {
            communicationServer.ForwardToPlayer(this);
        }

        public override void Handle(IPlayer player)
        {
            player.SendMessage(GenerateResponse(player));
        }

        protected override Message GenerateResponse(IPlayer player)
        {
            return player.SendBoard(senderPlayerId);
        }
    }
}