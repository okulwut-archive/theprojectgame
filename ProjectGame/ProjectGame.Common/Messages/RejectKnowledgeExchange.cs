namespace ProjectGame.Common.Messages
{
    public partial class RejectKnowledgeExchange
    {
        public override void Handle(ICommunicationServer communicationServer, IAgent sender)
        {
            communicationServer.ForwardToPlayer(this);
        }

        public override void Handle(IPlayer player)
        {
            player.ResponseReceived();
        }
    }
}