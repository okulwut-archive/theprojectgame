using System;

namespace ProjectGame.Common.Messages
{
    public partial class PickUpPiece
    {
		/// <summary>
		/// Delayed used by Game Master when handling message
		/// </summary>
		public static uint Delay { get; set; }

		public override void Handle(ICommunicationServer communicationServer, IAgent sender)
        {
            communicationServer.ForwardToGameMaster(this);
        }

		public override void Handle(IGameMaster gameMaster)
		{
            gameMaster.LogEvent("PickUpPiece", playerGuid);
            var response = GenerateResponse(gameMaster);
            if (response != null)
            {
                gameMaster.ScheduleJob(response, Delay);
            }
        }

		protected override Message GenerateResponse(IGameMaster gameMaster)
		{
            Data data = gameMaster.PlayerPickUpPiece(this.playerGuid);
            Data.TimestampMessage(data);
            return data;
        }
	}
}