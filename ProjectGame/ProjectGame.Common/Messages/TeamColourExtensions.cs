﻿namespace ProjectGame.Common.Messages
{
    public static class TeamColourExtensions
    {
        public static TeamColour Other(this TeamColour colour)
        {
            if (colour == TeamColour.red)
            {
                return TeamColour.blue;
            }
            else
            {
                return TeamColour.red;
            }
        }
    }
}
