﻿namespace ProjectGame.Common.Messages
{
	public partial class Location
	{
		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType())
				return false;

			var l = (Location)obj;
			return (x == l.x) && (y == l.y);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hash = 17;
				hash = hash * 23 + x.GetHashCode();
				hash = hash * 23 + y.GetHashCode();
				return hash;
			}
		}
	}
}
