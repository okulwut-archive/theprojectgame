﻿using System;
using System.Xml.Serialization;

namespace ProjectGame.Common.Messages
{
    /// <summary>
    ///     Base class for all messages.
    /// </summary>
    public abstract class Message
    {
        /// <summary>
        ///     Method for handling the message on the side of the communication server.
        /// </summary>
        /// <param name="communicationServer">Communicatioon server instance.</param>
        /// <param name="sender">The agent that sent in the message.</param>
        public virtual void Handle(ICommunicationServer communicationServer, IAgent sender)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        ///     Method for handling the message on the side of the player.
        /// </summary>
        /// <param name="player">Player instance to handle message on.</param>
        public virtual void Handle(IPlayer player)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        ///     Method for handling the message on the side of the game master.
        /// </summary>
        /// <param name="gameMaster">Game master instance to handle message on.</param>
        public virtual void Handle(IGameMaster gameMaster)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        ///     Generates a response from the communication server to the given message.
        /// </summary>
        /// <param name="communicationServer">Communicatioon server instance.</param>
        /// <param name="sender">The agent that sent in the message.</param>
        /// <returns>Response <see cref="Message" /> object.</returns>
        protected virtual Message GenerateResponse(ICommunicationServer communicationServer, IAgent sender)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        ///     Generates a response from the player to the given message.
        /// </summary>
        /// <param name="player">Player instance to handle message on.</param>
        /// <returns>Response <see cref="Message" /> object.</returns>
        protected virtual Message GenerateResponse(IPlayer player)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        ///     Generates a response from the game master to the given message.
        /// </summary>
        /// <param name="gameMaster">Game master instance to handle message on.</param>
        /// <returns>Response <see cref="Message" /> object.</returns>
        protected virtual Message GenerateResponse(IGameMaster gameMaster)
        {
            throw new NotSupportedException();
        }
    }

    public abstract partial class PlayerMessage : Message
    {
    }

    public abstract partial class GameMessage : Message
    {
    }
}