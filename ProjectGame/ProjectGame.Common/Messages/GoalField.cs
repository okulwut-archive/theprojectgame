﻿namespace ProjectGame.Common.Messages
{
	public partial class GoalField
	{
		public GoalField() { }

		public GoalField(GoalField field, GoalFieldType type)
		{
			this.x = field.x;
			this.y = field.y;
			this.playerId = field.playerId;
			this.playerIdSpecified = field.playerIdSpecified;
			this.team = field.team;
			this.type = type;
		}
	}
}
