﻿namespace ProjectGame.Common.Messages
{
    public partial class GameMasterDisconnected : Message
    {
        public GameMasterDisconnected() { }

        public GameMasterDisconnected(ulong gameId)
        {
            this.gameId = gameId;
        }

        public override void Handle(IPlayer player)
        {
            player.GameFinished();
        }
    }
}
