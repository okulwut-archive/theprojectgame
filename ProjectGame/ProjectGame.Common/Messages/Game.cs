namespace ProjectGame.Common.Messages
{
    public partial class Game
    {
        public override void Handle(ICommunicationServer communicationServer, IAgent sender)
        {
            communicationServer.ForwardToPlayer(this);
        }

        public override void Handle(IPlayer player)
        {
            foreach (var p in Players)
            {
                player.AddPlayer(p);
            }

            player.CreateBoard(Board.tasksHeight, Board.goalsHeight, Board.width, PlayerLocation);
			player.ResponseReceived();
        }
    }
}