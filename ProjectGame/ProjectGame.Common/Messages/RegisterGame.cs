﻿using ProjectGame.Common.Exceptions;
using ProjectGame.Common.Logging;

namespace ProjectGame.Common.Messages
{
    public partial class RegisterGame : Message
    {
        public override void Handle(ICommunicationServer communicationServer, IAgent sender)
        {
            try
            {
                var response = GenerateResponse(communicationServer, sender);
                sender.SendMessage(response);
            }
            catch (CannotCompleteRequestException ex)
            {
                Log.Game.Info(ex.Message);
				sender.SendMessage(new RejectGameRegistration() { gameName = this.NewGameInfo.gameName });
            }
        }

        protected override Message GenerateResponse(ICommunicationServer communicationServer, IAgent sender)
        {
            var gameId = communicationServer.RegisterGame(this, sender);
            return new ConfirmGameRegistration {gameId = gameId};
        }
    }
}