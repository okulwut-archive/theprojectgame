using ProjectGame.Common.Logging;

namespace ProjectGame.Common.Messages
{
    public partial class ConfirmJoiningGame
    {
        public override void Handle(ICommunicationServer communicationServer, IAgent sender)
        {
            communicationServer.AddPlayerToGame(gameId, PlayerDefinition.team, playerId);
            communicationServer.ForwardToPlayer(this);
        }

        public override void Handle(IPlayer player)
        {
            player.SetGame(playerId, gameId, privateGuid, PlayerDefinition.team, PlayerDefinition.type);
            Log.Game.Info($"Registered on server with ID {playerId}");
        }
    }
}