﻿namespace ProjectGame.Common.Messages
{
    public partial class GameStarted : Message
    {
        public override void Handle(ICommunicationServer communicationServer, IAgent sender)
        {
            communicationServer.GameStarted(gameId);
        }
    }
}