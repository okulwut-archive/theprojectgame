using ProjectGame.Common.Exceptions;
using ProjectGame.Common.Logging;

namespace ProjectGame.Common.Messages
{
    public partial class JoinGame : Message
    {
        public override void Handle(ICommunicationServer communicationServer, IAgent sender)
        {
            try
            {
                communicationServer.ForwardJoinGameRequest(this, sender);
            }
            catch (CannotCompleteRequestException ex)
            {
                Log.Game.Info(ex.Message);
            }
        }

        public override void Handle(IGameMaster gameMaster)
        {
            Log.Protocol.Info($"New player {playerId}");
            Player p = new Player
            {
                id = playerId,
                team = preferredTeam,
                type = preferredRole
            };

            gameMaster.AddPlayer(p);
        }
    }
}