﻿using ProjectGame.Common.Logging;

namespace ProjectGame.Common.Messages
{
    public partial class PlayerDisconnected : Message
    {
        public PlayerDisconnected() { }

        public PlayerDisconnected(ulong playerId)
        {
            this.playerId = playerId;
        }

		public override void Handle(IGameMaster gameMaster)
		{
			Log.Debug($"Player with id {playerId} disconnected from the game");
            gameMaster.LogEvent("Disconnected", playerId);
            gameMaster.RemovePlayer(playerId);
		}
	}
}
