using System;

namespace ProjectGame.Common.Messages
{
    public partial class KnowledgeExchangeRequest
    {
		public override void Handle(ICommunicationServer communicationServer, IAgent sender)
        {
            communicationServer.ForwardToPlayer(this);
        }

        public override void Handle(IPlayer player)
        {
            var response = GenerateResponse(player);
            player.SendMessage(response);
            if (response is Data)
            {
				player.AcceptKnowledgeExchange();
                player.SendMessage(player.AuthorizeKnowledgeExchange(senderPlayerId));
            }
        }

        protected override Message GenerateResponse(IPlayer player)
        {
			if (player.ShouldAcceptKnowledgeExchange(this.senderPlayerId))
			{
				return player.SendBoard(this.senderPlayerId);
			}
			else
			{
				return player.RejectKnowledgeExchange(this.senderPlayerId);
			}
        }
    }
}