
using ProjectGame.Common.Logging;
using System.Linq;

namespace ProjectGame.Common.Messages
{
    public partial class RegisteredGames : Message
    {
        public override void Handle(IPlayer player)
        {
            Log.Game.Debug("Start of registeredGames handling");
            bool found = false;

            if (GameInfo != null)
            {
                found = (GameInfo.Any(g => g.gameName == player.GameName));
            }

            if (found)
            {
                Log.Game.Info("Found desired game, registering");
                player.Register();
            }
            else
            {
                Log.Game.Info("Desired game not found, not giving up...");
                player.RetryGetGames();
            }
        }
    }
}