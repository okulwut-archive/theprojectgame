using System.Threading;
using ProjectGame.Common.Logging;

namespace ProjectGame.Common.Messages
{
    public partial class Data
    {
        public override void Handle(ICommunicationServer communicationServer, IAgent sender)
        {
            communicationServer.ForwardToPlayer(this);
            if (gameFinished)
            {
                communicationServer.RemovePlayerFromGame(playerId, sender.Id);
            }
        }

        public override void Handle(IPlayer player)
        {
			player.UpdateBoard(this);
			player.ResponseReceived();
			if(gameFinished)
			{
				player.GameFinished();
			}
        }

        static public void TimestampMessage(Data data)
        {
            if (data?.TaskFields != null)
            {
                foreach (TaskField field in data.TaskFields)
                {
                    field.timestamp = DateTimeFormatter.FormatDateForMessage();
                }
            }
            if (data?.GoalFields != null)
            {
                foreach (GoalField field in data.GoalFields)
                {
                    field.timestamp = DateTimeFormatter.FormatDateForMessage();
                }
            }
            if (data?.Pieces != null)
            {
                foreach (Piece piece in data.Pieces)
                {
                    piece.timestamp = DateTimeFormatter.FormatDateForMessage();
                }
            }
        }
    }
}