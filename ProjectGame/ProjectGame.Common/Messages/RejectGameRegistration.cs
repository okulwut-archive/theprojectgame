using ProjectGame.Common.Logging;

namespace ProjectGame.Common.Messages
{
    public partial class RejectGameRegistration : Message
    {
        public override void Handle(IGameMaster gameMaster)
        {
            Log.Game.Info("Game registration denied.");
            gameMaster.RetryRegistration();
        }
    }
}