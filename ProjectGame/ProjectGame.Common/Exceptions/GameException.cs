﻿using System;

namespace ProjectGame.Common.Exceptions
{
    /// <summary>
    ///     General abstract class for exceptions thrown during the cause of the game.
    /// </summary>
    public abstract class GameException : Exception
    {
        public GameException(string message) : base(message)
        {
        }

        public GameException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}