﻿using System;

namespace ProjectGame.Common.Exceptions
{
    /// <summary>
    ///     Exception class used for protocol-related errors.
    /// </summary>
    public class ProtocolException : Exception
    {
        public ProtocolException(string message) : base(message)
        {
        }

        public ProtocolException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}