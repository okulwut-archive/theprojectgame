﻿using System;

namespace ProjectGame.Common.Exceptions
{
    /// <summary>
    ///     Thrown when a request from a player cannot be completed.
    /// </summary>
    public class CannotCompleteRequestException : GameException
    {
        public CannotCompleteRequestException(string message) : base($"Cannot complete the received request: {message}")
        {
        }

        public CannotCompleteRequestException(string message, Exception innerException)
            : base($"Cannot complete the received request: {message}", innerException)
        {
        }

        public CannotCompleteRequestException(Exception exception) : base(exception.Message)
        {
        }
    }
}