﻿using System;

namespace ProjectGame.Common.Exceptions
{
    /// <summary>
    ///     Thrown when a game or a player is not found.
    /// </summary>
    public class NotFoundException : GameException
    {
        public NotFoundException(string message) : base(message)
        {
        }

        public NotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}