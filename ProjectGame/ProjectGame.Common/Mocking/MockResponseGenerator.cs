﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectGame.Common.Messages;

namespace ProjectGame.Common.Mocking
{
    public class MockResponseGenerator
    {
        private readonly Dictionary<Type, IResponseTypeGenerator> _responseGenerators;

        public MockResponseGenerator(IEnumerable<IResponseTypeGenerator> responseGenerators)
        {
            _responseGenerators = responseGenerators.ToDictionary(generator => generator.RequestType);
        }

        public Message GenerateResponse(Message request)
        {
            return _responseGenerators[request.GetType()].GenerateResponse(request);
        }
    }
}