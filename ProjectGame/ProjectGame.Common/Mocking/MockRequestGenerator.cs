﻿using System.Collections.Generic;
using ProjectGame.Common.Messages;

namespace ProjectGame.Common.Mocking
{
    public class MockRequestGenerator
    {
        private readonly List<IRequestTypeGenerator> _requestGenerators;
        private int _counter;

        public MockRequestGenerator(IEnumerable<IRequestTypeGenerator> requestGenerators)
        {
            _requestGenerators = new List<IRequestTypeGenerator>(requestGenerators);
            _counter = 0;
        }

        public Message NextMessage(ulong playerId)
        {
            _counter = (_counter + 1) % _requestGenerators.Count;
            return _requestGenerators[_counter].GenerateRequest(playerId);
        }
    }
}