﻿using System;
using ProjectGame.Common.Messages;

namespace ProjectGame.Common.Mocking
{
    public interface IResponseTypeGenerator
    {
        Type RequestType { get; }
        Message GenerateResponse(Message request);
    }
}