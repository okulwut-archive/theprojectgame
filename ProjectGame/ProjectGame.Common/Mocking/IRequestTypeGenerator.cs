﻿using ProjectGame.Common.Messages;

namespace ProjectGame.Common.Mocking
{
    public interface IRequestTypeGenerator
    {
        Message GenerateRequest(ulong playerId);
    }
}