﻿using System.Collections.Generic;
using ProjectGame.Common.Messages;

namespace ProjectGame.Common
{
    public interface IPlayer
    {
        /// <summary>
        ///     The identification number of the agent.
        /// </summary>
        ulong Id { get; set; }

        /// <summary>
        ///     Sends a message to the given agent instance.
        /// </summary>
        /// <param name="message"><see cref="Message" /> to be sent to the agent.</param>
        void SendMessage(Message message);

        /// <summary>
        ///     Sets a player's info about the game and their information.
        /// </summary>
        void SetGame(ulong playerId, ulong gameId, string guid, TeamColour team, PlayerType type);

        /// <summary>
        ///     Registers a player with the desired game
        /// </summary>
        void Register();

        /// <summary>
        ///     Gets games from the CS
        /// </summary>
        void GetGames();

        /// <summary>
        ///     Tries to find the game after a preconfigured interval
        /// </summary>
        void RetryGetGames();

        string GameName { get; }

        ulong GameId { get; }

        string Guid { get; }

        TeamColour Team { get; }
        PlayerType Type { get; }

        IAgent GameMaster { get; }

        IEnumerable<IAgent> Players { get; }

        /// <summary>
        ///     Adds another player to this player's internal list
        /// </summary>
        void AddPlayer(Player p);

        /// <summary>
        ///     Initializes a board inside the player
        /// </summary>
        void CreateBoard(uint tasksHeight, uint goalsHeight, uint width, Location location);

		void ResponseReceived();

		void UpdateBoard(Data data);

        Data SendBoard(ulong targetId);

        RejectKnowledgeExchange RejectKnowledgeExchange(ulong targetId);

        AuthorizeKnowledgeExchange AuthorizeKnowledgeExchange(ulong targetId);

        void GameFinished();

		bool ShouldAcceptKnowledgeExchange(ulong playerId);

		void AcceptKnowledgeExchange();
    }
}