﻿using ProjectGame.Common.Messages;

namespace ProjectGame.Common
{
    /// <summary>
    ///     Interface for instances of the communication server.
    /// </summary>
    public interface ICommunicationServer
    {
        /// <summary>
        ///     Forwards the supplied message to one of the connected Game Masters.
        /// </summary>
        /// <param name="message"><see cref="Message" /> to be forwarded.</param>
        void ForwardToGameMaster(GameMessage message);

        /// <summary>
        ///     Forwards the supplied message to one of the connected Players.
        /// </summary>
        /// <param name="message"><see cref="Message" /> to be forwarded.</param>
        void ForwardToPlayer(PlayerMessage message);

        /// <summary>
        ///     Broadcasts the message to all connected Players from the given game.
        /// </summary>
        /// <param name="message"><see cref="Message" /> to be broadcast.</param>
        /// <param name="gameId">Identification number of the game to send data to.</param>
        void BroadcastMessage(Message message, ulong gameId);

        /// <summary>
        ///     Registers a game on the server.
        /// </summary>
        /// <param name="message"><see cref="Messages.RegisterGame" /> message containing the game details.</param>
        /// <param name="gameMaster">The <see cref="IAgent" /> to be set as the game master for the game.</param>
        /// <returns></returns>
        ulong RegisterGame(RegisterGame message, IAgent gameMaster);

        /// <summary>
        ///     Forwards a <see cref="JoinGame" /> request to a game master.
        /// </summary>
        /// <param name="joinGame">The <see cref="JoinGame" /> request to forward.</param>
        /// <param name="sender">The original request sender.</param>
        void ForwardJoinGameRequest(JoinGame joinGame, IAgent sender);

        /// <summary>
        ///     Adds a player to one of the existing games.
        /// </summary>
        /// <param name="gameId">ID number of the game to join.</param>
        /// <param name="team">Team to add the player to.</param>
        /// <param name="player">ID number of the player to add.</param>
        void AddPlayerToGame(ulong gameId, TeamColour team, ulong player);

        /// <summary>
        ///     Gets a list of all games registered on the server.
        /// </summary>
        /// <returns>A <see cref="RegisteredGames" /> message to send.</returns>
        RegisteredGames GetGames();

        /// <summary>
        ///     Used to signal to the server that no more messages will be sent to the player with the supplied ID.
        /// </summary>
        /// <param name="playerId">The ID of the player.</param>
        /// <param name="gameMasterId">The ID of the game master that signaled the end of game.</param>
        void RemovePlayerFromGame(ulong playerId, ulong gameMasterId);

        /// <summary>
        ///     Notifies the communication server that a game has started and does not accept players.
        /// </summary>
        /// <param name="gameId">ID of the game which has started.</param>
        void GameStarted(ulong gameId);
    }
}