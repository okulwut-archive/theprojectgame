﻿using System;

namespace ProjectGame.Common
{
	/// <summary>
	/// Helper class to make all timestamps match the same pattern
	/// </summary>
	internal static class DateTimeFormatter
	{
		/// <summary>
		/// Creates timestamp, that always matches the same pattern
		/// </summary>
		/// <returns>DateTime object, to be used as timestamp</returns>
		internal static DateTime FormatDateForMessage()
		{
			var date = DateTime.Now;
			date = DateTime.SpecifyKind(date, DateTimeKind.Unspecified);
			date = date.AddTicks(-(date.Ticks % TimeSpan.TicksPerSecond));
			return date;
		}
	}
}
