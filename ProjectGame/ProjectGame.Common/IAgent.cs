﻿using ProjectGame.Common.Messages;
using System;

namespace ProjectGame.Common
{
    /// <summary>
    ///     Common interface for agents connecting to the server.
    /// </summary>
    public interface IAgent : IDisposable
    {
        /// <summary>
        ///     The identification number of the agent.
        /// </summary>
        ulong Id { get; set; }

        /// <summary>
        ///     Sends a message to the given agent instance.
        /// </summary>
        /// <param name="message"><see cref="Message" /> to be sent to the agent.</param>
        void SendMessage(Message message);
    }
}