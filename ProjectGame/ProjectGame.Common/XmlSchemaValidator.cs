﻿using ProjectGame.Common.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace ProjectGame.Common
{
    public class XmlSchemaValidator
    {
        /// <summary>
        ///     Schema set used for XML message validation.
        /// </summary>
        private readonly XmlSchemaSet _xmlSchemaSet;

        /// <summary>
        ///     Constructs a <see cref="XmlSchemaValidator" />.
        /// </summary>
        /// <param name="schemaPath">Filename from which the schema should be loaded from.</param>
        public XmlSchemaValidator(string schemaPath)
        {
            _xmlSchemaSet = new XmlSchemaSet();
            var assembly = Assembly.GetExecutingAssembly();
            var stream = assembly.GetManifestResourceStream(schemaPath);
            if (stream == null)
            {
                throw new Exception("The XSD resource stream was null.");
            }
            _xmlSchemaSet.Add(null, new XmlTextReader(stream));
        }

        /// <summary>
        ///     Validates an XML <see cref="XDocument" /> instance.
        /// </summary>
        public void Validate(XDocument xDoc)
        {
			xDoc.Validate(_xmlSchemaSet, null);
        }
    }
}
