﻿using ProjectGame.Common.Messages;

namespace ProjectGame.Common
{
	/// <summary>
	/// Interface for instances of Game Master
	/// </summary>
    public interface IGameMaster
    {
        /// <summary>
        ///     The identification number of the agent.
        /// </summary>
        ulong Id { get; }

        /// <summary>
        ///     Sends a message to the given agent instance.
        /// </summary>
        /// <param name="message"><see cref="Message" /> to be sent to the agent.</param>
        void SendMessage(Message message);

        /// <summary>
        /// Schedule job that adds message to outgoing queue
        /// </summary>
        /// <param name="message">Message to be added to queue</param>
        void ScheduleJob(Message message, uint delay);

        /// <summary>
        /// Registers the Game Master with the Communication Server
        /// </summary>
        void Register();

        /// <summary>
        /// Waits for a preconfigured period of time, then retries registering
        /// </summary>
        void RetryRegistration();

        /// <summary>
        /// Initializes the game inside the Game Master
        /// </summary>
        void CreateGame(ulong id);

        /// <summary>
        /// Adds a player to the Game Master's game
        /// </summary>
        void AddPlayer(Player player);

        /// <summary>
        /// Players' Discover action.
        /// </summary>
        Data PlayerDiscover(string playerGuid);

        /// <summary>
        /// Players' Move action.
        /// </summary>
        Data PlayerMove(string playerGuid, MoveType direction);

        /// <summary>
        /// Players' PickUpPiece action.
        /// </summary>
        Data PlayerPickUpPiece(string playerGuid);

        /// <summary>
        /// Players' PlacePiece action.
        /// </summary>
        Data PlayerPlacePiece(string playerGuid);

        /// <summary>
        /// Players' TestPiece action.
        /// </summary>
        Data PlayerTestPiece(string playerGuid);


        /// <summary>
        /// Players' AuthorizeKnowledgeExchange action.
        /// </summary>
        Message PlayerAuthorizeKnowledgeExchange(string playerGuid, ulong targetId);

        /// <summary>
        /// Removes player from Game Master's game
        /// </summary>
        /// <param name="playerId">Id of player to be removed</param>
        void RemovePlayer(ulong playerId);

        /// <summary>
		/// Logs an event in the CSV log
		/// </summary>
        /// <param name="name">Name of the event</param>
		/// <param name="guid">Guid of player</param>
        void LogEvent(string name, string guid);

        /// <summary>
		/// Logs an event in the CSV log
		/// </summary>
        /// <param name="name">Name of the event</param>
		/// <param name="id">Id of player</param>
        void LogEvent(string name, ulong id);
    }
}