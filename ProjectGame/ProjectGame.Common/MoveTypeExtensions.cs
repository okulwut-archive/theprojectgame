﻿using ProjectGame.Common.Messages;

namespace ProjectGame.Common
{
	public static class MoveTypeExtensions
	{
		public static MoveType Opposite(this MoveType type)
		{
			switch (type)
			{
				case MoveType.up:
					return MoveType.down;
				case MoveType.down:
					return MoveType.up;
				case MoveType.left:
					return MoveType.right;
				case MoveType.right:
					return MoveType.left;
				default:
					return MoveType.up;
			}
		}
	}
}
