﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using ProjectGame.Common.Messages;
using System.Linq;
using ProjectGame.Common.Logging;

namespace ProjectGame.Common
{
    /// <summary>
    ///     Class used to send data over a network stream. Encapsulates buffer and networking operations.
    /// </summary>
    public class CommunicationStream : IDisposable
    {
        /// <summary>
        ///     Allocated constant reading buffer size.
        /// </summary>
        private const int BufferSize = 1024;

        /// <summary>
        ///     Separator used to distinguish ends of messages. Byte 23 is used here, as per the specification.
        /// </summary>
        private const char EndOfTransmission = (char) 23;

        /// <summary>
        ///     XML schema resource path.
        /// </summary>
        private static readonly string SchemaPath = $"{typeof(Message).Namespace}.TheProjectGameCommunication.xsd";

        ///     Is this instance sending keep alives unsolicited.
        /// </summary>
        private readonly bool _keepAliveSource;

        /// <summary>
        ///     Delay between keep alives (in ms)
        /// </summary>
        private readonly uint _keepAliveDelay;

        /// <summary>
        ///     Task responsible for sending keep alives.
        /// </summary>
        private readonly Task _keepAliveTask;

        /// <summary>
        ///     The buffer which the newest received bytes were read to.
        /// </summary>
        private readonly char[] _buffer;

        /// <summary>
        ///     Network stream reader.
        /// </summary>
        private readonly StreamReader _streamReader;

        /// <summary>
        ///     Network stream writer.
        /// </summary>
        private readonly StreamWriter _streamWriter;

        /// <summary>
        ///     Mutex, to ensure _streamWriter is not flushed by keep alive thread while main thread is working.
        /// </summary>
        private readonly Mutex _streamWriterLock;

        /// <summary>
        /// Source of a cancellation token used to cancel async operations.
        /// </summary>
        private readonly CancellationTokenSource _cancellationTokenSource;

        /// <summary>
        ///     <see cref="StringBuilder" /> used for assembling messages from partial packets.
        /// </summary>
        private readonly StringBuilder _stringBuilder;

        /// <summary>
        ///     Schema validator used for XML message validation.
        /// </summary>
        private readonly XmlSchemaValidator _schemaValidator;

        /// <summary>
        ///     Constructs a <see cref="CommunicationStream" /> from a given <see cref="Stream" />. The created stream will store
        ///     the incoming messages in the supplied <see cref="BlockingCollection{T}" />.
        /// </summary>
        /// <param name="networkStream">The network stream to perform read/write operations on.</param>
        /// <param name="keepAliveSource">If true, the stream will produce keep alive messages, but not bounce them. If false, it will bounce, but not produce them.</param>
        /// <param name="keepAliveDelay">Delay in miliseconds between keep-alive bytes sent.</param>
        public CommunicationStream(Stream networkStream, bool keepAliveSource = false, uint keepAliveDelay = 5000)
        {
            _streamReader = new StreamReader(networkStream);
            _streamWriter = new StreamWriter(networkStream);
            _streamWriterLock = new Mutex();
            _stringBuilder = new StringBuilder();
            try
            {
                _schemaValidator = new XmlSchemaValidator(SchemaPath);
            }
            catch(Exception e)
            {
                Log.Error($"Schema message validation initialization failure\nReason: {e.Message}");
                throw;
            }
            _buffer = new char[BufferSize];
            _cancellationTokenSource = new CancellationTokenSource();
            _keepAliveSource = keepAliveSource;
            _keepAliveDelay = keepAliveDelay;

            if (keepAliveSource)
            {
                _keepAliveTask = SendKeepAlive(_cancellationTokenSource.Token);
            }
        }

        /// <summary>
        ///     Do not use. For testing purposes ONLY.
        /// </summary>
        public CommunicationStream()
        {
            
        }

        /// <summary>
        ///     Disposes of resources used by this stream instance.
        /// </summary>
        public void Dispose()
        {
            _cancellationTokenSource.Cancel();
            _streamReader?.Dispose();
            _streamWriter?.Dispose();
            _streamWriterLock?.Dispose();
        }

        /// <summary>
        ///     This task reads incoming data asynchronously, splits the bytes into full messages and places them in the message
        ///     queue.
        /// </summary>
        /// <returns>A task responsible for reading data.</returns>
        /// <param name="token"><see cref="CancellationToken"/> to be used to cancel asynchronous events.</param>
        /// <param name="onMessage">Action to be performed upon message arrival.</param>
        /// <param name="keepAliveAction">Action to be performed upon reading any ETB byte.</param>
        public async Task ReadMessages(CancellationToken token, Action<Message, CancellationToken> onMessage, Action keepAliveAction = null)
        {
            await Task.Yield();

            while (!_streamReader.EndOfStream)
            {
                Message message;
                var readBytes = await _streamReader.ReadAsync(_buffer, 0, BufferSize);
                if (token.IsCancellationRequested) break;
                if (readBytes == 0)
                {
                    return;
                }
                int messageChunkLength;
                while ((messageChunkLength = MessageInBuffer(out message)) > 0)
                {
                    readBytes -= messageChunkLength;
                    keepAliveAction?.Invoke();
                    if (!_keepAliveSource)
                    {
                        _streamWriterLock.WaitOne();
                        Log.Network.Debug("Bouncing keep alive");
                        EndTransmission();
                        _streamWriterLock.ReleaseMutex();
                    }
                    if (message == null)
                    {
                        continue;
                    }
                    onMessage(message, token);
                    
                }
                _stringBuilder.Append(_buffer, 0, readBytes);
            }
        }

        /// <summary>
        ///     Checks whether the buffer contains an end chunk of a message (a chunk terminated by
        ///     <see cref="EndOfTransmission" />). If the end of a message is found, the message is saved in an out parameter and
        ///     the buffer contents are moved
        ///     accordingly.
        ///     If a keep alive message is found, the out parameter is set to null.
        /// </summary>
        /// <param name="message">Out parameter indicating where the full message is to be stored.</param>
        /// <returns>
        ///     Returns the length of the extracted chunk, if the <see cref="EndOfTransmission" /> byte is found; otherwise
        ///     returns 0 (as no chunk has been extracted)
        /// </returns>
        private int MessageInBuffer(out Message message)
        {
            message = null;
            var eotIndex = Array.IndexOf(_buffer, EndOfTransmission);
            if (eotIndex < 0)
            {
                return 0;
            }
            _stringBuilder.Append(_buffer, 0, eotIndex);
            if (_stringBuilder.Length == 0)
            {
                Log.Network.Debug("Received keep alive");
                return ReorganizeBuffers(eotIndex);
            }
            message = DeserializeFromString(_stringBuilder.ToString());
            var chunkLength = ReorganizeBuffers(eotIndex);
            return chunkLength;
        }

        /// <summary>
        ///     Clears the contents of the buffer up to the supplied index, then moves buffer contents to the left and clears the
        ///     cells to the right of the copied buffer.
        /// </summary>
        /// <param name="index">The end index of the part to clear.</param>
        /// <returns>The number of bytes cleared.</returns>
        private int ReorganizeBuffers(int index)
        {
            var chunkLength = index + 1;
            Array.Clear(_buffer, 0, chunkLength);
            Array.Copy(_buffer, chunkLength, _buffer, 0, BufferSize - chunkLength);
            Array.Clear(_buffer, BufferSize - chunkLength, chunkLength);
            _stringBuilder.Clear();
            return chunkLength;
        }

        /// <summary>
        ///     Deserializes a XML string into a <see cref="Message" /> instance.
        /// </summary>
        /// <param name="messageString">XML string with the supplied message.</param>
        /// <returns>Deserialized <see cref="Message" /> objeect.</returns>
        private Message DeserializeFromString(string messageString)
        {
            var xdoc = XDocument.Load(new StringReader(messageString));
            Log.Protocol.Debug($"Message content:\n{messageString}");

            try
            {
                _schemaValidator.Validate(xdoc);
            }
            catch (Exception e)
            {
                Log.Error($"Schema message validation failure for message:\n{messageString}\nReason: {e.Message}");
                throw new Exception("Schema message validation failure",e);
            }
            
			var serializer = XmlSerializerFactory.GetDeserializer(messageString);
            using (var reader = new StringReader(messageString))
            {
                return (Message) serializer.Deserialize(reader);
            }
        }

        /// <summary>
        ///     Sends a <see cref="Message" /> object over the stream.
        /// </summary>
        /// <param name="message">The <see cref="Message" /> to send.</param>
        public virtual void SendMessage(Message message)
        {
            if (message == null)
                return;
            var rootAttributes = (XmlRootAttribute[])Attribute.GetCustomAttributes(message.GetType(), typeof(XmlRootAttribute));

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces(new[]
            {
                new XmlQualifiedName(string.Empty, rootAttributes.First().Namespace)
            });
            _streamWriterLock.WaitOne();
            try
            {
                XmlSerializerFactory.GetSerializer(message).Serialize(_streamWriter, message, ns);
                EndTransmission();
            }
            catch (IOException)
            {
                Log.Protocol.Info("The agent has disconnected during message serialization");
            }
            _streamWriterLock.ReleaseMutex();

        }

        /// <summary>
        ///     Sends a keep alive message every second.
        /// </summary>
        private async Task SendKeepAlive(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                _streamWriterLock.WaitOne();
                Log.Network.Debug("Sending keep alive");
                EndTransmission();
                _streamWriterLock.ReleaseMutex();
                await Task.Delay(TimeSpan.FromMilliseconds(_keepAliveDelay), token);
            }
        }

        /// <summary>
        ///     Terminates and sends the message.
        /// </summary>
        private void EndTransmission()
        {
            _streamWriter.Write(EndOfTransmission);
            _streamWriter.Flush();
        }
    }
}