﻿using NUnit.Framework;
using ProjectGame.Common;
using ProjectGame.Common.Messages;
using ProjectGame.GameMaster;
using Moq;
using System.Collections.Concurrent;
using Quartz;
using System;

namespace ProjectGame.Test.GameMaster
{
    [TestFixture, Description("Unit Tests for GameMaster class")]
    public class GameMasterTests
    {
        [Test, Description("GameMaster SendMessage method test")]
        public void SendMessageTest()
        {
            var clientMock = new Mock<CommunicationClient>(new BlockingCollection<Message>(), null,null);
            var message = new Data();
            clientMock.Setup(cl => cl.SendMessage(It.Is<Message>(msg => msg == message)));
            var gameMock = new Mock<ProjectGame.GameMaster.Game>(new Exception());
            gameMock.Setup(g => g.NoteMessageSent(It.IsAny<ulong>()));
            
            var gm = new ProjectGame.GameMaster.GameMaster(null, null, null, clientMock.Object,null,gameMock.Object);
            gm.SendMessage(message);
            clientMock.Verify(c => c.SendMessage(It.IsAny<Message>()));
            gameMock.Verify(g => g.NoteMessageSent(It.IsAny<ulong>()));
        }

        [Test, Description("GameMaster Register method test")]
        public void RegisterTest()
        {
            var clientMock = new Mock<CommunicationClient>(new BlockingCollection<Message>(),null,null);
            var message = new RegisterGame()
            {
                NewGameInfo = new GameInfo()
                {
                    blueTeamPlayers = (ulong)GameMasterConfiguration.NumberOfPlayersPerTeam,
                    redTeamPlayers = (ulong)GameMasterConfiguration.NumberOfPlayersPerTeam,
                    gameName = GameMasterConfiguration.GameName
                }
            };
            clientMock.Setup(cl => cl.SendMessage(It.Is<RegisterGame>(
                                msg => msg.NewGameInfo.blueTeamPlayers == message.NewGameInfo.blueTeamPlayers &&
                                       msg.NewGameInfo.redTeamPlayers == message.NewGameInfo.blueTeamPlayers &&
                                       msg.NewGameInfo.gameName == message.NewGameInfo.gameName)));
            var gm = new ProjectGame.GameMaster.GameMaster(null, null, null, clientMock.Object,null,null);
            gm.Register();
            clientMock.Verify(c => c.SendMessage(It.IsAny<Message>()));
        }

        [Test, Description("GameMaster SheduleJob method test")]
        [TestCase(1000)]
        [TestCase(500)]
        [TestCase(200)]
        [TestCase(100)]
        [TestCase(50)]
        [TestCase(10)]
        public void SheduleJobTest(int accuracy)
        {
            uint delay = 1000*60*60;
            long ticks = DateTime.UtcNow.Ticks+TimeSpan.FromMilliseconds(delay).Ticks;
            var date = DateTime.UtcNow;
            date=date.AddHours(1);
            var schedulerMock = new Mock<IScheduler>();
            schedulerMock.Setup(s => s.ScheduleJob(It.IsAny<IJobDetail>(), It.Is<ITrigger>(t => TimeSpan.FromTicks(Math.Abs(t.StartTimeUtc.UtcDateTime.Ticks- date.Ticks)).Milliseconds < accuracy)));
            var clientMock = new Mock<CommunicationClient>(new BlockingCollection<Message>(), null, null);
            var gm = new ProjectGame.GameMaster.GameMaster(null, new BlockingCollection<Message>(), null, clientMock.Object,schedulerMock.Object,null);
            gm.ScheduleJob(new Data(), delay);
            schedulerMock.Verify(s => s.ScheduleJob(It.IsAny<IJobDetail>(), It.Is<ITrigger>(t => TimeSpan.FromTicks(Math.Abs(t.StartTimeUtc.UtcDateTime.Ticks - date.Ticks)).Milliseconds < accuracy)));
        }

    }
}