﻿using Moq;
using NUnit.Framework;
using ProjectGame.Common.Messages;
using ProjectGame.GameMaster;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace ProjectGame.Test.GameMaster
{
    [TestFixture, Description("Unit Tests for Game class")]
    public class GameTests
    {
        [Test, Description("Game Full method test")]
        public void FullTest()
        {
            var _players = new Dictionary<TeamColour, List<ProjectGame.GameMaster.Player>>();
            var redPlayers = new List<ProjectGame.GameMaster.Player>();
            var bluePlayers = new List<ProjectGame.GameMaster.Player>();
            redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red, type: PlayerType.leader));
            bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue, type: PlayerType.leader));
            for (int i = 0; i < 4; i++)
                redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red));
            for (int i = 0; i < 3; i++)
                bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue));
            _players.Add(TeamColour.blue, bluePlayers);
            _players.Add(TeamColour.red, redPlayers);
            var maxcount = new Dictionary<TeamColour, int>();
            maxcount.Add(TeamColour.blue, 5);
            maxcount.Add(TeamColour.red, 5);
            var leaderPresent = new Dictionary<TeamColour, bool>();
            leaderPresent.Add(TeamColour.blue, true);
            leaderPresent.Add(TeamColour.red, true);
            var g = new ProjectGame.GameMaster.Game(_players, maxcount, leaderPresent, null, null, 0);
            Assert.AreEqual(g.Full, false);
            bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue));
            Assert.AreEqual(g.Full, true);

        }
        [Test, Description("Game RegisterPlayer method test")]
        public void RegisterPlayerTest()
        {
            var _players = new Dictionary<TeamColour, List<ProjectGame.GameMaster.Player>>();
            var redPlayers = new List<ProjectGame.GameMaster.Player>();
            var bluePlayers = new List<ProjectGame.GameMaster.Player>();
            redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red, type: PlayerType.leader));
            bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue, type: PlayerType.leader));
            for (int i = 0; i < 4; i++)
                redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red));
            for (int i = 0; i < 3; i++)
                bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue));
            _players.Add(TeamColour.blue, bluePlayers);
            _players.Add(TeamColour.red, redPlayers);
            var maxcount = new Dictionary<TeamColour, int>();
            maxcount.Add(TeamColour.blue, 5);
            maxcount.Add(TeamColour.red, 5);
            var leaderPresent = new Dictionary<TeamColour, bool>();
            leaderPresent.Add(TeamColour.blue, true);
            leaderPresent.Add(TeamColour.red, true);
            var g = new ProjectGame.GameMaster.Game(_players, maxcount, leaderPresent, null, null, 0);
            Assert.IsTrue(g.RegisterPlayer(
                                            new ProjectGame.Common.Messages.Player()
                                            {
                                                team = TeamColour.red,
                                                type = PlayerType.leader
                                            }
                                           ) is ConfirmJoiningGame);
            Assert.AreEqual(bluePlayers.Count, 5);
            Assert.IsTrue(g.RegisterPlayer(
                                            new ProjectGame.Common.Messages.Player()
                                            {
                                                team = TeamColour.red,
                                                type = PlayerType.leader
                                            }
                                           ) is RejectJoiningGame);

        }
        [Test, Description("Game GetPlayerByGuid method test")]
        public void GetPlayerByGuidTest()
        {
            var guid = new System.Guid(1, 2, 3, 4, 5, 4, 3, 2, 1, 2, 3);
            var _players = new Dictionary<TeamColour, List<ProjectGame.GameMaster.Player>>();
            var redPlayers = new List<ProjectGame.GameMaster.Player>();
            var bluePlayers = new List<ProjectGame.GameMaster.Player>();
            redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red, type: PlayerType.leader, guid: guid));
            bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue, type: PlayerType.leader));
            for (int i = 0; i < 4; i++)
                redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red));
            for (int i = 0; i < 3; i++)
                bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue));
            _players.Add(TeamColour.blue, bluePlayers);
            _players.Add(TeamColour.red, redPlayers);
            var maxcount = new Dictionary<TeamColour, int>();
            maxcount.Add(TeamColour.blue, 5);
            maxcount.Add(TeamColour.red, 5);
            var leaderPresent = new Dictionary<TeamColour, bool>();
            leaderPresent.Add(TeamColour.blue, true);
            leaderPresent.Add(TeamColour.red, true);
            var g = new ProjectGame.GameMaster.Game(_players, maxcount, leaderPresent, null, null, 0);
            Assert.AreEqual(redPlayers[0], g.GetPlayerByGuid(guid.ToString()));
            Assert.IsNull(g.GetPlayerByGuid("TEST"));
        }

        [Test, Description("Game RandomPiece method test")]
        public void RandomPieceTest()
        {
            var _players = new Dictionary<TeamColour, List<ProjectGame.GameMaster.Player>>();
            var redPlayers = new List<ProjectGame.GameMaster.Player>();
            var bluePlayers = new List<ProjectGame.GameMaster.Player>();
            redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red, type: PlayerType.leader));
            bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue, type: PlayerType.leader));
            for (int i = 0; i < 4; i++)
                redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red));
            for (int i = 0; i < 3; i++)
                bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue));
            _players.Add(TeamColour.blue, bluePlayers);
            _players.Add(TeamColour.red, redPlayers);
            var maxcount = new Dictionary<TeamColour, int>();
            maxcount.Add(TeamColour.blue, 5);
            maxcount.Add(TeamColour.red, 5);
            var leaderPresent = new Dictionary<TeamColour, bool>();
            leaderPresent.Add(TeamColour.blue, true);
            leaderPresent.Add(TeamColour.red, true);
            var boardMock = new Mock<Board>(new Exception());
            boardMock.Setup(b => b.Left).Returns(0);
            boardMock.Setup(b => b.TaskBottom).Returns(0);
            boardMock.Setup(b => b.TaskTop).Returns(10);
            boardMock.Setup(b => b.Right).Returns(10);
            var fields = new Field[10, 10];
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                {
                    fields[i, j] = new TaskField() { pieceIdSpecified = i < 3 };
                }
            boardMock.Setup(b => b.Fields).Returns(fields);
            var g = new ProjectGame.GameMaster.Game(_players, maxcount, leaderPresent, null, null, 0, boardMock.Object);
            int count = 0;
            while (g.RandomPiece() && count < 10)
            {
                count++;
            }
            Assert.AreEqual(10, count);
        }

        [Test, Description("Game AddPiece method test")]
        public void AddPieceTest()
        {
            GameMasterConfiguration.GoalAreaLength = 10;
            GameMasterConfiguration.TaskAreaLength = 10;
            GameMasterConfiguration.BoardWidth = 10;
            var g = new ProjectGame.GameMaster.Game(new Exception());
            var loc = new TaskField
            {
                y = (uint) (GameMasterConfiguration.GoalAreaLength + GameMasterConfiguration.TaskAreaLength / 2),
                x = (uint) (GameMasterConfiguration.BoardWidth / 2)
            };
            g.AddPiece(loc);

            Assert.IsTrue(loc.pieceIdSpecified);
        }

        [Test, Description("Game UpdateNeighborPiece method test")]
        public void UpdateNeighborPieceTest()
        {
            GameMasterConfiguration.GoalAreaLength = 10;
            GameMasterConfiguration.TaskAreaLength = 10;
            GameMasterConfiguration.BoardWidth = 10;
            var g = new ProjectGame.GameMaster.Game(new Exception());
            var loc = new Location();
            loc.y = (uint)(GameMasterConfiguration.GoalAreaLength + GameMasterConfiguration.TaskAreaLength / 2);
            loc.x = (uint)(GameMasterConfiguration.BoardWidth / 2);
            (g.Board.Fields[loc.y, loc.x] as TaskField).pieceIdSpecified = true;
            g.UpdateNeighborPiece();
            Assert.AreEqual(0, (g.Board.Fields[loc.y, loc.x] as TaskField).distanceToPiece);
            Assert.AreEqual(1, (g.Board.Fields[loc.y - 1, loc.x] as TaskField).distanceToPiece);
            (g.Board.Fields[loc.y, loc.x] as TaskField).pieceIdSpecified = false;
            g.UpdateNeighborPiece();
            Assert.AreEqual(-1, (g.Board.Fields[loc.y, loc.x] as TaskField).distanceToPiece);
            Assert.AreEqual(-1, (g.Board.Fields[loc.y - 1, loc.x] as TaskField).distanceToPiece);
        }

        [Test, Description("Game SendGameFinish method test")]
        public void SendGameFinishTest()
        {
            var _players = new Dictionary<TeamColour, List<ProjectGame.GameMaster.Player>>();
            var redPlayers = new List<ProjectGame.GameMaster.Player>();
            var bluePlayers = new List<ProjectGame.GameMaster.Player>();
            redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red, type: PlayerType.leader));
            bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue, type: PlayerType.leader));
            for (int i = 0; i < 4; i++)
                redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red));
            for (int i = 0; i < 3; i++)
                bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue));
            _players.Add(TeamColour.blue, bluePlayers);
            _players.Add(TeamColour.red, redPlayers);
            var maxcount = new Dictionary<TeamColour, int>();
            maxcount.Add(TeamColour.blue, 5);
            maxcount.Add(TeamColour.red, 5);
            var leaderPresent = new Dictionary<TeamColour, bool>();
            leaderPresent.Add(TeamColour.blue, true);
            leaderPresent.Add(TeamColour.red, true);
            var boardMock = new Mock<Board>(new Exception());
            boardMock.Setup(b => b.Fields).Returns(new Field[0, 0]);
            var gameMasterMock = new Mock<ProjectGame.GameMaster.GameMaster>
                                    (new BlockingCollection<Message>(), null, null, null,null,null);
            int counter = 0;
            gameMasterMock.Setup(gm => gm.SendMessage(It.IsAny<Message>())).Callback(() => counter++);
            var g = new ProjectGame.GameMaster.Game(_players, maxcount, leaderPresent, gameMasterMock.Object, null, 0, boardMock.Object);
            g.SendGameFinished();
            Assert.AreEqual(9, counter);
        }

        [Test, Description("Game PlayerDiscover method test")]
        public void PlayerDiscoverTest()
        {
            var guid = new Guid(1, 2, 3, 4, 5, 4, 3, 2, 1, 2, 3);
            var _players = new Dictionary<TeamColour, List<ProjectGame.GameMaster.Player>>();
            var redPlayers = new List<ProjectGame.GameMaster.Player>();
            var bluePlayers = new List<ProjectGame.GameMaster.Player>();
            var loc = new Location();
            loc.x = loc.y = 5;
            redPlayers.Add(new ProjectGame.GameMaster.Player(loc, team: TeamColour.red, type: PlayerType.leader, guid: guid));
            bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue, type: PlayerType.leader));
            for (int i = 0; i < 4; i++)
                redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red));
            for (int i = 0; i < 3; i++)
                bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue));
            _players.Add(TeamColour.blue, bluePlayers);
            _players.Add(TeamColour.red, redPlayers);
            var maxcount = new Dictionary<TeamColour, int>();
            maxcount.Add(TeamColour.blue, 5);
            maxcount.Add(TeamColour.red, 5);
            var leaderPresent = new Dictionary<TeamColour, bool>();
            leaderPresent.Add(TeamColour.blue, true);
            leaderPresent.Add(TeamColour.red, true);
            var boardMock = new Mock<Board>(new Exception());
            boardMock.Setup(b => b.Left).Returns(0);
            boardMock.Setup(b => b.Right).Returns(10);
            var teamBottom = new Dictionary<TeamColour, uint>();
            var teamTop = new Dictionary<TeamColour, uint>();
            teamBottom.Add(TeamColour.blue, 0);
            teamBottom.Add(TeamColour.red, 0);
            teamTop.Add(TeamColour.blue, 6);
            teamTop.Add(TeamColour.red, 6);
            boardMock.Setup(b => b.TeamBottom).Returns(teamBottom);
            boardMock.Setup(b => b.TeamTop).Returns(teamTop);
            var fields = new Field[10, 10];
            for(int i=0;i<10;i++)
                for(int j = 0; j < 10; j++)
                {
                    fields[i, j] = i == 4 ?(Field) new GoalField() : (Field) new TaskField();
                }
            boardMock.Setup(b => b.Fields).Returns(fields);
            var g = new ProjectGame.GameMaster.Game(_players, maxcount, leaderPresent, null, null, 0,boardMock.Object);
            var data = g.PlayerDiscover(guid.ToString());
            Assert.AreEqual(3, data.GoalFields.Length);
            Assert.AreEqual(3, data.TaskFields.Length);
            Assert.AreEqual(0, data.Pieces.Length);
        }

        [Test, Description("Game PlayerMove method test")]
        [TestCase(MoveType.down)]
        [TestCase(MoveType.right)]
        [TestCase(MoveType.left)]
        [TestCase(MoveType.up)]
        public void PlayerMoveTest(MoveType direction)
        {
            var guid = new Guid(1, 2, 3, 4, 5, 4, 3, 2, 1, 2, 3);
            var _players = new Dictionary<TeamColour, List<ProjectGame.GameMaster.Player>>();
            var redPlayers = new List<ProjectGame.GameMaster.Player>();
            var bluePlayers = new List<ProjectGame.GameMaster.Player>();
            var loc = new Location();
            loc.x = 0;
            loc.y = 5;
            redPlayers.Add(new ProjectGame.GameMaster.Player(loc, team: TeamColour.red, type: PlayerType.leader, guid: guid));
            bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue, type: PlayerType.leader));
            for (int i = 0; i < 4; i++)
                redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red));
            for (int i = 0; i < 3; i++)
                bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue));
            _players.Add(TeamColour.blue, bluePlayers);
            _players.Add(TeamColour.red, redPlayers);
            var maxcount = new Dictionary<TeamColour, int>();
            maxcount.Add(TeamColour.blue, 5);
            maxcount.Add(TeamColour.red, 5);
            var leaderPresent = new Dictionary<TeamColour, bool>();
            leaderPresent.Add(TeamColour.blue, true);
            leaderPresent.Add(TeamColour.red, true);
            var boardMock = new Mock<Board>(new Exception());
            boardMock.Setup(b => b.Left).Returns(0);
            boardMock.Setup(b => b.Right).Returns(10);
            var teamBottom = new Dictionary<TeamColour, uint>();
            var teamTop = new Dictionary<TeamColour, uint>();
            teamBottom.Add(TeamColour.blue, 0);
            teamBottom.Add(TeamColour.red, 0);
            teamTop.Add(TeamColour.blue, 6);
            teamTop.Add(TeamColour.red, 6);
            boardMock.Setup(b => b.TeamBottom).Returns(teamBottom);
            boardMock.Setup(b => b.TeamTop).Returns(teamTop);
            var fields = new Field[10, 10];
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                {
                    fields[i, j] = i == 4 ? (Field)new GoalField() : (Field)new TaskField();
                }
            boardMock.Setup(b => b.Fields).Returns(fields);
            var g = new ProjectGame.GameMaster.Game(_players, maxcount, leaderPresent, null, null, 0, boardMock.Object);
            var data = g.PlayerMove(guid.ToString(), direction);
            switch (direction)
            {
                case MoveType.down:
                    Assert.AreEqual(1, data.GoalFields.Length);
                    Assert.IsNull(data.TaskFields);
                    break;
                case MoveType.left:
                    Assert.IsNull(data.TaskFields);
                    Assert.IsNull(data.GoalFields);
                    break;
                case MoveType.right:
                    Assert.AreEqual(1, data.TaskFields.Length);
                    Assert.IsNull(data.GoalFields);
                    break;
                case MoveType.up:
                    Assert.IsNull(data.TaskFields);
                    Assert.IsNull(data.GoalFields);
                    break;
            }
        }

        [Test, Description("Game PlayerPickUpPiece method test")]
        [TestCase(true)]
        [TestCase(false)]
        public void PlayerPickUpPiece(bool canPickUpPiece)
        {
            var guid = new Guid(1, 2, 3, 4, 5, 4, 3, 2, 1, 2, 3);
            var _players = new Dictionary<TeamColour, List<ProjectGame.GameMaster.Player>>();
            var redPlayers = new List<ProjectGame.GameMaster.Player>();
            var bluePlayers = new List<ProjectGame.GameMaster.Player>();
            var loc = new Location();
            loc.x = 0;
            loc.y = 5;
            redPlayers.Add(new ProjectGame.GameMaster.Player(loc, team: TeamColour.red, type: PlayerType.leader, guid: guid, id:12345));
            bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue, type: PlayerType.leader));
            for (int i = 0; i < 4; i++)
                redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red));
            for (int i = 0; i < 3; i++)
                bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue));
            _players.Add(TeamColour.blue, bluePlayers);
            _players.Add(TeamColour.red, redPlayers);
            var maxcount = new Dictionary<TeamColour, int>();
            maxcount.Add(TeamColour.blue, 5);
            maxcount.Add(TeamColour.red, 5);
            var leaderPresent = new Dictionary<TeamColour, bool>();
            leaderPresent.Add(TeamColour.blue, true);
            leaderPresent.Add(TeamColour.red, true);
            var boardMock = new Mock<Board>(new Exception());
            boardMock.Setup(b => b.Left).Returns(0);
            boardMock.Setup(b => b.Right).Returns(10);
            var teamBottom = new Dictionary<TeamColour, uint>();
            var teamTop = new Dictionary<TeamColour, uint>();
            teamBottom.Add(TeamColour.blue, 0);
            teamBottom.Add(TeamColour.red, 0);
            teamTop.Add(TeamColour.blue, 6);
            teamTop.Add(TeamColour.red, 6);
            boardMock.Setup(b => b.TeamBottom).Returns(teamBottom);
            boardMock.Setup(b => b.TeamTop).Returns(teamTop);
            var fields = new Field[10, 10];
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                {
                    fields[i, j] = i == 4 ? (Field)new GoalField() : (Field)new TaskField();
                }
            if (canPickUpPiece)
            {
                fields[5, 0] = new TaskField() { pieceIdSpecified = true, pieceId = 1 };
            }
            boardMock.Setup(b => b.Fields).Returns(fields);
            var g = new ProjectGame.GameMaster.Game(_players, maxcount, leaderPresent, null, null, 0, boardMock.Object);
            if (canPickUpPiece)
            {
                g.Pieces.Add(1, new Piece() { id = 1 });
            }
            var data = g.PlayerPickUpPiece(guid.ToString());
            if (!canPickUpPiece)
            {
                Assert.IsNull(data.Pieces);
            }
            else
            {
                Assert.IsNotNull(data.Pieces);
                Assert.AreEqual(1, data.Pieces[0].id);
                Assert.IsTrue(data.Pieces[0].playerIdSpecified);
                Assert.AreEqual(12345, data.Pieces[0].playerId);
            }
        }

        //[Test, Description("Game PlayerPlacePiece method test")]
        //[TestCase(true)]
        //[TestCase(false)]
        public void PlayerPlacePiece(bool goalArea)
        {
            throw new NotImplementedException();
        }

        [Test, Description("Game PlayerTestPiece method test")]
        [TestCase(true,true)]
        [TestCase(false,true)]
        [TestCase(false, false)]
        [TestCase(true,false)]
        public void PlayerTestPiece(bool canTest, bool isSham)
        {
            var guid = new Guid(1, 2, 3, 4, 5, 4, 3, 2, 1, 2, 3);
            var _players = new Dictionary<TeamColour, List<ProjectGame.GameMaster.Player>>();
            var redPlayers = new List<ProjectGame.GameMaster.Player>();
            var bluePlayers = new List<ProjectGame.GameMaster.Player>();
            var loc = new Location();
            loc.x = 0;
            loc.y = 5;
            redPlayers.Add(new ProjectGame.GameMaster.Player(loc, team: TeamColour.red, type: PlayerType.leader, guid: guid));
            bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue, type: PlayerType.leader));
            for (int i = 0; i < 4; i++)
                redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red));
            for (int i = 0; i < 3; i++)
                bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue));
            _players.Add(TeamColour.blue, bluePlayers);
            _players.Add(TeamColour.red, redPlayers);
            var maxcount = new Dictionary<TeamColour, int>();
            maxcount.Add(TeamColour.blue, 5);
            maxcount.Add(TeamColour.red, 5);
            var leaderPresent = new Dictionary<TeamColour, bool>();
            leaderPresent.Add(TeamColour.blue, true);
            leaderPresent.Add(TeamColour.red, true);
            var boardMock = new Mock<Board>(new Exception());
            boardMock.Setup(b => b.Left).Returns(0);
            boardMock.Setup(b => b.Right).Returns(10);
            var teamBottom = new Dictionary<TeamColour, uint>();
            var teamTop = new Dictionary<TeamColour, uint>();
            teamBottom.Add(TeamColour.blue, 0);
            teamBottom.Add(TeamColour.red, 0);
            teamTop.Add(TeamColour.blue, 6);
            teamTop.Add(TeamColour.red, 6);
            boardMock.Setup(b => b.TeamBottom).Returns(teamBottom);
            boardMock.Setup(b => b.TeamTop).Returns(teamTop);
            var fields = new Field[10, 10];
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                {
                    fields[i, j] = i == 4 ? (Field)new GoalField() : (Field)new TaskField();
                }

            Piece piece = null;
            if (canTest)
            {
                piece = new Piece()
                {
                    id = 123,
                    playerId = redPlayers[0].Id,
                    playerIdSpecified = true,
                    type = isSham ? PieceType.sham : PieceType.normal
                };
                redPlayers[0].Piece = piece;
            }
            boardMock.Setup(b => b.Fields).Returns(fields);
            var g = new ProjectGame.GameMaster.Game(_players, maxcount, leaderPresent, null, null, 0, boardMock.Object);
            if (canTest)
            {
                g.Pieces.Add(123, piece);
            }
            var data = g.PlayerTestPiece(guid.ToString());

            if (!canTest)
            {
                Assert.IsNull(data.Pieces);
            }
            else
            {
                Assert.AreEqual(data.Pieces[0].id, piece.id);
                Assert.AreEqual(data.Pieces[0].playerId, piece.playerId);
                Assert.AreEqual(data.Pieces[0].playerIdSpecified, piece.playerIdSpecified);
                Assert.AreEqual(data.Pieces[0].playerIdSpecified, true);
                Assert.AreEqual(data.Pieces[0].type, isSham ? PieceType.sham : PieceType.normal);
            }

        }

        [Test, Description("Game GetPlayerById test")]
        public void GetPlayerByIdTest()
        {
            var players = new Dictionary<TeamColour, List<ProjectGame.GameMaster.Player>>();
            var redPlayers = new List<ProjectGame.GameMaster.Player>();
            var bluePlayers = new List<ProjectGame.GameMaster.Player>();
            for (int i = 0; i < 4; i++)
                redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red, id:(ulong)i));
            for (int i = 4; i < 7; i++)
                bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue, id:(ulong)i));
            players.Add(TeamColour.blue, bluePlayers);
            players.Add(TeamColour.red, redPlayers);
            var game = new ProjectGame.GameMaster.Game(players: players);
            Assert.AreSame(game.GetPlayerById(6), bluePlayers[2]);
            Assert.AreSame(game.GetPlayerById(2), redPlayers[2]);
        }
        [Test, Description("Game PlayerAuthorizeKnowledgeExchange method test")]
        public void PlayerAuthorizeKnowledgeExchangeTest()
        {
            AuthorizeKnowledgeExchange.Delay = 1000;
            var players = new Dictionary<TeamColour, List<ProjectGame.GameMaster.Player>>();
            var redPlayers = new List<ProjectGame.GameMaster.Player>();
            var bluePlayers = new List<ProjectGame.GameMaster.Player>();
            var redGuid = Guid.NewGuid();
            var blueGuid = Guid.NewGuid();
            redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red, id: (ulong)0, guid:redGuid));
            bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue, id: (ulong)1, guid: blueGuid));
            players.Add(TeamColour.blue, bluePlayers);
            players.Add(TeamColour.red, redPlayers);
            var discoveredGoals = new Dictionary<ulong, HashSet<Location>>();
            var locations1 = new HashSet<Location>();
            locations1.Add(new Location { x = 1, y = 1 });
            var locations0 = new HashSet<Location>();
            locations0.Add(new Location { x = 0, y = 0 });
            discoveredGoals[0] = locations0;
            discoveredGoals[1] = locations1;
            var testedPieces = new Dictionary<ulong, HashSet<ulong>>();
            var tested0 = new HashSet<ulong>();
            tested0.Add(0);
            var tested1 = new HashSet<ulong>();
            tested1.Add(1);
            testedPieces[0] = tested0;
            testedPieces[1] = tested1;
            var game = new ProjectGame.GameMaster.Game(players: players, discoveredGoals:discoveredGoals, testedPieces:testedPieces);
            var msg = game.PlayerAuthorizeKnowledgeExchange(redGuid.ToString(), 1);
            Assert.IsTrue(msg is KnowledgeExchangeRequest);
            var req = msg as KnowledgeExchangeRequest;
            Assert.AreEqual(req.senderPlayerId, 0);
            Assert.AreEqual(req.playerId, 1);
            msg = game.PlayerAuthorizeKnowledgeExchange(blueGuid.ToString(), 0);
            Assert.IsTrue(msg is AcceptExchangeRequest);
            var req2 = msg as AcceptExchangeRequest;
            Assert.AreEqual(req2.senderPlayerId, 1);
            Assert.AreEqual(req2.playerId, 0);
        }
        [Test, Description("Game LeaderPresent field Test ")]
        public void AssignPlayerLeaderPresentUpdateTest()
        {
            var _players = new Dictionary<TeamColour, List<ProjectGame.GameMaster.Player>>();
            var redPlayers = new List<ProjectGame.GameMaster.Player>();
            var bluePlayers = new List<ProjectGame.GameMaster.Player>();
            redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red, type: PlayerType.member));
            bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue, type: PlayerType.member));
            for (int i = 0; i < 2; i++)
                redPlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.red));
            for (int i = 0; i < 2; i++)
                bluePlayers.Add(new ProjectGame.GameMaster.Player(new Location(), team: TeamColour.blue));
            _players.Add(TeamColour.blue, bluePlayers);
            _players.Add(TeamColour.red, redPlayers);
            var maxcount = new Dictionary<TeamColour, int>();
            maxcount.Add(TeamColour.blue, 5);
            maxcount.Add(TeamColour.red, 5);
            var leaderPresent = new Dictionary<TeamColour, bool>();
            leaderPresent.Add(TeamColour.blue, false);
            leaderPresent.Add(TeamColour.red, false);
            var g = new ProjectGame.GameMaster.Game(_players, maxcount, leaderPresent, null, null, 0);
            var msg=(ConfirmJoiningGame)g.RegisterPlayer(new ProjectGame.Common.Messages.Player() { team = TeamColour.red, type = PlayerType.leader, id=999 });
            Assert.AreEqual(msg.PlayerDefinition.id, 999);
            Assert.AreEqual(msg.PlayerDefinition.team, TeamColour.red);
            Assert.AreEqual(msg.PlayerDefinition.type, PlayerType.leader);
            Assert.IsTrue(leaderPresent[TeamColour.red]);

        }

    }
}
