﻿using System;
using NUnit.Framework;
using ProjectGame.Common.Messages;
using ProjectGame.GameMaster;
using Moq;
using System.Collections.Generic;
using ProjectGame.Common;

namespace ProjectGame.Test.GameMaster
{
    [TestFixture, Description("Unit Tests for Player class")]
    public class PlayerTests
    {
        [Test, Description("Player UnmarkBoardPosition method test")]
        public void UnmarkBoardPositionTest()
        {
            Location loc = new Location();
            loc.x = 3;
            loc.y = 10;
            var boardMock = new Mock<Board>(new Exception());
            var fields = new Field[11, 4];
            fields[loc.y, loc.x] = new TaskField() { playerIdSpecified = true};
            boardMock.Setup(b => b.Fields).Returns(fields);
            new ProjectGame.GameMaster.Player(location: loc).UnmarkBoardPosition(boardMock.Object);
            Assert.IsFalse(fields[loc.y,loc.x].playerIdSpecified);
        }

        [Test, Description("Player MarkBoardPosition method test")]
        public void MarkBoardPositionTest()
        {
            Location loc = new Location();
            loc.x = 3;
            loc.y = 10;
            var boardMock = new Mock<Board>(new Exception());
            var fields = new Field[11, 4];
            fields[loc.y, loc.x] = new TaskField() { playerIdSpecified = false };
            boardMock.Setup(b => b.Fields).Returns(fields);
            new ProjectGame.GameMaster.Player(location: loc, id: 12).MarkBoardPosition(boardMock.Object);
            Assert.AreEqual(fields[loc.y, loc.x].playerIdSpecified, true);
            Assert.AreEqual(fields[loc.y, loc.x].playerId, 12);
        }

        [Test, Description("Player Move method test (board edge)")]
        [TestCase(MoveType.down)]
        [TestCase(MoveType.up)]
        [TestCase(MoveType.right)]
        [TestCase(MoveType.left)]
        public void PlayerMoveBoundaryTest(MoveType direction)
        {
            Location loc = new Location();
            loc.x = 3;
            loc.y = 10;
            uint x=loc.x;
            uint y = loc.y;
            var boardMock = new Mock<Board>(new Exception());
            var fields = new Field[11, 4];
            fields[y, x] = new TaskField() { playerIdSpecified = true };
            fields[y - 1, x] = new TaskField() { playerIdSpecified = false };
            fields[y, x - 1] = new TaskField() { playerIdSpecified = false };
            boardMock.Setup(b => b.Fields).Returns(fields);
            boardMock.Setup(b => b.Top).Returns(y+1);
            boardMock.Setup(b => b.Right).Returns(x+1);
            boardMock.Setup(b => b.Bottom).Returns(0);
            boardMock.Setup(b => b.Left).Returns(0);
            var teamTop = new Dictionary<TeamColour, uint>();
            var teamBot = new Dictionary<TeamColour, uint>();

            teamBot.Add(TeamColour.blue, 0);
            teamBot.Add(TeamColour.red, 0);
            
            teamTop.Add(TeamColour.blue, y+1);
            teamTop.Add(TeamColour.red, y+1);

            boardMock.Setup(b => b.TeamBottom).Returns(teamBot);
            boardMock.Setup(b => b.TeamTop).Returns(teamTop);
            var gameMock = new Mock<ProjectGame.GameMaster.Game>(new Exception());
            Board board = boardMock.Object;
            gameMock.Setup(g => g.Board).Returns(board);
            var pl = new ProjectGame.GameMaster.Player(location: loc, id: 12);
            bool occupied = false;
            bool boundary = false;
            uint targetx;
            uint targety;
            pl.Move(gameMock.Object, direction, out occupied, out boundary, out targetx, out targety);
            
            switch (direction)
            {
                case (MoveType.up):
                    Assert.IsTrue(boundary);
                    Assert.IsFalse(occupied);
                    Assert.IsFalse(fields[y, x - 1].playerIdSpecified);
                    Assert.IsFalse(fields[y - 1, x].playerIdSpecified);
                    Assert.IsTrue(fields[y, x].playerIdSpecified);
                    break;
                case (MoveType.left):
                    Assert.IsFalse(boundary);
                    Assert.IsFalse(occupied);
                    Assert.IsTrue(fields[y, x - 1].playerIdSpecified);
                    Assert.IsFalse(fields[y - 1, x].playerIdSpecified);
                    Assert.IsFalse(fields[y, x].playerIdSpecified);
                    break;
                case (MoveType.right):
                    Assert.IsTrue(boundary);
                    Assert.IsFalse(occupied);
                    Assert.IsFalse(fields[y, x - 1].playerIdSpecified);
                    Assert.IsFalse(fields[y - 1, x].playerIdSpecified);
                    Assert.IsTrue(fields[y, x].playerIdSpecified);
                    break;
                case (MoveType.down):
                    Assert.IsFalse(boundary);
                    Assert.IsFalse(occupied);
                    Assert.IsFalse(fields[y, x - 1].playerIdSpecified);
                    Assert.IsTrue(fields[y - 1, x].playerIdSpecified);
                    Assert.IsFalse(fields[y, x].playerIdSpecified);
                    break;
            }
        }

        [Test, Description("Player Move method test (occupied fields)")]
        [TestCase(MoveType.down)]
        [TestCase(MoveType.up)]
        [TestCase(MoveType.right)]
        [TestCase(MoveType.left)]
        public void PlayerMoveOccupiedTest(MoveType direction)
        {
            Location loc = new Location();
            loc.x = 3;
            loc.y = 10;
            uint x = loc.x;
            uint y = loc.y;
            var boardMock = new Mock<Board>(new Exception());
            var fields = new Field[20, 20];
            fields[y, x] = new TaskField() { playerIdSpecified = true };
            fields[y - 1, x] = new TaskField() { playerIdSpecified = false };
            fields[y, x - 1] = new TaskField() { playerIdSpecified = true };
            fields[y + 1, x] = new TaskField() { playerIdSpecified = true };
            fields[y, x + 1] = new TaskField() { playerIdSpecified = false };
            boardMock.Setup(b => b.Fields).Returns(fields);
            boardMock.Setup(b => b.Top).Returns(20);
            boardMock.Setup(b => b.Right).Returns(20);
            boardMock.Setup(b => b.Bottom).Returns(0);
            boardMock.Setup(b => b.Left).Returns(0);
            var teamTop = new Dictionary<TeamColour, uint>();
            var teamBot = new Dictionary<TeamColour, uint>();

            teamBot.Add(TeamColour.blue, 0);
            teamBot.Add(TeamColour.red, 0);

            teamTop.Add(TeamColour.blue, 20);
            teamTop.Add(TeamColour.red, 20);

            boardMock.Setup(b => b.TeamBottom).Returns(teamBot);
            boardMock.Setup(b => b.TeamTop).Returns(teamTop);
            var gameMock = new Mock<ProjectGame.GameMaster.Game>(new Exception());
            Board board = boardMock.Object;
            gameMock.Setup(g => g.Board).Returns(board);
            var pl = new ProjectGame.GameMaster.Player(location: loc, id: 12);
            bool occupied = false;
            bool boundary = false;
            uint targetx;
            uint targety;
            pl.Move(gameMock.Object, direction, out occupied, out boundary, out targetx, out targety);

            switch (direction)
            {
                case (MoveType.up):
                    Assert.IsFalse(boundary);
                    Assert.IsTrue(occupied);
                    Assert.IsTrue(fields[y, x - 1].playerIdSpecified);
                    Assert.IsTrue(fields[y + 1, x].playerIdSpecified);
                    Assert.IsFalse(fields[y, x + 1].playerIdSpecified);
                    Assert.IsFalse(fields[y - 1, x].playerIdSpecified);
                    Assert.IsTrue(fields[y, x].playerIdSpecified);
                    break;
                case (MoveType.left):
                    Assert.IsFalse(boundary);
                    Assert.IsTrue(occupied);
                    Assert.IsTrue(fields[y, x - 1].playerIdSpecified);
                    Assert.IsTrue(fields[y + 1, x].playerIdSpecified);
                    Assert.IsFalse(fields[y, x + 1].playerIdSpecified);
                    Assert.IsFalse(fields[y - 1, x].playerIdSpecified);
                    Assert.IsTrue(fields[y, x].playerIdSpecified);
                    break;
                case (MoveType.right):
                    Assert.IsFalse(boundary);
                    Assert.IsFalse(occupied);
                    Assert.IsTrue(fields[y, x - 1].playerIdSpecified);
                    Assert.IsTrue(fields[y + 1, x].playerIdSpecified);
                    Assert.IsTrue(fields[y, x + 1].playerIdSpecified);
                    Assert.IsFalse(fields[y - 1, x].playerIdSpecified);
                    Assert.IsFalse(fields[y, x].playerIdSpecified);
                    break;
                case (MoveType.down):
                    Assert.IsFalse(boundary);
                    Assert.IsFalse(occupied);
                    Assert.IsTrue(fields[y, x - 1].playerIdSpecified);
                    Assert.IsTrue(fields[y + 1, x].playerIdSpecified);
                    Assert.IsFalse(fields[y, x + 1].playerIdSpecified);
                    Assert.IsTrue(fields[y - 1, x].playerIdSpecified);
                    Assert.IsFalse(fields[y, x].playerIdSpecified);
                    break;
            }
        }

        [Test, Description("Player PickUpPiece method test")]
        [TestCase(true)]
        [TestCase(false)]
        public void PlayerPickUpPieceTest(bool canPickUpPiece)
        {
            Location loc = new Location();
            loc.x = 3;
            loc.y = 10;
            uint x = loc.x;
            uint y = loc.y;
            var boardMock = new Mock<Board>(new Exception());
            var fields = new Field[20, 20];
            fields[y, x] = new TaskField() { playerIdSpecified = true,
                                             pieceIdSpecified = canPickUpPiece,
                                             pieceId=22};
            boardMock.Setup(b => b.Fields).Returns(fields);
            var gameMock = new Mock<ProjectGame.GameMaster.Game>(new Exception());
            Board board = boardMock.Object;
            gameMock.Setup(g => g.Board).Returns(board);
            Dictionary<ulong, Piece> pieces = new Dictionary<ulong, Piece>();
            var piece = new Piece() { type = PieceType.unknown };
            pieces.Add(22, piece);
            //gameMock.Setup(g => g._pieces).Returns(pieces);
            gameMock.Setup(g => g.UpdateNeighborPiece());
            var pl = new ProjectGame.GameMaster.Player(location: loc, id: 12);
            var game = gameMock.Object;
            game.Pieces = pieces;
            bool result = pl.PickUpPiece(game);
            Assert.AreEqual(result, canPickUpPiece);
            if (canPickUpPiece)
            {
                Assert.AreEqual(piece.playerId, pl.Id);
                Assert.AreSame(piece, pl.Piece);
                Assert.IsTrue(piece.playerIdSpecified);
                Assert.IsFalse((fields[y, x] as TaskField).pieceIdSpecified);
                gameMock.Verify(g => g.UpdateNeighborPiece());
            }
        }

        //[Test, Description("Player PlacePiece method test")]
        //[TestCase(true, true, true)]
        public void PlayerPlacePieceTest(bool isSham, bool isInGoalArea, bool isGoal)
        {

            throw new NotImplementedException();
            /*Location loc = new Location();
            loc.x = 3;
            loc.y = 10;
            uint x = loc.x;
            uint y = loc.y;
            var boardMock = new Mock<Board>(new Exception());
            var fields = new Field[20, 20];
            var goalFields = new bool[20, 20];
            fields[y, x] = isInGoalArea ? (Field) new GoalField() : (Field) new TaskField();
            goalFields[y, x] = isGoal;
            boardMock.Setup(b => b.Fields).Returns(fields);
            boardMock.Setup(b => b.GoalFields).Returns(goalFields);
            var gameMock = new Mock<ProjectGame.GameMaster.Game>(new Exception());
            Board board = boardMock.Object;
            gameMock.Setup(g => g.Board).Returns(board);
            gameMock.Setup(g => g.UpdateNeighborPiece());
            var pl = new ProjectGame.GameMaster.Player(location: loc, id: 12);
            pl.Piece = new Piece() {type=isSham?PieceType.sham:PieceType.normal };
            gameMock.Setup(g => g.RemovePiece(It.IsAny<ulong>()));
            if (isGoal&&!isSham)
            {
                gameMock.Setup(g => g.GoalCheck(It.IsAny<TeamColour>()));
            }
            var game = gameMock.Object;
            bool goalArea;
            bool completion;
            bool result = pl.PlacePiece(game, out goalArea, out completion);
            gameMock.Verify(g => g.RemovePiece(It.IsAny<ulong>()));
            if (!isInGoalArea)
            {
                Assert.IsFalse(result);
                Assert.IsFalse(completion);
                Assert.IsFalse(pl.Piece == null);
            }
            else
            {
                if (isSham)
                {
                    Assert.IsFalse(result);
                    Assert.IsFalse(completion);
                    Assert.IsFalse(pl.Piece == null);
                }
            }*/
        }
        [Test, Description("Player TestPiece method test")]
        [TestCase(true)]
        [TestCase(false)]
        public void PlayerTestPieceTest(bool isSham)
        {
            var gameMock = new Mock<ProjectGame.GameMaster.Game>(new Exception());
            var pl = new ProjectGame.GameMaster.Player(location: new Location(), id: 12);
            pl.Piece = new Piece() { type = isSham ? PieceType.sham : PieceType.normal };
            Assert.AreEqual(pl.TestPiece(gameMock.Object), !isSham);
        }

        [Test, Description("Player SendMessage method test")]
        [TestCase(true)]
        [TestCase(false)]
        public void PlayerSendMessageTest(bool isPlayerMessage)
        {
            Message msg = isPlayerMessage ? (Message) new KnowledgeExchangeRequest() : 
                                            (Message) new AuthorizeKnowledgeExchange();
            var gmMock = new Mock<IGameMaster>();
            gmMock.Setup(gm => gm.SendMessage(It.IsAny<Message>()));
            var pl = new ProjectGame.GameMaster.Player(location: new Location(),
                                                       gameMaster: gmMock.Object);
            if (!isPlayerMessage)
            {
                Assert.Throws(typeof(NotSupportedException),()=>pl.SendMessage(msg));
            }
            else
            {
                pl.SendMessage(msg);
                gmMock.Verify(gm => gm.SendMessage(It.IsAny<Message>()));
            }
        }

        [Test, Description("Player ForMessage method test")]
        public void PlayerForMessageTest()
        {
            
            var pl = new ProjectGame.GameMaster.Player(location: new Location());
            var msg = pl.ForMessage();
            Assert.AreEqual(msg.id, pl.Id);
            Assert.AreEqual(msg.team, pl.Team);
            Assert.AreEqual(msg.type, pl.Type);
        }

    }
}
