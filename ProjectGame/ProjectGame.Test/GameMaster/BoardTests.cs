﻿using NUnit.Framework;
using ProjectGame.Common.Messages;
using ProjectGame.GameMaster;

namespace ProjectGame.Test.GameMaster
{
    [TestFixture, Description("Unit Tests for Board class")]
    public class BoardTests
    {
        [Test, Description("Board GetTeamLocation method test")]
        public void GetTeamLocationTest()
        {
            Location loc = new Location() { x = 10, y = 10 };
            Assert.AreEqual(10, new Board().GetTeamLocation(loc,TeamColour.blue).y);
            Assert.AreEqual(10-GameMasterConfiguration.GoalAreaLength, new Board().GetTeamLocation(loc, TeamColour.red).y);
        }

        [Test, Description("Board GetTeamY method test")]
        public void GetTeamYTest()
        {
            uint y = 10;
            Assert.AreEqual(y, new Board().GetTeamY(y, TeamColour.blue));
            Assert.AreEqual(y - GameMasterConfiguration.GoalAreaLength, new Board().GetTeamY(y, TeamColour.red));
        }

        [Test, Description("Board IsInTaskArea method test")]
        [TestCase(4,4)]
        [TestCase(2, 2)]
        [TestCase(8, 8)]
        [TestCase(40, 40)]
        public void IsInTaskAreaTest(int x, int y)
        {
            var b = new Board();
            Assert.AreEqual(b.IsInTaskArea((uint)x, (uint)y), 
                            (x >= 0 && x < GameMasterConfiguration.BoardWidth &&
                             y >= GameMasterConfiguration.GoalAreaLength &&
                             y < GameMasterConfiguration.GoalAreaLength + GameMasterConfiguration.TaskAreaLength));
            Assert.AreEqual(b.IsInTaskArea((uint)x, (uint)y), b.IsInTaskArea(new Location() { x = (uint)x, y = (uint)y }));
        }

        [Test, Description("Board ForMessage method test")]
        public void ForMessageTest()
        {
            var msg = new Board().ForMessage();
            Assert.AreEqual(msg.width, (uint)GameMasterConfiguration.BoardWidth);
            Assert.AreEqual(msg.goalsHeight, (uint)GameMasterConfiguration.GoalAreaLength);
            Assert.AreEqual(msg.tasksHeight, (uint)GameMasterConfiguration.TaskAreaLength);
        }

    }
}
