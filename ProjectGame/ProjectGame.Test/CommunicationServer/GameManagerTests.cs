﻿using Moq;
using NUnit.Framework;
using ProjectGame.Common;
using ProjectGame.Common.Exceptions;
using ProjectGame.Common.Messages;
using ProjectGame.CommunicationServer.Clients;
using System.Collections.Generic;
using Game = ProjectGame.CommunicationServer.Clients.Game;
using GameManager = ProjectGame.CommunicationServer.Clients.GameManager;

namespace ProjectGame.Test.CommunicationServer
{
    [TestFixture, Description("Unit Tests for GameManager class")]
    public class GameManagerTests
    {
        [Test, Description("GameManager GetGameById method test")]
        public void GetGameByIdTest()
        {
            Dictionary<ulong, Game> games = new Dictionary<ulong, Game>();
            var game = new Mock<Game>(null, new GameInfo(),null,null).Object;
            games.Add(5, game);
            games.Add(3, new Mock<Game>(null, new GameInfo(), null, null).Object);
            games.Add(59, new Mock<Game>(null, new GameInfo(), null, null).Object);
            var gman = new GameManager(null,games,null,null);
            Assert.Throws(typeof(NotFoundException), () => gman.GetGameById(12));
            Assert.AreSame(game, gman.GetGameById(5));
        }

        [Test, Description("GameManager GetGameByName method test")]
        public void GetGameByNameTest()
        {
            Dictionary<string, Game> games = new Dictionary<string, Game>();
            var game = new Mock<Game>(null, new GameInfo(), null, null).Object;
            games.Add("5", game);
            games.Add("3", new Mock<Game>(null, new GameInfo(), null, null).Object);
            games.Add("59", new Mock<Game>(null, new GameInfo(), null, null).Object);
            var gman = new GameManager(null, null, games, null);
            Assert.Throws(typeof(NotFoundException), () => gman.GetGameByName("12"));
            Assert.AreSame(game, gman.GetGameByName("5"));
        }

        [Test, Description("GameManager GetPlayerById method test")]
        public void GetPlayerByIdTest()
        {
            Dictionary<ulong, IAgent> agents = new Dictionary<ulong, IAgent>();
            var agent = new Mock<IAgent>().Object;
            agents.Add(5, agent);
            agents.Add(3, new Mock<IAgent>().Object);
            agents.Add(59, new Mock<IAgent>().Object);
            var gman = new GameManager(agents, null,null , null);
            Assert.Throws(typeof(NotFoundException), () => gman.GetPlayerById(12));
            Assert.AreSame(agent, gman.GetPlayerById(5));
        }

        [Test, Description("GameManager Dispose method test")]
        public void DisposeTest()
        {
            var agents = new List<Agent>();
            int counter = 0;
            for(int i = 0; i < 10; i++)
            {
                var agentMock = new Mock<Agent>();
                agentMock.Setup(a => a.Dispose()).Callback(() => counter++);
                var agent=agentMock.Object;
                agents.Add(agent);
            }
            
            var gman = new GameManager(null, null, null, agents);
            gman.Dispose();
            Assert.AreEqual(10, counter);
        }

        [Test, Description("GameManager AddPlayer method test")]
        public void AddPlayerTest()
        {
            Dictionary<ulong, IAgent> agents = new Dictionary<ulong, IAgent>();
            var agent = new Mock<IAgent>().Object;
            agents.Add(500, agent);
            agents.Add(300, new Mock<IAgent>().Object);
            agents.Add(590, new Mock<IAgent>().Object);
            var gman = new GameManager(agents, null, null, null);
            gman.AddPlayer(new Mock<IAgent>().Object);
            Assert.AreEqual(4, agents.Count);
        }

        [Test, Description("GameManager GameWithNameExists method test")]
        public void GameWithNameExistsTest()
        {
            Dictionary<string, Game> games = new Dictionary<string, Game>();
            var game = new Mock<Game>(null, new GameInfo(), null, null).Object;
            games.Add("5", game);
            games.Add("3", new Mock<Game>(null, new GameInfo(), null, null).Object);
            games.Add("59", new Mock<Game>(null, new GameInfo(), null, null).Object);
            var gman = new GameManager(null, null, games, null);
            Assert.IsTrue(gman.GameWithNameExists("5"));
            Assert.IsTrue(gman.GameWithNameExists("3"));
            Assert.IsTrue(gman.GameWithNameExists("59"));
            Assert.IsFalse(gman.GameWithNameExists("54556"));
        }
        [Test, Description("GameManager AddGame method test")]
        public void AddGameTest()
        {
            Dictionary<string, Game> games = new Dictionary<string, Game>();
            Dictionary<ulong, Game> gamesId = new Dictionary<ulong, Game>();
            var gameMock = new Mock<Game>(null, new GameInfo(), null, null);
            gameMock.Setup(g => g.Id).Returns(5);
            gameMock.Setup(g => g.Name).Returns("5");
            var game=gameMock.Object;
            var gameMock2 = new Mock<Game>(null, new GameInfo(), null, null);
            gameMock2.Setup(g => g.Id).Returns(3);
            gameMock2.Setup(g => g.Name).Returns("3");
            var game2 = gameMock2.Object;
            var gameMock3 = new Mock<Game>(null, new GameInfo(), null, null);
            gameMock3.Setup(g => g.Id).Returns(59);
            gameMock3.Setup(g => g.Name).Returns("59");
            var game3 = gameMock3.Object;
            var gameMock4 = new Mock<Game>(null, new GameInfo(), null, null);
            gameMock4.Setup(g => g.Id).Returns(590);
            gameMock4.Setup(g => g.Name).Returns("590");
            var game4 = gameMock4.Object;
            games.Add("5", game);
            games.Add("3", game2);
            games.Add("59", game3);
            gamesId.Add(5, game);
            gamesId.Add(3, game2);
            gamesId.Add(59, game3);
            var gman = new GameManager(null, gamesId, games, null);
            gman.AddGame(game4);
            Assert.IsTrue(games.ContainsKey("590"));
            Assert.IsTrue(gamesId.ContainsKey(590));
        }

        [Test, Description("GameManager AddPlayerToGame method test")]
        public void AddPlayerToGameTest()
        {
            var player = new Mock<IAgent>().Object;
            var gameMock = new Mock<Game>(null, new GameInfo(), null, null);
            gameMock.Setup(g => g.AddPlayer(player, TeamColour.red));
            Game game = gameMock.Object;
            var gman = new GameManager(null, null,null, null);
            gman.AddPlayerToGame(player, TeamColour.red, game);
            gameMock.Verify(g => g.AddPlayer(player, TeamColour.red));
        }

        [Test, Description("GameManager RegisterAgent method test")]
        public void RegisterAgentTest()
        {
            var agents = new List<Agent>();
            int counter = 0;
            for (int i = 0; i < 10; i++)
            {
                agents.Add(new Mock<Agent>().Object);
            }

            var gman = new GameManager(null, null, null, agents);
            var agentMock = new Mock<Agent>();
            var agent = agentMock.Object;
            gman.RegisterAgent(agent);
            Assert.AreEqual(11, agents.Count);
            Assert.IsTrue(agents.Contains(agent));
        }

        [Test, Description("GameManager GetGames method test")]
        public void GetGamesTest()
        {
            Dictionary<string, Game> gamesId = new Dictionary<string, Game>();
            var gameMock = new Mock<Game>(null, new GameInfo(), null, null);
            gameMock.Setup(g => g.Id).Returns(5);
            gameMock.Setup(g => g.Name).Returns("5");
            gameMock.Setup(g => g.GetGameInfo())
                    .Returns(new GameInfo { blueTeamPlayers = 3, redTeamPlayers = 3, gameName = "G1" });
            var game = gameMock.Object;
            var gameMock2 = new Mock<Game>(null, new GameInfo(), null, null);
            gameMock2.Setup(g => g.Id).Returns(3);
            gameMock2.Setup(g => g.Name).Returns("3");
            gameMock2.Setup(g => g.GetGameInfo())
                    .Returns(new GameInfo { blueTeamPlayers = 1, redTeamPlayers = 1, gameName = "G2" });
            var game2 = gameMock2.Object;
            var gameMock3 = new Mock<Game>(null, new GameInfo(), null, null);
            gameMock3.Setup(g => g.Id).Returns(59);
            gameMock3.Setup(g => g.Name).Returns("59");
            gameMock3.Setup(g => g.GetGameInfo())
                    .Returns(new GameInfo { blueTeamPlayers = 3, redTeamPlayers = 1, gameName = "G3" });
            var game3 = gameMock3.Object;
            gamesId.Add(game.Name, game);
            gamesId.Add(game2.Name, game2);
            gamesId.Add(game3.Name, game3);
            var gman = new GameManager(null, null, gamesId, null);
            var result = gman.GetGames();
            Assert.AreEqual("G1", result.GameInfo[0].gameName);
            Assert.AreEqual("G2", result.GameInfo[1].gameName);
            Assert.AreEqual("G3", result.GameInfo[2].gameName);

            Assert.AreEqual(3, result.GameInfo[0].blueTeamPlayers);
            Assert.AreEqual(3, result.GameInfo[0].redTeamPlayers);
            Assert.AreEqual(1, result.GameInfo[1].blueTeamPlayers);
            Assert.AreEqual(1, result.GameInfo[1].redTeamPlayers);
            Assert.AreEqual(1, result.GameInfo[2].redTeamPlayers);
            Assert.AreEqual(3, result.GameInfo[2].blueTeamPlayers);
        }

        [Test,Description("StartGame method test")]
        public void StartGameTest()
        {
            Dictionary<string, Game> gamesId = new Dictionary<string, Game>();
            var gameMock = new Mock<Game>(null, new GameInfo(), null, null);
            gameMock.Setup(g => g.Id).Returns(5);
            gameMock.Setup(g => g.Name).Returns("5");
            gameMock.Setup(g => g.GetGameInfo())
                    .Returns(new GameInfo { blueTeamPlayers = 3, redTeamPlayers = 3, gameName = "G1" });
            var game = gameMock.Object;
            var gameMock2 = new Mock<Game>(null, new GameInfo(), null, null);
            gameMock2.Setup(g => g.Id).Returns(3);
            gameMock2.Setup(g => g.Name).Returns("3");
            gameMock2.Setup(g => g.GetGameInfo())
                    .Returns(new GameInfo { blueTeamPlayers = 1, redTeamPlayers = 1, gameName = "G2" });
            var game2 = gameMock2.Object;
            var gameMock3 = new Mock<Game>(null, new GameInfo(), null, null);
            gameMock3.Setup(g => g.Id).Returns(59);
            gameMock3.Setup(g => g.Name).Returns("59");
            gameMock3.Setup(g => g.GetGameInfo())
                    .Returns(new GameInfo { blueTeamPlayers = 3, redTeamPlayers = 1, gameName = "G3" });
            var game3 = gameMock3.Object;
            gamesId.Add(game.Name, game);
            gamesId.Add(game2.Name, game2);
            gamesId.Add(game3.Name, game3);
            Dictionary<ulong, Game> _idGameMap = new Dictionary<ulong, Game>();
            _idGameMap.Add(5, game);
            _idGameMap.Add(3, game2);
            _idGameMap.Add(59,game3);
            var gman = new GameManager(null, _idGameMap, gamesId, null);
            var result1 = gman.GetGames();
            gman.GameStarted(59);
            var result2 = gman.GetGames();
            Assert.AreEqual(result1.GameInfo.Length, 1 + result2.GameInfo.Length);
        }

    }
}
