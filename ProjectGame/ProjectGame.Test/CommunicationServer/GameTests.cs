﻿using Moq;
using NUnit.Framework;
using ProjectGame.Common;
using ProjectGame.Common.Exceptions;
using ProjectGame.Common.Messages;
using System.Collections.Generic;

namespace ProjectGame.Test.CommunicationServer
{
    [TestFixture, Description("Unit Tests for Game class")]
    public class GameTests
    {
        [Test, Description("Game BroadcastMessage method test")]
        public void BroadcastMessageTest()
        {
            var redteam = new Dictionary<ulong, IAgent>();
            var blueteam = new Dictionary<ulong, IAgent>();
            var alivePlayerMock = new Mock<IAgent>();
            var deadPlayerMock = new Mock<IAgent>();
            int counter = 0;
            alivePlayerMock.Setup(p => p.SendMessage(It.IsAny<Message>())).Callback(() => counter++);
            deadPlayerMock.Setup(p => p.SendMessage(It.IsAny<Message>())).Throws(new ProtocolException("TEST"));
            redteam.Add(1, alivePlayerMock.Object);
            redteam.Add(2, deadPlayerMock.Object);
            var game = new ProjectGame.CommunicationServer.Clients.Game(null, new GameInfo(), redteam, blueteam);
            var result = game.BroadcastMessage(new Data());
            Assert.AreEqual(1, counter);
            Assert.AreEqual(1, result.Count);
            Assert.AreSame(result[0], redteam[2]);

        }

        [Test, Description("Game AddPlayer method test")]
        public void AddPlayerTest()
        {
            var redteam = new Dictionary<ulong, IAgent>();
            var blueteam = new Dictionary<ulong, IAgent>();
            var alivePlayerMock = new Mock<IAgent>();
            var deadPlayerMock = new Mock<IAgent>();
            int counter = 0;
            alivePlayerMock.Setup(p => p.SendMessage(It.IsAny<Message>())).Callback(() => counter++);
            deadPlayerMock.Setup(p => p.SendMessage(It.IsAny<Message>())).Throws(new ProtocolException("TEST"));
            redteam.Add(1, alivePlayerMock.Object);
            redteam.Add(2, deadPlayerMock.Object);
            var game = new ProjectGame.CommunicationServer.Clients.Game(null, new GameInfo(), redteam, blueteam);
            game.AddPlayer(new Mock<IAgent>().Object, TeamColour.blue);
            Assert.AreEqual(1, blueteam.Count);

        }

        [Test, Description("Game GetGameInfo method test")]
        public void GetGameInfoTest()
        {
            var redteam = new Dictionary<ulong, IAgent>();
            var blueteam = new Dictionary<ulong, IAgent>();
            var alivePlayerMock = new Mock<IAgent>();
            var deadPlayerMock = new Mock<IAgent>();
            int counter = 0;
            alivePlayerMock.Setup(p => p.SendMessage(It.IsAny<Message>())).Callback(() => counter++);
            deadPlayerMock.Setup(p => p.SendMessage(It.IsAny<Message>())).Throws(new ProtocolException("TEST"));
            redteam.Add(1, alivePlayerMock.Object);
            redteam.Add(2, deadPlayerMock.Object);
            var game = new ProjectGame.CommunicationServer.Clients.Game(null, 
                                                                        new GameInfo()
                                                                        {
                                                                            blueTeamPlayers =2,
                                                                            redTeamPlayers =2,
                                                                            gameName ="TEST"
                                                                        },  redteam, blueteam);
            var result = game.GetGameInfo();
            Assert.AreEqual("TEST",result.gameName);
            Assert.AreEqual(0, result.redTeamPlayers);
            Assert.AreEqual(2, result.blueTeamPlayers);

        }


    }
}
