﻿using System.Collections.Generic;
using NUnit.Framework;
using ProjectGame.Common;
using System.Net.Sockets;
using ProjectGame.Common.Messages;
using System.Threading.Tasks;
using System.Net;
using ProjectGame.CommunicationServer.Clients;
using Moq;
using Game = ProjectGame.CommunicationServer.Clients.Game;
using ProjectGame.Common.Exceptions;

namespace ProjectGame.Test.CommunicationServer
{
    [TestFixture, Description("Unit Tests for CommunicationServer class")]
    public class CommunicationServerTests
    {
        [Test, Description("CommunicationServer ForwardToGameMaster method test")]
        public void ForwardToGameMasterTest()
        {
            var gameMasterMock = new Mock<IAgent>();
            gameMasterMock.Setup(
                                gm => gm.SendMessage(It.Is<Move>(
                                            msg => msg.direction == MoveType.down)));

            var game = new Game(gameMasterMock.Object, new GameInfo());
            ulong x = game.Id;
            var gameManagerMock = new Mock<GameManager>();
            gameManagerMock.Setup(
                                gman => gman.GetGameById(It.IsAny<ulong>()))
                                .Returns(game);

            var cs = new ProjectGame.CommunicationServer
                                .CommunicationServer(null, null, gameManagerMock.Object, null);
            cs.ForwardToGameMaster(new Move() { gameId = x, direction=MoveType.down });
            gameMasterMock.Verify(gm => gm.SendMessage(It.IsAny<Message>()));
        }
        [Test, Description("CommunicationServer ForwardToPlayer method test")]
        public void ForwardToPlayerTest()
        {
            TcpListener listener = new TcpListener(IPAddress.Any,3030);
            listener.Start();
            TcpClient client=new TcpClient();
            TcpClient client2=null;
            client.Connect("localhost", 3030);
            var t = Task.Run(() => { client2 = listener.AcceptTcpClient(); listener.Stop(); });
            t.Wait();
            var playerMock = new Mock<Agent>();
            playerMock.Setup(
                                pl => pl.SendMessage(It.Is<RejectKnowledgeExchange>(
                                            msg => msg.senderPlayerId == 66)));
            
            var gameManagerMock = new Mock<GameManager>();
            gameManagerMock.Setup(
                                gman => gman.GetPlayerById(It.IsAny<ulong>()))
                                .Returns(playerMock.Object);

            var cs = new ProjectGame.CommunicationServer
                                .CommunicationServer(null, null, gameManagerMock.Object, null);
            cs.ForwardToPlayer(new RejectKnowledgeExchange() {senderPlayerId=66, playerId=22 });
            playerMock.Verify(pl => pl.SendMessage(It.IsAny<Message>()));
            client.Close();
            client2.Close();
        }
        [Test, Description("CommunicationServer BroadcastMessage method test")]
        public void BroadcastMessageTest()
        {
            var gameMasterMock = new Mock<IAgent>();
            var gameMock = new Mock<Game>(gameMasterMock.Object, new GameInfo());
            Message msg = new Data() { gameFinished = true };
            gameMock.Setup(g => g.BroadcastMessage(It.Is<Data>(m => m.gameFinished)))
                .Returns(() => new List<IAgent>());
            var game = gameMock.Object;

            ulong x = game.Id;
            var gameManagerMock = new Mock<GameManager>();
            gameManagerMock.Setup(
                                gman => gman.GetGameById(It.IsAny<ulong>()))
                                .Returns(game);

            var cs = new ProjectGame.CommunicationServer
                                .CommunicationServer(null, null, gameManagerMock.Object, null);
            cs.BroadcastMessage(msg, x);
            gameMock.Verify(g => g.BroadcastMessage(It.IsAny<Message>()));
        }

        [Test, Description("CommunicationServer RegisterGame method test")]
        public void RegisterGameTest()
        {
            var gameManagerMock = new Mock<GameManager>();
            gameManagerMock.Setup(
                                gman => gman.GameWithNameExists(It.Is<string>(
                                    name=>name.Equals("TEST"))))
                           .Returns(false);
            gameManagerMock.Setup(
                                gman => gman.GameWithNameExists(It.Is<string>(
                                    name => !name.Equals("TEST"))))
                            .Returns(true);
            gameManagerMock.Setup(
                                gman => gman.AddGame(It.Is<Game>(
                                    g => g.Name.Equals("TEST"))));

            var cs = new ProjectGame.CommunicationServer
                                .CommunicationServer(null, null, gameManagerMock.Object, null);
            cs.RegisterGame(
                new RegisterGame()
                {
                    NewGameInfo =new GameInfo()
                        {
                            gameName="TEST",
                            blueTeamPlayers=5,
                            redTeamPlayers=5
                        }
                }, 
                null);
            gameManagerMock.Verify(gman => gman.AddGame(It.Is<Game>(g=>g.Name.Equals("TEST"))));
            Assert.Throws(
                typeof(CannotCompleteRequestException),
                () => cs.RegisterGame(
                        new RegisterGame()
                        {
                            NewGameInfo = new GameInfo()
                            {
                                gameName = "EXCEPTION",
                                blueTeamPlayers = 5,
                                redTeamPlayers = 5
                            }
                        },
                        null)
                );
        }

        [Test, Description("CommunicationServer ForwardJoinGameRequest method test")]
        public void ForwardJoinGameRequestTest()
        {
            var gameMasterMock = new Mock<IAgent>();
            gameMasterMock.Setup(
                                gm => gm.SendMessage(It.Is<JoinGame>(
                                            msg => msg.gameName == "TEST" &&
                                                   msg.playerIdSpecified == true &&
                                                   msg.preferredRole == PlayerType.leader &&
                                                   msg.preferredTeam == TeamColour.blue)));

            var game = new Game(gameMasterMock.Object,
                                new GameInfo()
                                {
                                    gameName = "TEST",
                                    blueTeamPlayers = 5,
                                    redTeamPlayers = 5
                                }
                               );
            ulong x = game.Id;
            var gameManagerMock = new Mock<GameManager>();
            gameManagerMock.Setup(
                                gman => gman.GetGameByName(It.Is<string>(arg => arg.Equals("TEST"))))
                                .Returns(game);
            gameManagerMock.Setup(
                                gman => gman.AddPlayer(It.IsAny<IAgent>()));

            var cs = new ProjectGame.CommunicationServer
                                .CommunicationServer(null, null, gameManagerMock.Object, null);
            cs.ForwardJoinGameRequest(
                new JoinGame()
                {
                    gameName = "TEST",
                    preferredRole = PlayerType.leader,
                    preferredTeam = TeamColour.blue
                },
                new Mock<IAgent>().Object);
            gameManagerMock.Verify(gman => gman.AddPlayer(It.IsAny<IAgent>()));
            gameMasterMock.Verify(gm => gm.SendMessage(It.IsAny<Message>()));
        }

        [Test, Description("CommunicationServer AddPlayerToGame method test")]
        public void AddPlayerToGameTest()
        {
            var gameMasterMock = new Mock<IAgent>();
            var game = new Game(gameMasterMock.Object, new GameInfo());
            ulong x = game.Id;
            ulong playerId = 5;
            var playerMock = new Mock<IAgent>();
            playerMock.Setup(pl => pl.Id).Returns(playerId);
            var player = playerMock.Object;
            var gameManagerMock = new Mock<GameManager>();
            gameManagerMock.Setup(
                                gman => gman.GetGameById(It.IsAny<ulong>()))
                                .Returns(game);
            gameManagerMock.Setup(
                                gman => gman.GetPlayerById(It.Is<ulong>(arg => arg == playerId)))
                                .Returns(player);
            gameManagerMock.Setup(
                                gman => gman.AddPlayerToGame(It.Is<IAgent>(a => a.Equals(player)), TeamColour.blue, game));

            var cs = new ProjectGame.CommunicationServer
                                .CommunicationServer(null, null, gameManagerMock.Object, null);
            cs.AddPlayerToGame(x, TeamColour.blue, playerId);
            gameManagerMock.Verify(gman => gman.AddPlayerToGame(It.IsAny<IAgent>(),It.IsAny<TeamColour>(), It.IsAny<Game>()));
        }

    }
}
