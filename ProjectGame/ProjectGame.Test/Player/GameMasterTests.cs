﻿using System;
using NUnit.Framework;
using ProjectGame.Common.Messages;
using Moq;

namespace ProjectGame.Test.Player
{
    [TestFixture, Description("Unit Tests for GameMaster class")]
    public class GameMasterTests
    {
        [Test, Description("GameMaster sending RegisterGame message")]
        public void GameMasterSendsRegisterGamemessage()
        {
            //Arrange
            var playerMock= new Mock<ProjectGame.Player.Player>(null);
            Message msg = TestHelper.GetRandomRegisterGame();
            var gm = new ProjectGame.Player.Agents.GameMaster(playerMock.Object,1);
            //Act & Assert
            Assert.Throws(typeof(NotSupportedException),()=>gm.SendMessage(msg));
        }
        [Test, Description("GameMaster sending GameMessage message")]
        public void GameMasterSendsGameMessage()
        {
            //Arrange
            bool playersMethodCalled = false;
            var playerMock = new Mock<ProjectGame.Player.Player>(null);
            GameMessage msg = new PlacePiece();
            playerMock.Setup(p => p.SendMessage(It.Is<Message>(m=> m is GameMessage)))
                .Callback(()=>playersMethodCalled=true);
            playerMock.Setup(p => p.Id).Returns(12);
            var gm = new ProjectGame.Player.Agents.GameMaster(playerMock.Object, 1);
            //Act
            gm.SendMessage(msg);
            //Assert
            Assert.IsTrue(playersMethodCalled);
            Assert.AreEqual(msg.gameId, 1);
        }
    }
}
