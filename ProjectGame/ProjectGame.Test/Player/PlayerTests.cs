﻿using NUnit.Framework;
using ProjectGame.Common.Messages;
using ProjectGame.Common;
using Moq;
using System.Threading;
using System;
using ProjectGame.Player;
using System.Collections.Generic;
using ProjectGame.Player.Agents;

namespace ProjectGame.Test.Player
{
    [TestFixture, Description("Unit Tests for Player class")]
    public class PlayerTests
    {
        

        [Test, Description("Player AddPlayer test")]
        public void PlayerAddPlayerTest()
        {
            //Arrange
            var player = new ProjectGame.Player.Player(players: new List<OtherPlayer>());
            var player2 = new ProjectGame.Common.Messages.Player()
            {
                id = 1234,
                team = TeamColour.red,
                type = PlayerType.member
            };
            //Act
            player.AddPlayer(player2);
            //Assert
            Assert.IsTrue(player._playersField.Find(p => p.Id == 1234) != null);
        }

        [Test, Description("Player GetGames test")]
        public void GetGamesTest()
        {
            var clientMock = new Mock<CommunicationClient>(null,new CancellationTokenSource(),null);
            clientMock.Setup(cl => cl.SendMessage(It.IsAny<GetGames>()));
            new ProjectGame.Player.Player(clientMock.Object).GetGames();
            clientMock.Verify(cl => cl.SendMessage(It.IsAny<GetGames>()));
        }



        [Test, Description("Player send message test")]
        public void PlayerSendMessageTest()
        {
            var clientMock = new Mock<CommunicationClient>(null, new CancellationTokenSource(), null);
            clientMock.Setup(cl => cl.SendMessage(It.IsAny<GetGames>()));
            new ProjectGame.Player.Player(clientMock.Object).SendMessage(new GetGames());
            clientMock.Verify(cl => cl.SendMessage(It.IsAny<GetGames>()));

        }

        [Test, Description("Player Register method test + SetExpectations")]
        public void PlayerRegisterAndSetExpecttionsTest()
        {
            var clientMock = new Mock<CommunicationClient>(null, new CancellationTokenSource(), null);
            clientMock.Setup(cl => cl.SendMessage(It.Is<JoinGame>(m=>m.gameName.Equals("TESTTEST")&&
                                                                     m.preferredRole.Equals(PlayerType.leader)&&
                                                                     m.preferredTeam.Equals(TeamColour.red))));
            var pl = new ProjectGame.Player.Player(clientMock.Object);
            pl.SetExpectations("TESTTEST", "red", "leader");
            pl.Register();
            clientMock.Verify(cl => cl.SendMessage(It.IsAny<JoinGame>()));

        }

        [Test, Description("Player UpdateBoard method test")]
        public void PlayerUpdateBoardTest()
        {
            var boardMock = new Mock<Board>(null, new List<OtherPlayer>(), null,(uint)0, (uint)0, (uint)0,TeamColour.blue,(ulong)0,"",(ulong)0);
            boardMock.Setup(b => b.Update(It.IsAny<Data>()));
            new ProjectGame.Player.Player(null, null,PlayerType.leader, TeamColour.blue, boardMock.Object).UpdateBoard(new Data());
            boardMock.Verify(b => b.Update(It.IsAny<Data>()));
        }

        [Test, Description("Player GameFinished method test")]
        public void PlayerGameFinishedTest()
        {
            var boardMock = new Mock<Board>(null, new List<OtherPlayer>(), null, (uint)0, (uint)0, (uint)0, TeamColour.blue, (ulong)0, "", (ulong)0);
            boardMock.Setup(b => b.DisplayBoard());
            var list = new List<OtherPlayer>();
            list.Add(new OtherPlayer(new ProjectGame.Player.Player(),new ProjectGame.Common.Messages.Player()));
            new ProjectGame.Player.Player(null, null, PlayerType.leader, TeamColour.blue, boardMock.Object, list).GameFinished();
            boardMock.Verify(b => b.DisplayBoard());
            Assert.AreEqual(list.Count, 0);
        }
        [Test, Description("Player RejectKnowledgeExchange method test")]
        public void PlayerRejectKnowledgeExchangeTest()
        {
            var msg = new ProjectGame.Player.Player(col: TeamColour.red).RejectKnowledgeExchange(123);
            Assert.AreEqual(msg.permanent, false);
            Assert.AreEqual(msg.playerId, 123);
            Assert.AreEqual(msg.senderPlayerId, 123123);
        }
        [Test, Description("Player AuthorizeKnowledgeExchange method test")]
        public void PlayerAuthorizeKnowledgeExchangeTest()
        {
           Guid guid = Guid.NewGuid();
            var msg = new ProjectGame.Player.Player(col: TeamColour.red, guid:guid.ToString(), gameId:2).AuthorizeKnowledgeExchange(123);
            Assert.AreEqual(msg.withPlayerId, 123);
            Assert.AreEqual(msg.playerGuid,guid.ToString());
            Assert.AreEqual(msg.gameId, 2);
        }
        //[Test, Description("Player ShouldAcceptKnowledgeExchange method test - leader asks")]
        //public void PlayerShouldAcceptKnowledgeExchangeFromLeaderTest()
        //{
        //    var boardMock = new Mock<Board>(null, new List<OtherPlayer>(), null, (uint)0, (uint)0, (uint)0, TeamColour.blue, (ulong)0);
        //    boardMock.Setup(b => b.IsLeader(It.IsAny<ulong>())).Returns(true);
        //    boardMock.Setup(b => b.IsLeader()).Returns(false);
        //    bool result = new ProjectGame.Player.Player(board:boardMock.Object).ShouldAcceptKnowledgeExchange(3);
        //    Assert.IsTrue(result);
        //}
    }
}
