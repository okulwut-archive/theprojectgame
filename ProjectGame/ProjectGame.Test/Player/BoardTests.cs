﻿using Moq;
using NUnit.Framework;
using ProjectGame.Common;
using ProjectGame.Common.Messages;
using ProjectGame.Player;
using ProjectGame.Player.Agents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ProjectGame.Test.Player
{
    [TestFixture, Description("Unit Tests for Board class")]
    public class BoardTests
    {
        [Test, Description("Board Move method test")]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void MoveTest(int x)
        {
            var gameMasterMock = new Mock<IAgent>();
            gameMasterMock.Setup(
                                gm => gm.SendMessage(It.Is<Move>(
                                            msg => msg.direction == (MoveType)x)));
            Board b = new Board(gameMaster:gameMasterMock.Object, otherPlayers:new List<OtherPlayer>(),location:null, tasksHeight: 0, goalsHeight: 0, width: 0, color: TeamColour.blue, id: 0, guid:null, gameId:0);
            b.Move((MoveType)x);
            gameMasterMock.Verify(gm => gm.SendMessage(It.IsAny<Move>()));
        }

        [Test, Description("Board Discover method test")]
        public void DiscoverTest()
        {
            var gameMasterMock = new Mock<IAgent>();
            gameMasterMock.Setup(
                                gm => gm.SendMessage(It.IsAny<Discover>()));
            Board b = new Board(gameMasterMock.Object, new List<OtherPlayer>(), null, 0, 0, 0, TeamColour.blue, 0,"",0);
            b.Discover();
            gameMasterMock.Verify(gm => gm.SendMessage(It.IsAny<Discover>()));
        }

        [Test, Description("Board TestPiece method test")]
        public void TestPieceTest()
        {
            var gameMasterMock = new Mock<IAgent>();
            gameMasterMock.Setup(
                                gm => gm.SendMessage(It.IsAny<TestPiece>()));
            Board b = new Board(gameMasterMock.Object, new List<OtherPlayer>(), null, 0, 0, 0, TeamColour.blue, 0,"",0);
            b.TestPiece();
            gameMasterMock.Verify(gm => gm.SendMessage(It.IsAny<TestPiece>()));
        }

        [Test, Description("Board PickUpPiece method test")]
        public void PickUpPieceTest()
        {
            var gameMasterMock = new Mock<IAgent>();
            gameMasterMock.Setup(
                                gm => gm.SendMessage(It.IsAny<PickUpPiece>()));
            Board b = new Board(gameMasterMock.Object, new List<OtherPlayer>(), null, 0, 0, 0, TeamColour.blue, 0,"",0);
            b.PickUpPiece();
            gameMasterMock.Verify(gm => gm.SendMessage(It.IsAny<PickUpPiece>()));
        }

        [Test, Description("Board PlacePiece method test")]
        public void PlacePieceTest()
        {
            var gameMasterMock = new Mock<IAgent>();
            gameMasterMock.Setup(
                                gm => gm.SendMessage(It.IsAny<PlacePiece>()));
            Board b = new Board(gameMasterMock.Object, new List<OtherPlayer>(), null, 0, 0, 0, TeamColour.blue, 0,"",0);
            b.PlacePiece();
            gameMasterMock.Verify(gm => gm.SendMessage(It.IsAny<PlacePiece>()));
        }

        [Test, Description("Board Update method test")]
        public void UpdateTest()
        {
            var fields = new Field[3, 3];
            Board b = new Board(new List<OtherPlayer>(), fields, null, null, null, 1, 1, 3, TeamColour.blue, 0);
            var oldtimestamp = DateTime.Now;
            Thread.Sleep(40);
            b.Update(new Data()
            {
                TaskFields = new[] { new TaskField() { x = 1, y = 1, distanceToPiece = 3, timestamp = DateTime.Now } },
                GoalFields = new[] { new GoalField() { x = 0, y = 0, playerIdSpecified = false, timestamp = DateTime.Now } }
            });
            b.Update(new Data()
            {
                TaskFields = new[] { new TaskField() { x = 1, y = 1, distanceToPiece = 30, timestamp = oldtimestamp } },
                GoalFields = new[] { new GoalField() { x = 0, y = 0, playerIdSpecified = true, timestamp = oldtimestamp } }
            });
            Assert.AreEqual((fields[0, 0] as GoalField).playerIdSpecified, false);
            Assert.IsFalse(oldtimestamp.Equals(fields[0, 0].timestamp));
            Assert.AreEqual((fields[1, 1] as TaskField).distanceToPiece, 3);
            Assert.IsFalse(oldtimestamp.Equals(fields[1, 1].timestamp));
        }
        [Test, Description("Board AcceptKnowledgeExchange and CompleteKnowledgeExchange methods test")]
        public void AcceptAndCompleteKnowledgeExchangeTest()
        {
            var board = new Board(new List<OtherPlayer>());
            Assert.IsFalse(board.AcceptedExchange);
            board.AcceptKnowledgeExchange();
            Assert.IsTrue(board.AcceptedExchange);
            board.KnowledgeExchangeComplete();
            Assert.IsFalse(board.AcceptedExchange);

        }

        //[Test, Description("Board IsLeader method test")]
        //public void IsLeaderTest()
        //{
        //    var otherPlayerMock = new Mock<OtherPlayer>(null,(ulong)0);
        //    otherPlayerMock.Setup(other => other.IsLeader(It.Is<TeamColour>(tc => tc == TeamColour.blue))).Returns(true);
        //    otherPlayerMock.Setup(other => other.IsLeader(It.Is<TeamColour>(tc => tc == TeamColour.red))).Returns(false);
        //    otherPlayerMock.Setup(other => other.Id).Returns(10);
        //    var list = new List<OtherPlayer>();
        //    list.Add(otherPlayerMock.Object);
        //    var board = new Board(list, color: TeamColour.red);
        //    Assert.IsFalse(board.IsLeader(10));
        //    board = new Board(list, color: TeamColour.blue, id: 10);
        //    Assert.IsTrue(board.IsLeader(10));
        //    Assert.IsTrue(board.IsLeader());

        //    otherPlayerMock = new Mock<OtherPlayer>(null, (ulong)0);
        //    otherPlayerMock.Setup(other => other.IsLeader(It.Is<TeamColour>(tc => tc == TeamColour.blue))).Returns(false);
        //    otherPlayerMock.Setup(other => other.IsLeader(It.Is<TeamColour>(tc => tc == TeamColour.red))).Returns(false);
        //    otherPlayerMock.Setup(other => other.Id).Returns(10);
        //    list = new List<OtherPlayer>();
        //    list.Add(otherPlayerMock.Object);
        //    board = new Board(list, color: TeamColour.blue, id: 10);
        //    Assert.IsFalse(board.IsLeader());
        //}
        [Test, Description("Board Send method test")]
        public void SendTest()
        {
            var piecesDict = new Dictionary<ulong, Piece>();
            piecesDict.Add(0, new Piece()
            {
                id = 0,
                playerIdSpecified = false,
                type = PieceType.unknown
            });
            piecesDict.Add(1, new Piece()
            {
                id = 1,
                playerIdSpecified = false,
                type = PieceType.sham
            });
            Field[,] fields = new Field[2, 2];
            fields[0, 0] = new GoalField()
            {
                playerIdSpecified = true,
                playerId = 1,
                team = TeamColour.red,
                timestamp = new DateTime(year: 1410, month: 7, day: 15),
                type = GoalFieldType.unknown,
                x = 0,
                y = 0
            };
            fields[0, 1] = new GoalField()
            {
                playerIdSpecified = false,
                team = TeamColour.red,
                timestamp = new DateTime(year: 1410, month: 7, day: 16),
                type = GoalFieldType.unknown,
                x = 0,
                y = 1
            };
            fields[1, 0] = new TaskField()
            {
                playerIdSpecified = false,
                timestamp = new DateTime(year: 1410, month: 8, day: 16),
                x = 1,
                y = 0,
                distanceToPiece=3
            };
            fields[1, 1] = new TaskField()
            {
                playerIdSpecified = false,
                timestamp = new DateTime(year: 1411, month: 7, day: 16),
                x = 1,
                y = 1,
                distanceToPiece = 4
            };
            var board = new Board(otherPlayers:new List<OtherPlayer>(),pieces: piecesDict, fields: fields);
            var data=board.Send(12);
            Assert.AreEqual(data.playerId, 12);
            Assert.AreEqual(data.Pieces.Where(piece => piece.playerIdSpecified == false && piece.type == PieceType.unknown).Count(), 1);
            Assert.AreEqual(data.Pieces.Where(piece => piece.playerIdSpecified == false && piece.type == PieceType.sham).Count(), 1);
            Assert.AreEqual(data.Pieces.Count(), 2);
            Assert.AreEqual(data.GoalFields.Count(), 2);
            Assert.AreEqual(data.TaskFields.Count(), 2);
            Assert.AreEqual(data.TaskFields.Where(tf => tf.distanceToPiece == 4 && tf.timestamp.Year == 1411 && tf.x == 1 && tf.y == 1).Count(), 1);
            Assert.AreEqual(data.GoalFields.Where(gf => gf.x == 0 && gf.y == 0 && gf.playerIdSpecified && gf.playerId == 1).Count(), 1);
        }


    }
}
