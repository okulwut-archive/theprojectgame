﻿using NUnit.Framework;
using ProjectGame.Common.Messages;
using ProjectGame.Common;
using Moq;
using System.Threading;
using System;
using ProjectGame.Player;
using System.Collections.Generic;
using ProjectGame.Player.Agents;
using ProjectGame.Player.Strategies;

namespace ProjectGame.Test.Player
{
    [TestFixture, Description("Unit Tests for PlayerStrategy class")]
    public class PlayerStrategyTests
    {
        [Test, Description("PlayerStrategy MakeNextMove test")]
        [TestCase(true,false,0,0,0,0,MoveType.down,GoalFieldType.unknown)]
        [TestCase(false, true, 5, 5, 10, 2, MoveType.down, GoalFieldType.unknown)]
        [TestCase(false, true, 5, 5, 10, 2, MoveType.down, GoalFieldType.goal)]
        [TestCase(false, false, 5, 9, 10, 2, MoveType.down, GoalFieldType.goal)]
        [TestCase(false, false, 5, 5, 10, 2, MoveType.down, GoalFieldType.goal)]
        public void MakeNextMoveTest(
            bool gameFinished,bool holdingPiece, int xloc, int yloc, int boardSize, 
            int goalSize, MoveType direction, GoalFieldType fieldType)
        {
            Field[,] fields = new Field[boardSize, boardSize];
            var boardMock = new Mock<Board>(null, new List<OtherPlayer>(), null, (uint)0, (uint)0, (uint)0, TeamColour.blue, (ulong)0, "", (ulong)0);
            boardMock.Setup(b => b.GameFinished).Returns(gameFinished);
            boardMock.Setup(b => b.IsPlayerHoldingPiece).Returns(holdingPiece);
            boardMock.Setup(b => b.PlayerLocation).Returns(new Location() {x=(uint)xloc,y=(uint)yloc });
            boardMock.Setup(b => b.BoardHeight).Returns((uint)boardSize);
            boardMock.Setup(b => b.BoardWidth).Returns((uint)boardSize);
            boardMock.Setup(b => b.GoalsHeight).Returns((uint)goalSize);
            boardMock.Setup(b => b.GoalsDirection).Returns(direction);
            boardMock.Setup(b => b.Fields).Returns(fields);

            boardMock.Setup(b => b.Move(It.IsAny<MoveType>()));
            boardMock.Setup(b => b.PlacePiece());
            boardMock.Setup(b => b.PickUpPiece());
            boardMock.Setup(b => b.Discover());

            if (gameFinished)
            {
                Assert.IsTrue(new StandardPlayerStrategy(boardMock.Object).MakeNextMove());
            }
            else if (holdingPiece)
            {
                if(yloc>=goalSize&&yloc<(boardSize-goalSize))
                {
                    new StandardPlayerStrategy(boardMock.Object).MakeNextMove();
                    boardMock.Verify(b => b.Move(direction));
                }
                else
                {
                    fields[yloc, xloc] = new GoalField() {type=fieldType };
                    if (fieldType == GoalFieldType.unknown)
                    {
                        new StandardPlayerStrategy(boardMock.Object).MakeNextMove();
                        boardMock.Verify(b => b.PlacePiece());
                    }
                    else
                    {
                        for(int i=0;i<fields.GetLength(0);i++)
                            for (int j = 0; j < fields.GetLength(1); j++)
                                fields[i,j]= new GoalField() { type = GoalFieldType.goal };
                        if (direction == MoveType.up)
                            fields[0, 0] = new GoalField() { type = GoalFieldType.unknown };
                        if (direction == MoveType.down)
                            fields[fields.GetLength(0)-1, 0] = 
                                            new GoalField() { type = GoalFieldType.unknown };
                        new StandardPlayerStrategy(boardMock.Object).MakeNextMove();
                        boardMock.Verify(b => b.Move(xloc==0?direction:MoveType.left));

                    }
                }
            }
            else
            {
                if (yloc < goalSize && yloc >= (boardSize - goalSize))
                {
                    new StandardPlayerStrategy(boardMock.Object).MakeNextMove();
                    boardMock.Verify(b => b.Move(direction==MoveType.down?MoveType.up:MoveType.down));
                }
                else
                {
                    fields[yloc, xloc] = new TaskField() { distanceToPiece = 0 };
                    new StandardPlayerStrategy(boardMock.Object).MakeNextMove();
                    boardMock.Verify(b => b.PickUpPiece());
                    fields[yloc, xloc] = new TaskField() { distanceToPiece = 1 };
                    fields[yloc, xloc+1] = new TaskField() { distanceToPiece = 0 };
                    var plStr = new StandardPlayerStrategy(boardMock.Object);
                    plStr.MakeNextMove();
                    boardMock.Verify(b => b.Discover());
                    plStr.MakeNextMove();
                    boardMock.Verify(b => b.Move(MoveType.right));
                }
            }
            
        }
    }
}
