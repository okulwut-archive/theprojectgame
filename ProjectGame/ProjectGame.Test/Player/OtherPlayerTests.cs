﻿using System;
using NUnit.Framework;
using ProjectGame.Common.Messages;
using Moq;

namespace ProjectGame.Test.Player
{
    [TestFixture, Description("Unit Tests for OtherPlayer class")]
    public class OtherPlayerTests
    {
        [Test, Description("OtherPlayer sending RegisterGame message")]
        public void OtherPlayerSendsRegisterGameMessage()
        {
            //Arrange
            var playerMock = new Mock<ProjectGame.Player.Player>(null);
            Message msg = TestHelper.GetRandomRegisterGame();
            var otherPlayer = new ProjectGame.Player.Agents.OtherPlayer(playerMock.Object, null);
            //Act & Assert
            Assert.Throws(typeof(NotSupportedException), () => otherPlayer.SendMessage(msg));
        }
        [Test, Description("OtherPlayer sending Information Exchange message")]
        public void OtherPlayerSendsInformationExchangeMessage()
        {
            //Arrange
            bool playersMethodCalled = false;
            var playerMock = new Mock<ProjectGame.Player.Player>(null);
            BetweenPlayersMessage msg = new KnowledgeExchangeRequest();
            playerMock.Setup(p => p.SendMessage(It.Is<Message>(m => m is BetweenPlayersMessage)))
                .Callback(() => playersMethodCalled = true);
            playerMock.Setup(p => p.Id).Returns(12);
            var otherPlayer = new ProjectGame.Player.Agents.OtherPlayer(playerMock.Object, new ProjectGame.Common.Messages.Player());
            //Act
            otherPlayer.SendMessage(msg);
            //Assert
            Assert.IsTrue(playersMethodCalled);
            Assert.AreEqual(msg.senderPlayerId, 12);
        }
    }
}
