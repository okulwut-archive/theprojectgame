﻿using ProjectGame.Common.Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Xml.Serialization;

namespace ProjectGame.Test
{
    abstract class TestHelper
    {
        private static Random r = new Random();
        public static List<Message> ReceiveAll(NetworkStream stream, List<Type> types)
        {
            var reader = new StreamReader(stream);
            int bufferSize = 1024;
            char[] buffer = new char[bufferSize];
            StringBuilder sb = new StringBuilder();
            List<Message> messages=new List<Message>();
            var readBytes = 0;
            do
            {
                readBytes = reader.Read(buffer, 0, bufferSize);
                sb.Append(buffer, 0, readBytes);

            } while (readBytes == 1024);
            reader.Close();
            string[] receivedMsgStrings = sb.ToString().Split('\u0017');
            int k = 0;
            for(int i=0;i<receivedMsgStrings.Length;i++)
            {
                if (receivedMsgStrings[i].Length < 2) continue;
                XmlSerializer serializer = new XmlSerializer(types[k++]);//ProjectGame.Common.XmlSerializerFactory.GetDeserializer(msgStringWithNoEoTSymbol);
                using (var stringReader = new StringReader(receivedMsgStrings[i]))
                {
                    messages.Add((Message)serializer.Deserialize(stringReader));
                }
            }
            
            return messages;
        }
        public static Message Receive(NetworkStream stream, Type type)
        {
            List<Type> types = new List<Type>();
            types.Add(type);
            var res = ReceiveAll(stream, types);
            if (res.Count == 0) return null;
            return res.First();
        }
        public static ConfirmJoiningGame GetSampleConfirmJoiningGame()
        {
            return new ConfirmJoiningGame()
            {
                gameId = 1,
                PlayerDefinition = new ProjectGame.Common.Messages.Player()
                {
                    id = 1,
                    team = TeamColour.blue,
                    type = PlayerType.leader
                },
                playerId = 1,
                privateGuid = "1"
            };
        }
        public static ConfirmJoiningGame GetRandomConfirmJoiningGame()
        {
            return new ConfirmJoiningGame()
            {
                gameId = (ulong)r.Next(),
                PlayerDefinition = new ProjectGame.Common.Messages.Player()
                {
                    id = (ulong)r.Next(),
                    team = r.Next() % 2 == 0 ? TeamColour.blue:TeamColour.red,
                    type = r.Next()%2==0?PlayerType.leader:PlayerType.member
                },
                playerId = (ulong)r.Next(),
                privateGuid = ((ulong)r.Next()).ToString()
            };
        }
        public static RegisterGame GetSampleRegisterGame()
        {
            return new RegisterGame()
            {
                NewGameInfo = new GameInfo()
                {
                    blueTeamPlayers = 1,
                    gameName = "game",
                    redTeamPlayers = 2
                }
            };
        }
        public static RegisterGame GetRandomRegisterGame()
        {
            return new RegisterGame()
            {
                NewGameInfo = new GameInfo()
                {
                    blueTeamPlayers = (ulong)r.Next(),
                    gameName = ((ulong)r.Next()).ToString(),
                    redTeamPlayers = (ulong)r.Next()
                }
            };
        }
        public static bool AreEqual(RegisterGame g1, RegisterGame g2)
        {
            return
                g1.NewGameInfo.blueTeamPlayers == g2.NewGameInfo.blueTeamPlayers &&
                g1.NewGameInfo.redTeamPlayers == g2.NewGameInfo.redTeamPlayers &&
                g1.NewGameInfo.gameName.Equals(g2.NewGameInfo.gameName);
        }
        public static bool AreEqual(ConfirmJoiningGame msg1, ConfirmJoiningGame msg2)
        {
            bool result= msg1.gameId.Equals(msg2.gameId) &&
                msg1.playerId.Equals(msg2.playerId) &&
                msg1.privateGuid.Equals(msg2.privateGuid) &&
                msg1.PlayerDefinition.id.Equals(msg2.PlayerDefinition.id) &&
                msg1.PlayerDefinition.team.Equals(msg2.PlayerDefinition.team) &&
                msg1.PlayerDefinition.type.Equals(msg2.PlayerDefinition.type);
            return result;
        }
        public static bool AreEqual(Message msg1, Message msg2)
        {
            if (msg1 is ConfirmJoiningGame && msg2 is ConfirmJoiningGame)
                return AreEqual((ConfirmJoiningGame)msg1, (ConfirmJoiningGame)msg2);
            if (msg1 is RegisterGame && msg2 is RegisterGame)
                return AreEqual((RegisterGame)msg1, (RegisterGame)msg2);
            throw new ArgumentException();
        }
    }
}
