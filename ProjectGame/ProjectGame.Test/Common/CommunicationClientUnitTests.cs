﻿using NUnit.Framework;
using ProjectGame.Common;
using System.Threading;
using ProjectGame.Common.Messages;
using System.Collections.Concurrent;
using Moq;

namespace ProjectGame.Test.Common
{
    [TestFixture, Description("Unit Tests for CommunicationClient class")]
    public class CommunicationClientUnitTests
    {
        [Test, Description("CommunicationClient GetMessage method test")]
        public void GetMessageTest()
        {
            var msgQue = new BlockingCollection<Message>();
            var source = new CancellationTokenSource();
            var msg = new Data();
            var msg2 = new RegisterGame();
            msgQue.Add(msg);
            msgQue.Add(msg2);
            var client = new CommunicationClient(msgQue,source);
            Assert.AreEqual(client.GetMessage(), msg);
            Assert.AreEqual(client.GetMessage(), msg2);
            Message msg3 = null;
            var t = new Thread(() => { try { msg3 = client.GetMessage(); } catch { } });
            t.Start();
            Thread.Sleep(200);
            source.Cancel();
            t.Join();
            Assert.IsNull(msg3);
        }

        [Test, Description("CommunicationClient TryGetMessage method test")]
        public void TryGetMessageTest()
        {
            var msgQue = new BlockingCollection<Message>();
            var source = new CancellationTokenSource();
            var msg = new Data();
            var msg2 = new RegisterGame();
            msgQue.Add(msg);
            msgQue.Add(msg2);
            var client = new CommunicationClient(msgQue, source);
            Assert.AreEqual(client.TryGetMessage(new System.TimeSpan(100)), msg);
            Assert.AreEqual(client.TryGetMessage(new System.TimeSpan(100)), msg2);
            Assert.IsNull(client.TryGetMessage(new System.TimeSpan(100)));
        }

        [Test, Description("CommunicationClient SendMessage method test")]
        public void SendMessageTest()
        {
            var streamMock = new Mock<CommunicationStream>();
            var msg = new Data();
            streamMock.Setup(stream => stream.SendMessage(It.IsAny<Message>()));
            var client = new CommunicationClient(null, null, streamMock.Object);
            client.SendMessage(new Data());
            streamMock.Verify(cs => cs.SendMessage(It.IsAny<Message>()));
        }

    }
}
