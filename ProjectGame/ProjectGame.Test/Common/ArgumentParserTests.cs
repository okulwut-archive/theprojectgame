﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using ProjectGame.Common;

namespace ProjectGame.Test.Common
{
    [TestFixture, Description("Unit Tests for ArgumentParser class")]
    public class ArgumentParserTests
    {
        [Test, Description("Valid arguments, no white spaces")]
        public void ArgumentParserValidArgumentsTest()
        {
            //Arrange
            var args = new[] { "key1=val1|key2=val2|key3=val3" };
            var requiredKeys = new List<string>(new[] { "key1", "key2", "key3" });
            var shouldBe = new Dictionary<string, string>();
            shouldBe.Add("key1", "val1");
            shouldBe.Add("key2", "val2");
            shouldBe.Add("key3", "val3");
            //Act
            var result = ArgumentParser.Parse(args, requiredKeys);
            //Assert
            Assert.IsTrue(AreEqual(result, shouldBe));
        }
        [Test, Description("Invalid arguments (required argument missing)")]
        public void ArgumentParserArgumentMissingTest()
        {
            //Arrange
            var args = new[] { "key1=val1", "key2=val2", "key3=val3" };
            var requiredKeys = new List<string>(new[] { "key1", "key2", "key3", "key4" });

            //Act & Assert
            Assert.Throws<ArgumentException>(() => ArgumentParser.Parse(args, requiredKeys));

        }

        [Test, Description("Empty input")]
        public void ArgumentParserEmptyInputTest()
        {
            //Arrange
            var args = new string[0];
            var requiredKeys = new List<string>(new string[0]);
            var shouldBe = new Dictionary<string, string>();
            //Act
            var result = ArgumentParser.Parse(args, requiredKeys);
            //Assert
            Assert.IsTrue(AreEqual(result, shouldBe));
        }

        [Test, Description("Valid arguments")]
        [TestCase("1234",1234)]
        [TestCase("12344", 12344)]
        [TestCase("12234", 12234)]
        [TestCase("80", 80)]
        [TestCase("1", 1)]
        [TestCase("12234", 12234)]
        public void ArgumentParserPortValidArgumentTest(string arg, int shouldBe)
        {
            //Arrange

            //Act
            var result = ArgumentParser.GetPort(arg);
            //Assert
            Assert.AreEqual(result, shouldBe);
        }

        [Test, Description("Invalid arguments")]
        [TestCase("-1234")]
        [TestCase("9999999")]
        [TestCase("1/23")]
        [TestCase("qwerty")]
        [TestCase("65536")]
        [TestCase("0")]
        public void ArgumentParserPortValidArgumentTest(string arg)
        {
            //Arrange

            //Act & Assert
            Assert.Throws<ArgumentException>(() => ArgumentParser.GetPort(arg));
        }

        private static bool AreEqual(Dictionary<string,string> d1, Dictionary<string, string> d2)
        {
            if (d1.Count != d2.Count)
                return false;
            foreach(var el in d1.Keys)
            {
                string s1=null,s2=null;
                d1.TryGetValue(el, out s1);
                d2.TryGetValue(el, out s2);
                if (!s1.Equals(s2))
                    return false;
            }
            return true;
        }

    }
}
