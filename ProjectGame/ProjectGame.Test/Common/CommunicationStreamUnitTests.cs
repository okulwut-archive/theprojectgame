﻿using NUnit.Framework;
using ProjectGame.Common;
using ProjectGame.Common.Messages;
using System.IO;
using System.Threading;
using System.Xml.Serialization;

namespace ProjectGame.Test.Common
{
    [TestFixture, Description("Unit Tests for CommunicationStream class")]
    public class CommunicationStreamUnitTests
    {
        [Test, Description("CommunicationStream SendMessage method test")]
        public void SendMessageTest()
        {
            var memStream = new MemoryStream();
            var stream = new CommunicationStream(memStream);
            var message = new RegisterGame();
            stream.SendMessage(message);
            string str = System.Text.Encoding.UTF8.GetString(memStream.GetBuffer());
            memStream.Close();
            str=str.TrimEnd('\0');
            Assert.IsTrue(str.EndsWith(""+(char)23));
            var serializer = new XmlSerializer(typeof(RegisterGame));
            memStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(str.Substring(0,str.Length-1)));
            RegisterGame msg = (RegisterGame) serializer.Deserialize(memStream);
            memStream.Close();
        }

        [Test, Description("CommunicationStream ReadMessages method test")]
        public void ReadMessagesTest()
        {
            var memStream = new MemoryStream();
            var stream = new CommunicationStream(memStream);
            var writer = new StreamWriter(memStream);
            writer.Write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<Move playerGuid=\"00000000-0000-0000-0000-000000000000\" gameId=\"1\" direction=\"down\" xmlns=\"https://se2.mini.pw.edu.pl/17-results/\" />\u0017");
            writer.Flush();
            memStream.Position = 0;
            int msgCounter = 0;
            CancellationTokenSource source = new CancellationTokenSource();
            var t = new Thread(() => stream.ReadMessages((source.Token), (msg, tok) => msgCounter++).Wait());
            t.Start();
            Thread.Sleep(100);
            source.Cancel();
            t.Join();
            Assert.AreEqual(1,msgCounter);
        }
    }
}
