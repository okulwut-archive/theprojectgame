﻿using NUnit.Framework;
using ProjectGame.Common;
using System.Net.Sockets;
using System.Threading;
using ProjectGame.Common.Messages;
using System.Collections.Concurrent;

namespace ProjectGame.Test.Common
{
    [TestFixture, Description("Integration Tests for CommunicationClient class")]
    public class CommunicationClientTests
    {
        [Test, Description("CommunicationClient sending RegisterGame message")]
        public void CommunicationClientSendRegisterGameTest()
        {
            //Arrange
            var listener = TcpListener.Create(1024);
            listener.Start();
            TcpClient serverside = null;
            var thread = new Thread(() => { serverside = listener.AcceptTcpClient(); listener.Stop();});
            thread.Start();
            var client = new CommunicationClient("localhost", 1024);
            thread.Join();
            var msgToSend = TestHelper.GetSampleRegisterGame();
            //Act
            client.SendMessage(msgToSend);
            RegisterGame message = (RegisterGame)TestHelper.Receive(
                serverside.GetStream(),typeof(RegisterGame));
            client.Dispose();
            serverside.Close();

            //Assert
            Assert.IsTrue(TestHelper.AreEqual(message, msgToSend));
        }

        [Test, Description("CommunicationClient receiving ConfirmJoiningGame message")]
        public void CommunicationClientReceiveConfirmJoiningGameTest()
        {
            //Arrange
            var listener = TcpListener.Create(1024);
            listener.Start();
            TcpClient serverside = null;
            var thread = new Thread(() => { serverside = listener.AcceptTcpClient(); listener.Stop(); });
            thread.Start();
            BlockingCollection<Message> msgCollection = new BlockingCollection<Message>();
            var msg = TestHelper.GetSampleConfirmJoiningGame();
            msgCollection.Add(msg);
            var client = new CommunicationClient("localhost", 1024,msgCollection);
            thread.Join();
            //Act
            var receivedMsg=client.GetMessage();
            client.Dispose();
            serverside.Close();
            //Assert
            Assert.AreEqual(receivedMsg, msg);
        }


    }
}
