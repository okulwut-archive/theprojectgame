﻿using System;
using NUnit.Framework;
using ProjectGame.Common;
using System.Net.Sockets;
using System.Threading;
using ProjectGame.Common.Messages;
using System.IO;
using System.Net;
using System.Collections.Generic;

namespace ProjectGame.Test.Common
{
    [TestFixture, Description("Integration Tests for CommunicationStream class")]
    public class CommunicationStreamTests
    {
        [Test, Description("CommunicationStream sending RegisterGame message")]
        public void CommunicationStreamSendMessageTest()
        {
            //Arrange
            TcpListener listener = new TcpListener(IPAddress.Any, 1024);
            listener.Start();
            TcpClient tcpClient1=null,tcpClient2=null;
            Thread thread = new Thread(()=> { tcpClient1 = listener.AcceptTcpClient(); listener.Stop(); });
            thread.Start();
            tcpClient2 = new TcpClient("localhost", 1024);
            thread.Join();
            CommunicationStream communicationStream = new CommunicationStream(tcpClient2.GetStream());
            var msg = TestHelper.GetSampleConfirmJoiningGame();
            //Act
            communicationStream.SendMessage(msg);
            ConfirmJoiningGame receivedMsg = (ConfirmJoiningGame)TestHelper.Receive(tcpClient1.GetStream(), typeof(ConfirmJoiningGame));
            communicationStream.Dispose();
            tcpClient1.Close();
            //Assert
            Assert.IsTrue(TestHelper.AreEqual(msg, receivedMsg));
        }
        [Test, Description("CommunicationStream sending many messages")]
        public void CommunicationStreamSendManyMessagesTest()
        {
            //Arrange
            TcpListener listener = new TcpListener(IPAddress.Any, 1024);
            listener.Start();
            TcpClient tcpClient1 = null, tcpClient2 = null;
            Thread thread = new Thread(() => { tcpClient1 = listener.AcceptTcpClient(); listener.Stop(); });
            thread.Start();
            tcpClient2 = new TcpClient("localhost", 1024);
            thread.Join();

            CommunicationStream communicationStream = new CommunicationStream(tcpClient2.GetStream());
            var msgs = new List<Message>();
            msgs.Add(TestHelper.GetRandomRegisterGame());
            msgs.Add(TestHelper.GetRandomRegisterGame());
            msgs.Add(TestHelper.GetRandomRegisterGame());
            msgs.Add(TestHelper.GetRandomRegisterGame());
            msgs.Add(TestHelper.GetRandomRegisterGame());
            msgs.Add(TestHelper.GetRandomConfirmJoiningGame());
            msgs.Add(TestHelper.GetRandomConfirmJoiningGame());
            msgs.Add(TestHelper.GetRandomConfirmJoiningGame());
            msgs.Add(TestHelper.GetRandomConfirmJoiningGame());
            var types = new List<Type>();
            //Act
            foreach (Message msg in msgs)
            {
                types.Add(msg.GetType());
                communicationStream.SendMessage(msg);
            }
            Thread.Sleep(200);
            List<Message> receivedMsgs =TestHelper.ReceiveAll(tcpClient1.GetStream(), types);
            communicationStream.Dispose();
            tcpClient1.Close();
            //Assert
            Assert.AreEqual(receivedMsgs.Count, msgs.Count);
            for(int i=0;i<receivedMsgs.Count;i++)
                Assert.IsTrue(TestHelper.AreEqual(msgs[i], receivedMsgs[i]));
        }

       
        [Test, Description("CommunicationStream keep alive test.")]
        public void CommunicationStreamKeepAliveTest()
        {
            //Arrange
            TcpListener listener = new TcpListener(IPAddress.Any, 1024);
            listener.Start();
            TcpClient tcpClient1 = null, tcpClient2 = null;
            Thread thread = new Thread(() => { tcpClient1 = listener.AcceptTcpClient(); listener.Stop(); });
            thread.Start();
            tcpClient2 = new TcpClient("localhost", 1024);
            thread.Join();
            //Act
            CommunicationStream stream = new CommunicationStream(tcpClient2.GetStream(), true, 50);
            Thread.Sleep(1000);
            stream.Dispose();
            StreamReader reader = new StreamReader(tcpClient1.GetStream());
            var eots = reader.ReadToEnd();
            reader.Close();
            tcpClient1.Close();
            tcpClient2.Close();
            //Assert
            foreach (char c in eots)
                if (c != 23) Assert.Fail();
            Assert.IsTrue(eots.Length > 15 && eots.Length < 25);
        }


    }
}
