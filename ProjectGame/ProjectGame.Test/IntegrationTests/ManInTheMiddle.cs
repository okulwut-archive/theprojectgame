﻿using ProjectGame.Common;
using ProjectGame.Common.Messages;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;

namespace ProjectGame.Test.IntegrationTests
{
    class ManInTheMiddle
    {
        private CommunicationStream s1;
        private CommunicationStream s2;
        private TcpClient c1;
        private TcpClient c2;
        private CancellationTokenSource tokenSource = new CancellationTokenSource();
        Queue<Message> messagesToFirstClient=new Queue<Message>();
        Queue<Message> messagesToSecondClient = new Queue<Message>();
        private Mutex m1 = new Mutex();
        private Mutex m2 = new Mutex();
        public ManInTheMiddle(TcpClient c1, TcpClient c2)
        {
            this.s1 = new CommunicationStream(c1.GetStream());
            this.s2 = new CommunicationStream(c2.GetStream());
            this.c1 = c1;
            this.c2 = c2;
        }
        public void Start()
        {
            s1.ReadMessages(tokenSource.Token, (msg, token) =>
            {
                s2.SendMessage(msg);
                m2.WaitOne();
                messagesToSecondClient.Enqueue(msg);
                m2.ReleaseMutex();
            });
            s2.ReadMessages(tokenSource.Token, (msg, token) =>
            {
                s1.SendMessage(msg);
                m1.WaitOne();
                messagesToFirstClient.Enqueue(msg);
                m1.ReleaseMutex();
            });

        }
        public void Stop()
        {
            s1.Dispose();
            s2.Dispose();
        }
        public bool IsFirstEmpty()
        {
            m1.WaitOne();
            int result = messagesToFirstClient.Count;
            m1.ReleaseMutex();
            return result == 0;
        }
        public bool IsSecondEmpty()
        {
            m2.WaitOne();
            int result = messagesToSecondClient.Count;
            m2.ReleaseMutex();
            return result == 0;
        }
        public int CountFirst()
        {
            m1.WaitOne();
            int result = messagesToFirstClient.Count;
            m1.ReleaseMutex();
            return result;
        }
        public int CountSecond()
        {
            m2.WaitOne();
            int result = messagesToSecondClient.Count;
            m2.ReleaseMutex();
            return result;
        }
        public Message GetMessageToFirst()
        {
            if (IsFirstEmpty())
                return null;
            m1.WaitOne();
            var result = messagesToFirstClient.Dequeue();
            m1.ReleaseMutex();
            return result;
        }
        public Message GetMessageToSecond()
        {
            if (IsSecondEmpty())
                return null;
            m2.WaitOne();
            var result = messagesToSecondClient.Dequeue();
            m2.ReleaseMutex();
            return result;
        }
        public void SendMessageToFirstClient(Message msg)
        {
            s1.SendMessage(msg);
        }
        public void SendMessageToSecondClient(Message msg)
        {
            s2.SendMessage(msg);
        }
        public void ConnectionFailure()
        {
            c1.Close();
            c2.Close();
            s1.Dispose();
            s2.Dispose();
        }

    }
}
