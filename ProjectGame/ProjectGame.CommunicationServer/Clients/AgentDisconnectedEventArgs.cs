﻿namespace ProjectGame.CommunicationServer.Clients
{
    public class AgentDisconnectedEventArgs
    {
        public AgentDisconnectedEventArgs(ulong agentId)
        {
            AgentId = agentId;
        }

        public ulong AgentId { get; }
    }
}