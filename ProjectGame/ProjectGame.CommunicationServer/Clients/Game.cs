﻿using System;
using System.Collections.Generic;
using ProjectGame.Common;
using ProjectGame.Common.Exceptions;
using ProjectGame.Common.Logging;
using ProjectGame.Common.Messages;

namespace ProjectGame.CommunicationServer.Clients
{
    /// <summary>
    ///     Class for games registered on the server.
    /// </summary>
    public class Game
    {
        /// <summary>
        ///     Static fields used for assigning game IDs to new games.
        /// </summary>
        private static ulong _gameCounter;

        /// <summary>
        ///     Number of available slots on the blue team.
        /// </summary>
        private readonly ulong _blueSlots;

        /// <summary>
        ///     List of agents on the blue team.
        /// </summary>
        private readonly Dictionary<ulong, IAgent> _blueTeamPlayers;

        /// <summary>
        ///     Number of available slots on the red team.
        /// </summary>
        private readonly ulong _redSlots;

        /// <summary>
        ///     List of agents on the red team.
        /// </summary>
        private readonly Dictionary<ulong, IAgent> _redTeamPlayers;

        /// <summary>
        ///     Constructor.
        /// </summary>
        /// <param name="gameMaster"><see cref="IAgent" /> instance to be set as the master of the given game.</param>
        /// <param name="newGameInfo"><see cref="GameInfo" /> object containing the information related to the game.</param>
        public Game(IAgent gameMaster, GameInfo newGameInfo)
        {
            Id = _gameCounter;
            _gameCounter += 1;
            GameMaster = gameMaster;
            Name = newGameInfo.gameName;
            _redTeamPlayers = new Dictionary<ulong, IAgent>();
            _blueTeamPlayers = new Dictionary<ulong, IAgent>();
            _blueSlots = newGameInfo.blueTeamPlayers;
            _redSlots = newGameInfo.redTeamPlayers;
        }

        /// <summary>
        ///     Do not use. For testing purposes ONLY.
        /// </summary>
        public Game(IAgent gameMaster, GameInfo newGameInfo, Dictionary<ulong, IAgent> redTeam, Dictionary<ulong, IAgent> blueTeam)
        {
            Id = _gameCounter;
            _gameCounter += 1;
            GameMaster = gameMaster;
            Name = newGameInfo.gameName;
            _redTeamPlayers = redTeam;
            _blueTeamPlayers = blueTeam;
            _blueSlots = newGameInfo.blueTeamPlayers;
            _redSlots = newGameInfo.redTeamPlayers;
        }

        /// <summary>
        ///     The ID number of the game.
        /// </summary>
        virtual public ulong Id { get; }

        /// <summary>
        ///     The name of the game.
        /// </summary>
        virtual public string Name { get; set; }

        /// <summary>
        ///     The <see cref="IAgent" /> serving as the game master for the game.
        /// </summary>
        public IAgent GameMaster { get; }

        /// <summary>
        ///     Broadcasts a <see cref="Message" /> to all of the players in the game.
        /// </summary>
        /// <param name="message"><see cref="Message"/> to broadcast.</param>
        /// <returns>The list of all <see cref="IAgent"/> the broadcast was not delivered to due to disconnection.</returns>
        virtual public List<IAgent> BroadcastMessage(Message message)
        {
            var disconnectedAgents = new List<IAgent>();
            Action<IAgent> sendAction = agent =>
            {
                try
                {
                    agent.SendMessage(message);
                }
                catch (ProtocolException ex)
                {
                    Log.Protocol.Warn(ex.Message);
                    disconnectedAgents.Add(agent);
                }
            };
			foreach (var player in _redTeamPlayers.Values)
			{
				sendAction(player);
			}
			foreach (var player in _blueTeamPlayers.Values)
			{
				sendAction(player);
			}
			return disconnectedAgents;
        }

        /// <summary>
        ///     Adds a player to one of the teams.
        /// </summary>
        /// <param name="player">The agent to add as a player.</param>
        /// <param name="team">The colour of the player's team.</param>
        virtual public void AddPlayer(IAgent player, TeamColour team)
        {
            switch (team)
            {
                case TeamColour.blue:
					if (!_blueTeamPlayers.ContainsKey(player.Id))
					{
						_blueTeamPlayers.Add(player.Id, player);
					}
                    break;
                case TeamColour.red:
					if (!_redTeamPlayers.ContainsKey(player.Id))
					{
						_redTeamPlayers.Add(player.Id, player);
					}
                    break;
            }
        }

        /// <summary>
        ///     Returns a <see cref="GameInfo" /> object with info pertaining to the game.
        /// </summary>
        /// <returns>A <see cref="GameInfo" /> object with the info about the game.</returns>
        virtual public GameInfo GetGameInfo()
        {
            return new GameInfo
            {
                gameName = Name,
                blueTeamPlayers = _blueSlots - (ulong) _blueTeamPlayers.Count,
                redTeamPlayers = _redSlots - (ulong) _redTeamPlayers.Count
            };
        }

		/// <summary>
		/// Checks whether player with specified id is in this game
		/// </summary>
		/// <param name="playerId">Id of player</param>
		/// <returns></returns>
        public bool IsPlayerInGame(ulong playerId)
        {
            return _blueTeamPlayers.ContainsKey(playerId) || 
                   _redTeamPlayers.ContainsKey(playerId);
        }

		/// <summary>
		/// Removes player from this game
		/// </summary>
		/// <param name="playerId">Id of player to be removed</param>
		public void RemovePlayerFromGame(ulong playerId)
		{
			_blueTeamPlayers.Remove(playerId);
			_redTeamPlayers.Remove(playerId);
		}

		/// <summary>
		/// Removes game master of this game
		/// </summary>
		public void RemoveGameMaster()
		{
			GameMaster.Dispose();
		}

        /// <summary>
        ///     Number of players currently in the game.
        /// </summary>
        public int PlayerCount => _blueTeamPlayers.Count + _redTeamPlayers.Count;
    }
}