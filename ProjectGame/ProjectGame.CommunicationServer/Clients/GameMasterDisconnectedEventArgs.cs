﻿namespace ProjectGame.CommunicationServer.Clients
{
    public class GameMasterDisconnectedEventArgs
    {
        public GameMasterDisconnectedEventArgs(ulong gameId)
        {
            GameId = gameId;
        }

        public ulong GameId { get; }
    }
}