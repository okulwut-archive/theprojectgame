﻿using System;
using System.Collections.Concurrent;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using ProjectGame.Common;
using ProjectGame.Common.Exceptions;
using ProjectGame.Common.Logging;
using ProjectGame.Common.Messages;
using Timer = System.Timers.Timer;

namespace ProjectGame.CommunicationServer.Clients
{
    /// <summary>
    ///     Represents a single agent connected to the communication server.
    /// </summary>
    public class Agent : IAgent
    {
        /// <summary>
        ///     The source of a <see cref="CancellationToken" /> for this agent only. This source is cancelled, when the keep-alive
        ///     timeout for this particular agent expires.
        /// </summary>
        private readonly CancellationTokenSource _agentTokenSource;

        /// <summary>
        ///     The underlying TCP client responsible for information exchange.
        /// </summary>
        private readonly TcpClient _client;

        /// <summary>
        ///     Communication stream used to send messages.
        /// </summary>
        private readonly CommunicationStream _communicationStream;

        /// <summary>
        ///     Collection with incoming <see cref="MessageEvent" />s.
        /// </summary>
        private readonly BlockingCollection<MessageEvent> _incomingQueue;

        /// <summary>
        ///     Timer used to keep track of the keep-alive timeout.
        /// </summary>
        private readonly Timer _keepAliveTimer;

        /// <summary>
        ///     This is a linked <see cref="CancellationTokenSource" />. This cancels in two cases:
        ///     <list type="bullet">
        ///         <item>
        ///             <description>when the server as a whole ceases operation (the token passed via constructor is cancelled),</description>
        ///         </item>
        ///         <item>
        ///             <description>the keep-alive timeout has expired for this particular agent.</description>
        ///         </item>
        ///     </list>
        ///     In both cases, we can cease reading from the <see cref="CommunicationStream" /> and therefore mark the agent as
        ///     dead.
        /// </summary>
        private readonly CancellationTokenSource _linkedTokenSource;

        /// <summary>
        ///     Constructor.
        /// </summary>
        /// <param name="client">A <see cref="TcpClient" /> instance received from the accepting thread.</param>
        /// <param name="serverToken"><see cref="CancellationToken" /> used to cancel all server operations.</param>
        /// <param name="incomingQueue">A reference to the incoming message queue.</param>
        public Agent(TcpClient client, CancellationToken serverToken, BlockingCollection<MessageEvent> incomingQueue)
        {
            _client = client;
            _agentTokenSource = new CancellationTokenSource();
            _linkedTokenSource = CancellationTokenSource.CreateLinkedTokenSource(serverToken, _agentTokenSource.Token);
            _incomingQueue = incomingQueue;
            _communicationStream = new CommunicationStream(_client.GetStream());
            _keepAliveTimer = new Timer(CommunicationServerConfiguration.KeepAliveInterval) {AutoReset = false};
            _keepAliveTimer.Elapsed += KeepAliveTimerElapsed;
            Id = GlobalId++;
        }

        /// <summary>
        ///     Do not use. For testing purposes ONLY.
        /// </summary>
        public Agent()
        {
        }

        /// <summary>
        ///     Exposes the task used to read messages with.
        /// </summary>
        private Task ReadingTask { get; set; }

        /// <summary>
        ///     Indicates whether the given agent is alive or not.
        /// </summary>
        public bool Alive => !ReadingTask.IsCompleted;

        /// <summary>
        ///     Sends a <see cref="Message" /> to the given agent.
        /// </summary>
        /// <param name="message"><see cref="Message" /> object to send.</param>
        public virtual void SendMessage(Message message)
        {
            if (!Alive)
            {
                throw new ProtocolException($"Agent with ID {Id} dead; message {message} not sent");
            }
            _communicationStream.SendMessage(message);
        }

        /// <summary>
        ///     ID of the agent.
        /// </summary>
        public ulong Id { get; set; }

        private static ulong GlobalId;

        /// <summary>
        ///     Disposes of resources used by the agent.
        /// </summary>
        public virtual void Dispose()
        {
            ((IDisposable) _client)?.Dispose();
            _communicationStream?.Dispose();
        }

        /// <summary>
        ///     Event that fires when the timer interval expires.
        /// </summary>
        public event EventHandler<AgentDisconnectedEventArgs> TimerExpiredEvent;

        /// <summary>
        ///     Fires when the keep-alive timer has elapsed/
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Arguments of the event.</param>
        private void KeepAliveTimerElapsed(object sender, ElapsedEventArgs e)
        {
            Log.Protocol.Info($"Keep-alive timeout expired for agent with ID {Id}");
            TimerExpiredEvent?.Invoke(this, new AgentDisconnectedEventArgs(Id));
            _agentTokenSource.Cancel();
        }

        /// <summary>
        ///     Starts listening for messages from the given agent.
        /// </summary>
        public void StartReading()
        {
            ReadingTask?.Wait(_linkedTokenSource.Token);
            _keepAliveTimer.Start();
            ReadingTask = _communicationStream.ReadMessages(_linkedTokenSource.Token,
                (message, token) => _incomingQueue.Add(new MessageEvent(message, this), token),
                ResetTimer);
        }

        /// <summary>
        ///     Resets the timer upon receiving a message.
        /// </summary>
        private void ResetTimer()
        {
            _keepAliveTimer.Interval = CommunicationServerConfiguration.KeepAliveInterval;
        }

        /// <summary>
        ///     Removes all subscriptions from the <see cref="TimerExpiredEvent" />. Used for game restarts.
        /// </summary>
        public void ClearTimerEvent()
        {
            TimerExpiredEvent = null;
        }
    }
}