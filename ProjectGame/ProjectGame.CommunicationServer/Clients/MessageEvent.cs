﻿using ProjectGame.Common.Messages;

namespace ProjectGame.CommunicationServer.Clients
{
    public class MessageEvent
    {
        public Message Message { get; }
        public Agent Sender { get; }

        public MessageEvent(Message message, Agent sender)
        {
            Message = message;
            Sender = sender;
        }
    }
}
