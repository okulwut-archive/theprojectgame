﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectGame.Common;
using ProjectGame.Common.Exceptions;
using ProjectGame.Common.Messages;

namespace ProjectGame.CommunicationServer.Clients
{
    /// <summary>
    ///     Class responsible for managing <see cref="Game" /> and <see cref="IAgent" /> instances.
    /// </summary>
    public class GameManager
    {
        /// <summary>
        ///     List of all <see cref="Agent" /> instances that have ever connected to the server.
        /// </summary>
        private readonly List<Agent> _allClients;

        /// <summary>
        ///     Dictionary indexing <see cref="Game" />s by ID.
        /// </summary>
        private readonly Dictionary<ulong, Game> _idGameMap;

        /// <summary>
        ///     Dictionary indexing players by IDs.
        /// </summary>
        private readonly Dictionary<ulong, IAgent> _playerMap;

        /// <summary>
        ///     Dictionary indexing <see cref="Game" />s by names.
        ///     This map contains ONLY the games visible by name - the games for which the Game Master has sent a
        ///     <see cref="RegisterGame" /> message, but not a <see cref="Common.Messages.GameStarted" /> message. Upon receiving
        ///     the latter, as per the specification, the game is no longer visible to any new players (effectively, it is missing
        ///     from the <see cref="RegisteredGames" /> message).
        ///     The <see cref="GetGames" /> method should use this dictionary to generate responses EXCLUSIVELY.
        /// </summary>
        private readonly Dictionary<string, Game> _stringGameMap;

        /// <summary>
        ///     Constructor.
        /// </summary>
        public GameManager()
        {
            _playerMap = new Dictionary<ulong, IAgent>();
            _idGameMap = new Dictionary<ulong, Game>();
            _stringGameMap = new Dictionary<string, Game>();
            _allClients = new List<Agent>();
        }

        /// <summary>
        ///     Do not use. For testing purposes ONLY.
        /// </summary>
        public GameManager(Dictionary<ulong, IAgent> playerMap, Dictionary<ulong, Game> idGameMap,
            Dictionary<string, Game> stringGameMap, List<Agent> clients)
        {
            _playerMap = playerMap;
            _idGameMap = idGameMap;
            _stringGameMap = stringGameMap;
            _allClients = clients;
        }

        /// <summary>
        ///     An event that fires when a game master disconnects from the server.
        /// </summary>
        public event EventHandler<GameMasterDisconnectedEventArgs> GameMasterDisconnected;

        /// <summary>
        ///     An event that fires when a player disconnects from the server.
        /// </summary>
        public event EventHandler<PlayerDisconnectedEventArgs> PlayerDisconnected;

        /// <summary>
        ///     Gets a game for the given ID.
        /// </summary>
        /// <param name="id">The ID for the game to fetch.</param>
        /// <returns>A <see cref="Game" /> with the supplied ID.</returns>
        public virtual Game GetGameById(ulong id)
        {
            if (_idGameMap.ContainsKey(id))
            {
                return _idGameMap[id];
            }
            throw new NotFoundException($"The game with ID {id} could not be found");
        }

        /// <summary>
        ///     Gets a game for the given name.
        /// </summary>
        /// <param name="name">The name of the game to fetch.</param>
        /// <returns>A <see cref="Game" /> with the supplied name.</returns>
        public virtual Game GetGameByName(string name)
        {
            if (_stringGameMap.ContainsKey(name))
            {
                return _stringGameMap[name];
            }
            throw new NotFoundException($"The game with name {name} could not be found");
        }

        /// <summary>
        ///     Gets a player for the given ID.
        /// </summary>
        /// <param name="id">The ID for the player to fetch.</param>
        /// <returns>A <see cref="IAgent" /> instance with the supplied ID.</returns>
        public virtual IAgent GetPlayerById(ulong id)
        {
            if (_playerMap.ContainsKey(id))
            {
                return _playerMap[id];
            }
            throw new NotFoundException($"The player with ID {id} could not be found");
        }

        /// <summary>
        ///     Disposes of all registered <see cref="IAgent" /> instances.
        /// </summary>
        public void Dispose()
        {
            _allClients.ForEach(c => c.Dispose());
        }

        /// <summary>
        ///     Adds a player to the player map and assigns an ID to the player.
        /// </summary>
        /// <param name="player"><see cref="IAgent" /> instance to add to the map.</param>
        /// <returns>The ID assigned to the player.</returns>
        public virtual ulong AddPlayer(IAgent player)
        {
            _playerMap.Add(player.Id, player);
            return player.Id;
        }

        /// <summary>
        ///     Checks whether a game with the given name exists.
        /// </summary>
        /// <param name="name">The game name to check.</param>
        /// <returns>True if the game with the given name exists, false otherwise.</returns>
        public virtual bool GameWithNameExists(string name)
        {
            return _stringGameMap.ContainsKey(name);
        }

        /// <summary>
        ///     Adds a <see cref="Game" /> to the maps.
        /// </summary>
        /// <param name="game">The <see cref="Game" /> instance to add to the maps.</param>
        public virtual void AddGame(Game game)
        {
            EventHandler<AgentDisconnectedEventArgs> handler = null;
            handler = (sender, args) =>
            {
                GameMasterDisconnected?.Invoke(this, new GameMasterDisconnectedEventArgs(game.Id));
                ((Agent) game.GameMaster).TimerExpiredEvent -= handler;
            };
            var agent = game.GameMaster as Agent;
            if (agent != null)
            {
                agent.TimerExpiredEvent += handler;
            }
            _idGameMap.Add(game.Id, game);
            _stringGameMap.Add(game.Name, game);
        }

        /// <summary>
        ///     Adds the supplied player to the supplied game, on the given team.
        /// </summary>
        /// <param name="player">The player instance to add.</param>
        /// <param name="team">The colour of the team to add the player to.</param>
        /// <param name="game">The game to add the player to.</param>
        public virtual void AddPlayerToGame(IAgent player, TeamColour team, Game game)
        {
            EventHandler<AgentDisconnectedEventArgs> handler = null;
            handler = (sender, args) =>
            {
                PlayerDisconnected?.Invoke(this, new PlayerDisconnectedEventArgs(player.Id, game.Id));
                ((Agent) player).TimerExpiredEvent -= handler;
            };
            var agent = player as Agent;
            if (agent != null)
            {
                agent.TimerExpiredEvent += handler;
            }
            game.AddPlayer(player, team);
        }

        /// <summary>
        ///     Registers an agent in the <see cref="_allClients" /> list for later cleanup.
        /// </summary>
        /// <param name="agent">An <see cref="Agent" /> instance to register.</param>
        public void RegisterAgent(Agent agent)
        {
            _allClients.Add(agent);
        }

        /// <summary>
        ///     Returns a <see cref="RegisteredGames" /> message with all currently registered games.
        /// </summary>
        /// <returns>An instance of <see cref="RegisteredGames" />.</returns>
        public RegisteredGames GetGames()
        {
            var gameInfos = _stringGameMap.Values
                .Select(game => game.GetGameInfo())
                .ToArray();
            return new RegisteredGames
            {
                GameInfo = gameInfos
            };
        }

        /// <summary>
        ///     Finds a game that given player belongs to
        /// </summary>
        /// <param name="playerId">Id of a player, whose game is to be found</param>
        /// <returns>Id of a game, that players belongs to</returns>
        public ulong GetPlayersGameId(ulong playerId)
        {
            foreach (var game in _idGameMap.Values)
            {
                if (game.IsPlayerInGame(playerId))
                {
                    return game.Id;
                }
            }
            return 0;
        }

        /// <summary>
        ///     Removes game with given id from list of games
        /// </summary>
        /// <param name="gameId">Id of a game to be removed</param>
        public void RemoveGame(ulong gameId)
        {
            Game game = null;
            if (_idGameMap.ContainsKey(gameId))
            {
                game = _idGameMap[gameId];
                _idGameMap.Remove(gameId);
            }
            if (game != null && _stringGameMap.ContainsKey(game.Name))
            {
                _stringGameMap.Remove(game.Name);
            }
        }

        /// <summary>
        ///     Removes player from list of players, and disposes resources
        /// </summary>
        /// <param name="playerId">Id of player to be removed</param>
        public void RemovePlayer(ulong playerId, Game game)
        {
            _playerMap[playerId].Dispose();
            _playerMap.Remove(playerId);
            game.RemovePlayerFromGame(playerId);
        }

        /// <summary>
        ///     Called when the player with the supplied ID will not receive any more messages from the GM with the supplied ID.
        /// </summary>
        /// <param name="playerId">Player's ID.</param>
        /// <param name="gameMasterId">Game master's ID.</param>
        public void PlayerGameEnded(ulong playerId, ulong gameMasterId)
        {
            var playerGame = _idGameMap.Values
                .FirstOrDefault(game => game.GameMaster.Id == gameMasterId);
            playerGame?.RemovePlayerFromGame(playerId);
            _playerMap.Remove(playerId);
            if (playerGame?.PlayerCount == 0)
            {
                RemoveGame(playerGame.Id);
                var gameMaster = (Agent) playerGame.GameMaster;
                gameMaster.ClearTimerEvent();
            }
        }

        /// <summary>
        ///     Removes game master from game
        /// </summary>
        /// <param name="gameId">Id of game, to remove game master from</param>
        public void RemoveGameMaster(ulong gameId)
        {
            var game = GetGameById(gameId);
            game.RemoveGameMaster();
        }

        /// <summary>
        ///     Removes a game from the string map, therefore making it invisible in the contents of <see cref="RegisteredGames" />
        ///     messages and impossible to join.
        /// </summary>
        /// <param name="gameId">ID of the game which has started.</param>
        public void GameStarted(ulong gameId)
        {
            var game = GetGameById(gameId);
            _stringGameMap.Remove(game.Name);
        }
    }
}