﻿namespace ProjectGame.CommunicationServer.Clients
{
    public class PlayerDisconnectedEventArgs
    {
        public PlayerDisconnectedEventArgs(ulong playerId, ulong gameId)
        {
            PlayerId = playerId;
            GameId = gameId;
        }

        public ulong PlayerId { get; }
        public ulong GameId { get; }
    }
}