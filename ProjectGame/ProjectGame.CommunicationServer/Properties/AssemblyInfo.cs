﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ProjectGame.CommunicationServer")]
[assembly: AssemblyDescription("Communication server for the ProjectGame distributed system.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("J. Czyżniejewski,, B. Dach, D. Dulka, J. Dziwulski, J. Mazurkiewicz")]
[assembly: AssemblyProduct("ProjectGame.CommunicationServer")]
[assembly: AssemblyCopyright("Copyright © J. Czyżniejewski,, B. Dach, D. Dulka, J. Dziwulski, J. Mazurkiewicz 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("931e6c70-447b-44eb-ae26-9d73a420f00b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
