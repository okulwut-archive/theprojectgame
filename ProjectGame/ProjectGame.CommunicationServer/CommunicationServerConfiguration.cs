﻿using ProjectGame.Common.Configurations;
using ProjectGame.Common.Logging;
using System.IO;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ProjectGame.CommunicationServer
{
	public static class CommunicationServerConfiguration
	{
		public static uint KeepAliveInterval { get; set; }

		public static bool ParseSettings(string filePath)
		{
			var validator = new Common.XmlSchemaValidator($"{typeof(Configuration).Namespace}.Configuration.xsd");
			var xdoc = XDocument.Load(filePath);
			try
			{
				validator.Validate(xdoc);
			}
			catch (XmlSchemaValidationException ex)
			{
				Log.Error($"{filePath} is not a valid configuration file. Details:\n${ex.Message}");
				return false;
			}

			var deserializer = new XmlSerializer(typeof(CommunicationServerSettings));
			var reader = new StringReader(xdoc.ToString());
			var configuration = (CommunicationServerSettings) deserializer.Deserialize(reader);

			KeepAliveInterval = configuration.KeepAliveInterval;

			return true;
		}
	}
}
