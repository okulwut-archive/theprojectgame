﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using ProjectGame.Common;
using ProjectGame.Common.Exceptions;
using ProjectGame.Common.Logging;
using ProjectGame.Common.Messages;
using ProjectGame.CommunicationServer.Clients;
using Game = ProjectGame.CommunicationServer.Clients.Game;

namespace ProjectGame.CommunicationServer
{
    /// <summary>
    ///     Main class of the communication server.
    /// </summary>
    public class CommunicationServer : ICommunicationServer
    {
        /// <summary>
        ///     Source of cancellation tokens. Used to cancel server operations.
        /// </summary>
        private readonly CancellationTokenSource _cancellationTokenSource;

        /// <summary>
        ///     A <see cref="GameManager" /> instance used to fetch <see cref="IAgent" /> and <see cref="Clients.Game" /> instances
        ///     by ID and name.
        /// </summary>
        private readonly GameManager _gameManager;

        /// <summary>
        ///     Queue for incoming messages.
        /// </summary>
        private readonly BlockingCollection<MessageEvent> _incomingQueue;

        /// <summary>
        ///     Listener used to establish new <see cref="TcpClient" />s.
        /// </summary>
        private readonly TcpListener _listener;

        /// <summary>
        ///     The <see cref="Task" /> responsible for accepting new clients.
        /// </summary>
        private Task _acceptTask;

        /// <summary>
        ///     Do not use. For testing purposes ONLY.
        /// </summary>
        private BlockingCollection<MessageEvent> debugList=null;

        /// <summary>
        ///     Creates the communication server.
        /// </summary>
        /// <param name="port">The port number, which is to be bound on all interfaces.</param>
        public CommunicationServer(int port)
        {
            var ipAddress = IPAddress.Any;
            var ipEndPoint = new IPEndPoint(ipAddress, port);
            _listener = new TcpListener(ipEndPoint);
            _cancellationTokenSource = new CancellationTokenSource();
            _gameManager = new GameManager();
            _gameManager.PlayerDisconnected += HandleDisconnectedPlayer;
            _gameManager.GameMasterDisconnected += HandleDisconnectedGameMaster;
            _incomingQueue = new BlockingCollection<MessageEvent>();
        }

        /// <summary>
        ///     Do not use. For testing purposes ONLY.
        /// </summary>
        public CommunicationServer(int port, BlockingCollection<MessageEvent> debuglist)
        {
            this.debugList = debuglist;
            var ipAddress = IPAddress.Any;
            var ipEndPoint = new IPEndPoint(ipAddress, port);
            _listener = new TcpListener(ipEndPoint);
            _cancellationTokenSource = new CancellationTokenSource();
            _gameManager = new GameManager();
            _gameManager.PlayerDisconnected += HandleDisconnectedPlayer;
            _gameManager.GameMasterDisconnected += HandleDisconnectedGameMaster;
            _incomingQueue = new BlockingCollection<MessageEvent>();
        }

        /// <summary>
        ///     Do not use. For testing purposes ONLY.
        /// </summary>
        public CommunicationServer(TcpListener listener,
            CancellationTokenSource tokenSource,
            GameManager gameManager,
            BlockingCollection<MessageEvent> incomingQueue)
        {
            _listener = listener;
            _cancellationTokenSource = tokenSource;
            _gameManager = gameManager;
            _incomingQueue = incomingQueue;
        }

        /// <summary>
        ///     Forwards the supplied message to one of the connected Game Masters.
        /// </summary>
        /// <param name="message"><see cref="Message" /> to be forwarded.</param>
        public void ForwardToGameMaster(GameMessage message)
        {
            try
            {
                var game = _gameManager.GetGameById(message.gameId);
                Log.Protocol.Debug($"Forwarding to GM of game {game.Name}");
                game.GameMaster.SendMessage(message);
            }
            catch (NotFoundException ex)
            {
                Log.Protocol.Warn(ex.Message);
            }
            catch (ProtocolException ex)
            {
                Log.Network.Warn(ex.Message);
            }
        }

        /// <summary>
        ///     Forwards the supplied message to one of the connected Players.
        /// </summary>
        /// <param name="message"><see cref="Message" /> to be forwarded.</param>
        public void ForwardToPlayer(PlayerMessage message)
        {
            Agent player;
            try
            {
                player = (Agent) _gameManager.GetPlayerById(message.playerId);
                Log.Protocol.Debug($"Forwarding to player #{message.playerId}");
                player.SendMessage(message);
            }
            catch (NotFoundException ex)
            {
                Log.Protocol.Warn(ex.Message);
            }
            catch (ProtocolException ex)
            {
                Log.Network.Warn(ex.Message);
            }
        }

        /// <summary>
        ///     Broadcasts the message to all connected Players from the given game.
        /// </summary>
        /// <param name="message"><see cref="Message" /> to be broadcast.</param>
        /// <param name="gameId">Identification number of the game to send data to.</param>
        public void BroadcastMessage(Message message, ulong gameId)
        {
            try
            {
                var game = _gameManager.GetGameById(gameId);
                Log.Protocol.Debug($"Broadcasting message {message} to game {game.Name}");
                var disconnectedAgents = game.BroadcastMessage(message);

                foreach (var agent in disconnectedAgents)
                {
                    RemovePlayer(agent.Id, game);
                }
            }
            catch (NotFoundException ex)
            {
                Log.Protocol.Warn(ex.Message);
            }
        }

        /// <summary>
        ///     Registers a game on the server.
        /// </summary>
        /// <param name="message"><see cref="Messages.RegisterGame" /> message containing the game details.</param>
        /// <param name="gameMaster">The <see cref="IAgent" /> to be set as the game master for the game.</param>
        /// <returns></returns>
        public ulong RegisterGame(RegisterGame message, IAgent gameMaster)
        {
            if (_gameManager.GameWithNameExists(message.NewGameInfo.gameName))
            {
                throw new CannotCompleteRequestException(
                    $"A game with name \"{message.NewGameInfo.gameName}\" already exists");
            }
            var game = new Game(gameMaster, message.NewGameInfo);
            Log.Info($"Registered game \"{game.Name}\" with ID {game.Id}");
            _gameManager.AddGame(game);
            return game.Id;
        }

        /// <summary>
        ///     Forwards a <see cref="JoinGame" /> request to a game master.
        /// </summary>
        /// <param name="joinGame">The <see cref="JoinGame" /> request to forward.</param>
        /// <param name="sender">The original request sender.</param>
        public void ForwardJoinGameRequest(JoinGame joinGame, IAgent sender)
        {
            try
            {
                var game = _gameManager.GetGameByName(joinGame.gameName);
                var playerId = _gameManager.AddPlayer(sender);
                joinGame.playerId = playerId;
                joinGame.playerIdSpecified = true;
                game.GameMaster.SendMessage(joinGame);
            }
            catch (NotFoundException ex)
            {
                throw new CannotCompleteRequestException(ex);
            }
            catch (ProtocolException ex)
            {
                Log.Network.Warn(ex.Message);
            }
        }

        /// <summary>
        ///     Adds a player to one of the existing games.
        /// </summary>
        /// <param name="gameId">ID number of the game to join.</param>
        /// <param name="team">Team to add the player to.</param>
        /// <param name="playerId">ID number of the player to add.</param>
        public void AddPlayerToGame(ulong gameId, TeamColour team, ulong playerId)
        {
            try
            {
                var game = _gameManager.GetGameById(gameId);
                var player = _gameManager.GetPlayerById(playerId);
                Log.Info($"Added player with ID {player.Id} to game \"{game.Name}\"");
                _gameManager.AddPlayerToGame(player, team, game);
            }
            catch (NotFoundException ex)
            {
                Log.Warn(ex.Message);
            }
        }

        /// <summary>
        ///     Gets a list of all games registered on the server.
        /// </summary>
        /// <returns>A <see cref="RegisteredGames" /> message to send.</returns>
        public RegisteredGames GetGames()
        {
            return _gameManager.GetGames();
        }

        /// <summary>
        ///     Starts server operations.
        /// </summary>
        public void Start()
        {
            Log.Info("Starting");

            _listener.Start();
            Log.Network.Info($"Listening at {_listener.LocalEndpoint}");

            _acceptTask = AcceptClientsAsync(_cancellationTokenSource.Token);
            try
            {
                HandleMessages(_cancellationTokenSource.Token);
            }
            catch (OperationCanceledException)
            {
                Log.Info("Cancellation requested, stopping execution");
            }
        }

        /// <summary>
        ///     Ends server operations.
        /// </summary>
        public void Stop()
        {
            _cancellationTokenSource.Cancel();
            _acceptTask.Wait();
            _listener.Stop();
            _gameManager.Dispose();
        }

        /// <summary>
        ///     Handles all incoming messages.
        /// </summary>
        /// <param name="token">Cancellation token to check.</param>
        private void HandleMessages(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                var messageEvent = _incomingQueue.Take(token);
                if (debugList != null)
                {
                    debugList.Add(messageEvent);
                }
                Log.Protocol.Debug($"Incoming message: {messageEvent.Message}");
                HandleMessage(messageEvent);
            }
        }

        /// <summary>
        ///     Handles an incoming <see cref="MessageEvent" />.
        /// </summary>
        /// <param name="messageEvent"><see cref="MessageEvent" /> to handle.</param>
        private void HandleMessage(MessageEvent messageEvent)
        {
            try
            {
                messageEvent.Message.Handle(this, messageEvent.Sender);
            }
            catch (NotSupportedException)
            {
                Log.Protocol.Warn($"Unexpected {messageEvent.Message} message received");
            }
        }

        /// <summary>
        ///     Accepts incoming connections asynchronously.
        /// </summary>
        /// <param name="token">Cancellation token to check.</param>
        /// <returns>Task used to accept clients on.</returns>
        private async Task AcceptClientsAsync(CancellationToken token)
        {
            await Task.Yield();

            while (!token.IsCancellationRequested)
            {
                try
                {
                    var delay = Task.Delay(1000, token);
                    var accept = _listener.AcceptTcpClientAsync();
                    await Task.WhenAny(delay, accept);
                    if (!accept.IsCompleted)
                    {
                        continue;
                    }
                    var agent = new Agent(await accept, _cancellationTokenSource.Token, _incomingQueue);
                    _gameManager.RegisterAgent(agent);
                    Log.Protocol.Info("New client accepted");
                    Log.Network.Info("Opened new socket");
                    agent.StartReading();
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            }
        }

        /// <summary>
        ///     Handles a player disconnection event.
        /// </summary>
        /// <param name="sender"><see cref="GameManager" /> instance that reported the disconnection.</param>
        /// <param name="args">Event arguments.</param>
        private void HandleDisconnectedPlayer(object sender, PlayerDisconnectedEventArgs args)
        {
            var gameId = _gameManager.GetPlayersGameId(args.PlayerId);
            try
            {
                var game = _gameManager.GetGameById(gameId);
                RemovePlayer(args.PlayerId, game);
                Log.Protocol.Debug($"Sending to GM of game ${game.Name}");
                game.GameMaster.SendMessage(new PlayerDisconnected(args.PlayerId));
            }
            catch (NotFoundException ex)
            {
                Log.Protocol.Warn(ex.Message);
            }
            catch (ProtocolException ex)
            {
                Log.Network.Warn(ex.Message);
            }
        }

        /// <summary>
        ///     Handles a game master disconnection event.
        /// </summary>
        /// <param name="sender"><see cref="GameManager" /> that reported the disconnection.</param>
        /// <param name="args">Event arguments.</param>
        private void HandleDisconnectedGameMaster(object sender, GameMasterDisconnectedEventArgs args)
        {
            _gameManager.RemoveGameMaster(args.GameId);
            BroadcastMessage(new GameMasterDisconnected(args.GameId), args.GameId);
            _gameManager.RemoveGame(args.GameId);
        }

        /// <summary>
        ///     Removes player from list of active players
        /// </summary>
        /// <param name="playerId">Id of player to be removed</param>
        /// <param name="game">Game that this player belongs to</param>
        private void RemovePlayer(ulong playerId, Game game)
        {
            _gameManager.RemovePlayer(playerId, game);
        }

        /// <summary>
        ///     Used to signal to the server that no more messages will be sent to the player with the supplied ID.
        /// </summary>
        /// <param name="playerId">The ID of the player.</param>
        /// <param name="gameMasterId">The ID of the game master that signaled the end of game.</param>
        public void RemovePlayerFromGame(ulong playerId, ulong gameMasterId)
        {
            _gameManager.PlayerGameEnded(playerId, gameMasterId);
        }

        /// <summary>
        ///     Notifies the communication server that a game has started and does not accept players.
        /// </summary>
        /// <param name="gameId">ID of the game which has started.</param>
        public void GameStarted(ulong gameId)
        {
            Log.Protocol.Info($"Game with ID {gameId} has started; hiding game from new players");
            _gameManager.GameStarted(gameId);
        }
    }
}