﻿using System;
using ProjectGame.Common;
using ProjectGame.Common.Logging;

namespace ProjectGame.CommunicationServer
{
    /// <summary>
    ///     Main class for the communication server.
    /// </summary>
    public class Program
    {
        private static readonly string[] RequiredParameters = { "port", "conf" };
        private static CommunicationServer _communicationsServer;

        /// <summary>
        ///     Entry point method for the communication server.
        /// </summary>
        /// <param name="args">Arguments passed to the program in the command line, as a string array.</param>
        public static void Main(string[] args)
        {
            var parsedArgs = ArgumentParser.Parse(args, RequiredParameters);
            var port = ArgumentParser.GetPort(parsedArgs["port"]);
            _communicationsServer = new CommunicationServer(port);
            Log.Verbose = ArgumentParser.IsVerbose(parsedArgs);

			if(!CommunicationServerConfiguration.ParseSettings(parsedArgs["conf"]))
			{
				return;
			}

            Console.CancelKeyPress += Console_CancelKeyPress;
            _communicationsServer.Start();
        }

        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            _communicationsServer.Stop();
        }
    }
}