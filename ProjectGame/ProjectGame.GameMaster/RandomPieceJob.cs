﻿using Quartz;

namespace ProjectGame.GameMaster
{
    internal class RandomPieceJob : IJob
    {
        /// <summary>
        /// Function that runs when job is triggered
        /// </summary>
        /// <param name="context">Job context with passed parameters</param>
        public void Execute(IJobExecutionContext context)
        {
            var dataMap = context.MergedJobDataMap;
            var game = (Game)dataMap["game"];
            if(!game.GameFinished)
            {
                game.RandomPiece();
            }
        }
    }
}
