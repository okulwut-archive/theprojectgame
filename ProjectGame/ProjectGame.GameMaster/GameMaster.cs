﻿using ProjectGame.Common;
using System;
using ProjectGame.Common.Messages;
using System.Collections.Concurrent;
using Quartz;
using System.Threading;
using Quartz.Impl;
using ProjectGame.Common.Logging;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ProjectGame.GameMaster
{
    public class GameMaster : IGameMaster
    {
        /// <summary>
        /// Agent's unique identifier
        /// </summary>
        public ulong Id { get; set; }

        /// <summary>
		/// The game this GameMaster is responsible for
		/// </summary>
        private Game _game;

        /// <summary>
        /// Helper class for communication
        /// </summary>
        private CommunicationClient _communicationClient;

        /// <summary>
        /// Queue with messages to process
        /// </summary>
        private readonly BlockingCollection<Message> _incomingQueue;

        /// <summary>
        /// Queue with messages ready to send
        /// </summary>
        private readonly BlockingCollection<Message> _outgoingQueue;

        /// <summary>
        /// Collection with <see cref="_incomingQueue"/> and <see cref="_outgoingQueue"/> 
        /// </summary>
        private readonly BlockingCollection<Message>[] _queues;

        /// <summary>
        /// Scheduler to run jobs with delay
        /// </summary>
        private readonly IScheduler _scheduler;

        /// <summary>
        /// Token to interrupt blocking on queues
        /// </summary>
        private readonly CancellationTokenSource _cancellationTokenSource;

        /// <summary>
        /// Address of server to connect to
        /// </summary>
        private readonly string _host;

        /// <summary>
        /// Port that server is listening on
        /// </summary>
        private readonly int _port;

        private readonly Dictionary<ulong, uint> _victories;
        private readonly Dictionary<ulong, uint> _games;
        private ConcurrentQueue<string> logBuffer = new ConcurrentQueue<string>();

        public GameMaster(string host, int port)
        {
            _incomingQueue = new BlockingCollection<Message>();
            _outgoingQueue = new BlockingCollection<Message>();
            _queues = new[] { _incomingQueue, _outgoingQueue };
            _cancellationTokenSource = new CancellationTokenSource();
            _scheduler = StdSchedulerFactory.GetDefaultScheduler();
            _host = host;
            _port = port;
            _victories = new Dictionary<ulong, uint>();
            _games = new Dictionary<ulong, uint>();
        }

        /// <summary>
        ///     Do not use. For testing purposes ONLY.
        /// </summary>
        public GameMaster(
            BlockingCollection<Message> inque,
            BlockingCollection<Message> outque,
            CancellationTokenSource cancelsource,
            CommunicationClient client,
            IScheduler scheduler,
            Game game)
        {
            _incomingQueue = inque;
            _outgoingQueue = outque;
            _queues = new[] { _incomingQueue, _outgoingQueue };
            _cancellationTokenSource = cancelsource;
            _scheduler = scheduler;
            _host = "localhost";
            _port = 8888;
            _communicationClient = client;
            _game = game;
        }

        /// <summary>
        ///     Entry point method for the player.
        /// </summary>
        public void Start()
        {
            //Log.Info("Starting");
            Log.Debug("Starting scheduler");
            _scheduler.Start();

            _communicationClient = new CommunicationClient(_host, _port, _incomingQueue, GameMasterConfiguration.KeepAliveInterval);

            _communicationClient.StartReading();
            Register();

            ReadAndRespond();
            _communicationClient.Dispose();
        }

        /// <summary>
        /// Schedules job to run with delay
        /// </summary>
        /// <param name="message">Message to be passed as parameter to job</param>
        /// <param name="delay">Miliseconds to delay the message by</param>
        public void ScheduleJob(Message message, uint delay)
        {
            var dataMap = new JobDataMap
            {
                new System.Collections.Generic.KeyValuePair<string, object>("message", message),
                new System.Collections.Generic.KeyValuePair<string, object>("queue", _outgoingQueue)
            };
            var job = JobBuilder.Create<SendWithDelayJob>()
                .UsingJobData(dataMap)
                .Build();

            var trigger = TriggerBuilder.Create()
                .StartAt(DateBuilder.FutureDate((int)delay, IntervalUnit.Millisecond))
                .Build();

            Log.Protocol.Debug($"Scheduling delayed response to ${message}");
            _scheduler.ScheduleJob(job, trigger);
        }

        /// <summary>
        /// Sends message to agent specified in message
        /// </summary>
        /// <param name="message">Message to be sent</param>
        public virtual void SendMessage(Message message)
        {
            if (message is PlayerMessage)
            {
                PlayerMessage pm = message as PlayerMessage;
                _game.NoteMessageSent(pm.playerId);
            }
            _communicationClient.SendMessage(message);
        }

        /// <summary>
        /// Function to read, process and send messages
        /// </summary>
        private void ReadAndRespond()
        {
            Console.CancelKeyPress += Console_CancelKeyPress;
            Message message;
            int index;

            while (true)
            {
                try
                {
                    if (_outgoingQueue.TryTake(out message))
                    {
                        SendMessage(message);
                        continue;
                    }

                    index = BlockingCollection<Message>.TakeFromAny(_queues, out message, _cancellationTokenSource.Token);
                    if (_queues[index] == _incomingQueue)
                    {
                        message.Handle(this);
                    }
                    else
                    {
                        SendMessage(message);
                    }
                }
                catch (OperationCanceledException)
                {
                    Log.Info("Cancellation requested, stopping execution");
                    break;
                }
            }
        }

        /// <summary>
        /// Event handler that releases resources when program is cancelled
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event parameters</param>
        private void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            _communicationClient.EndReading();
            _scheduler.Shutdown();
            _cancellationTokenSource.Cancel();
            e.Cancel = true;
        }

        /// <summary>
        /// Registers game master with server
        /// </summary>
        public void Register()
        {
            Log.Game.Info("Registering on communication server...");
            Log.Game.Info(GameMasterConfiguration.GameName);
            var message = new RegisterGame
            {
                NewGameInfo = new GameInfo()
                {
                    blueTeamPlayers = (ulong)GameMasterConfiguration.NumberOfPlayersPerTeam,
                    redTeamPlayers = (ulong)GameMasterConfiguration.NumberOfPlayersPerTeam,
                    gameName = GameMasterConfiguration.GameName
                }
            };
            _communicationClient.SendMessage(message);
        }

        /// <summary>
		/// Creates the game
		/// </summary>
        /// <param name="id">The id of the game</param>
        public void CreateGame(ulong id)
        {
            _game = new Game(id, this);
        }

        /// <summary>
		/// Tries to add the player to the game.
        /// Starts the game if there are enough players.
		/// </summary>
        /// <param name="player">The player to register</param>
        public void AddPlayer(Common.Messages.Player player)
        {
            var res = _game.RegisterPlayer(player);

            SendMessage(res);

            if (res is ConfirmJoiningGame & _game.Full)
            {
                _game.Start();
            }
        }

        public async void RetryRegistration()
        {
            try
            {
                await Task.Delay((int)GameMasterConfiguration.RetryRegisterGameInterval, _cancellationTokenSource.Token);
                Register();
            }
            catch (OperationCanceledException) { }
        }

        /// <summary>
        /// Schedules restart to run with delay.
        /// </summary>
        public void ScheduleRestart(uint delay)
        {
            // todo...?
        }

        /// <summary>
        /// Restarts game.
        /// </summary>
        public virtual void Restart()
        {
            _scheduler.Clear();
            Log.Game.Info($"Restarting with id {_game.Id}");
            Register();
        }

        /// <summary>
        /// Attempts to perform a given player's Discover action.
        /// Returns the Data message to be sent back to the given player.
        /// </summary>
        public Data PlayerDiscover(string playerGuid)
        {
            return _game.PlayerDiscover(playerGuid);
        }

        /// <summary>
        /// Attempts to perform a given player's Move action.
        /// Returns the Data message to be sent back to the given player.
        /// </summary>
        public Data PlayerMove(string playerGuid, MoveType direction)
        {
            return _game.PlayerMove(playerGuid, direction);
        }

        /// <summary>
        /// Attempts to perform a given player's PickUpPiece action.
        /// Returns the Data message to be sent back to the given player.
        /// </summary>
        public Data PlayerPickUpPiece(string playerGuid)
        {
            return _game.PlayerPickUpPiece(playerGuid);
        }

        /// <summary>
        /// Attempts to perform a given player's PlacePiece action.
        /// Returns the Data message to be sent back to the given player.
        /// </summary>
        public Data PlayerPlacePiece(string playerGuid)
        {
            return _game.PlayerPlacePiece(playerGuid);
        }

        /// <summary>
        /// Attempts to perform a given player's TestPiece action.
        /// Returns the Data message to be sent back to the given player.
        /// </summary>
        public Data PlayerTestPiece(string playerGuid)
        {
            return _game.PlayerTestPiece(playerGuid);
        }

        /// <summary>
        /// Generates a response to a given player's AuthorizeKnowledgeExchange message.
        /// Returns the message to be sent next.
        /// </summary>
        public Message PlayerAuthorizeKnowledgeExchange(string playerGuid, ulong targetId)
        {
            return _game.PlayerAuthorizeKnowledgeExchange(playerGuid, targetId);
        }


        public void RemovePlayer(ulong playerId)
        {
            _game.RemovePlayer(playerId);
        }

        public void LogEvent(string name, string guid)
        {
            LogEvent(name, guid, false);
        }
        /// <summary>
        /// Logs a recieved message to the csv log
        /// </summary>
        public void LogEvent(string name, string guid, bool flushLog)
        {
            Player p = _game.GetPlayerByGuid(guid);
            if (p == null)
            {
                return;
            }
            logBuffer.Enqueue($"{name},{DateTime.Now.ToString("s")},{_game.Id},{p.Id},{guid},{p.Team.ToString()},{p.Type.ToString()}");
            try
            {
                if (logBuffer.Count > 100 || flushLog)
                {
                    System.IO.File.AppendAllLines($"log_{_game.Name}.csv", logBuffer);
                    string rubbish;
                    while (logBuffer.TryDequeue(out rubbish)) ;
                }
            }
            catch
            {
#warning An ugly solution without checking why it happened.
            }
        }


        public void LogEvent(string name, ulong id)
        {
            LogEvent(name, _game.GetPlayerById(id).Guid);
        }

        /// <summary>
        /// Notes a game for a player, and displays winrate
        /// </summary>
        public void LogGame(ulong id, bool won)
        {
            if (_games.ContainsKey(id))
            {
                _games[id]++;
            }
            else
            {
                _games[id] = 1;
            }

            if (won)
            {
                if (_victories.ContainsKey(id))
                {
                    _victories[id]++;
                }
                else
                {
                    _victories[id] = 1;
                }
            }
            else
            {
                if (!_victories.ContainsKey(id))
                {
                    _victories[id] = 0;
                }
            }

            Log.Game.Info($"player {id} - {_victories[id]}/{_games[id]}");
        }
    }
}
