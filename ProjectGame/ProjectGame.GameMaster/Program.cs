﻿using ProjectGame.Common;
using ProjectGame.Common.Logging;

namespace ProjectGame.GameMaster
{
    /// <summary>
    ///     Main class for the game master.
    /// </summary>
    public class Program
    {
        /// <summary>
        ///     List of parameters required by the game master.
        /// </summary>
        private static readonly string[] RequiredParameters = {"host", "port", "conf"};

		/// <summary>
		///     Entry point method for the player.
		/// </summary>
		/// <param name="args">Arguments passed to the program in the command line, as a string array.</param>
		public static void Main(string[] args)
        {
            Log.Info("Starting");

            var parsedArgs = ArgumentParser.Parse(args, RequiredParameters);
            var port = ArgumentParser.GetPort(parsedArgs["port"]);
            var gameMaster = new GameMaster(parsedArgs["host"], port);
            //for overriding default game name
            string _gameName = null;
            if (parsedArgs.ContainsKey("game") && !string.IsNullOrWhiteSpace(parsedArgs["game"]))
            {
                _gameName = parsedArgs["game"];
                Log.Info(parsedArgs["game"]);
            }
            if (!GameMasterConfiguration.ParseSettings(parsedArgs["conf"], _gameName))
            {
                return;
            }
            Log.Verbose = ArgumentParser.IsVerbose(parsedArgs);
            gameMaster.Start();
        }
	}
}