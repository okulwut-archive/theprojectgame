﻿using ProjectGame.Common.Messages;
using System.Collections.Generic;

namespace ProjectGame.GameMaster
{
    public class Board
    {
        /// <summary>
        ///     Height of the goal areas of the board.
        /// </summary>
        virtual public uint GoalsHeight { get; }

        /// <summary>
        ///     Height of the task areas of the board.
        /// </summary>
        virtual public uint TasksHeight { get; }

        /// <summary>
        ///     Width of the board.
        /// </summary>
        virtual public uint Width { get; }

        /// <summary>
        ///     Height of the board.
        /// </summary>
        virtual public uint Height { get; }

        /// <summary>
        ///     Left border of the playing field. The minimal x coordinate players can stand on.
        /// </summary>
        virtual public uint Left { get; }

        /// <summary>
        ///     Right border of the playing field. The maximal x coordinate players can stand on plus one.
        /// </summary>
        virtual public uint Right { get; }

        /// <summary>
        ///     Top border of the playing field for different teams. The maximal y coordinate players can stand on plus one.
        /// </summary>
        virtual public Dictionary<TeamColour, uint> TeamTop { get; }

        /// <summary>
        ///     Bottom border of the playing field for different teams. The minimal y coordinate players can stand on.
        /// </summary>
        virtual public Dictionary<TeamColour, uint> TeamBottom { get; }

        /// <summary>
        ///     Top border of the whole play area.
        /// </summary>
        virtual public uint Top { get; }

        /// <summary>
        ///     Bottom border of the whole play area.
        /// </summary>
        virtual public uint Bottom { get; }

        /// <summary>
        ///     Top border of the task area.
        /// </summary>
        virtual public uint TaskTop { get; }

        /// <summary>
        ///     Bottom border of the task area.
        /// </summary>
        virtual public uint TaskBottom { get; }

        /// <summary>
        ///     Fields on the board.
        /// </summary>
        virtual public Field[,] Fields { get; set; }

        /// <summary>
        ///     Goal fields in the goal areas.
        /// </summary>
        virtual public bool[,] GoalFields { get; set; }

        public Board()
        {
            GoalsHeight = (uint)GameMasterConfiguration.GoalAreaLength;
            TasksHeight = (uint)GameMasterConfiguration.TaskAreaLength;
            Width = (uint)GameMasterConfiguration.BoardWidth;
            Height = GoalsHeight * 2 + TasksHeight;

            Left = 0;
            Right = Width;
            TeamTop = new Dictionary<TeamColour, uint>();
            TeamTop.Add(TeamColour.red, Height);
            TeamTop.Add(TeamColour.blue, Height-GoalsHeight);
            TeamBottom = new Dictionary<TeamColour, uint>();
            TeamBottom.Add(TeamColour.red, GoalsHeight);
            TeamBottom.Add(TeamColour.blue, 0);
            Top = Height;
            Bottom = 0;
            TaskTop = Height - GoalsHeight;
            TaskBottom = GoalsHeight;

            Field field;
            Fields = new Field[Top, Right];
            GoalFields = new bool[Top, Right];
            for(uint X = Left; X < Right; X ++)
            {
                for (uint Y = 0; Y < GoalsHeight; Y++)
                {
                    Fields[Bottom + Y, X] = new GoalField()
                    {
                        x = X,
                        y = Bottom + Y,
                        team = TeamColour.blue,
                        type = GoalFieldType.unknown
                    };
                    Fields[Top - 1 - Y, X] = new GoalField()
                    {
                        x = X,
                        y = Top - 1 - Y,
                        team = TeamColour.red,
                        type = GoalFieldType.unknown
                    };
                }
                for (uint Y = 0; Y < TasksHeight; Y++)
                {
                    field = new TaskField();
                    field.x = X;
                    field.y = TaskBottom + Y;
                    Fields[field.y,field.x] = field;
                }
            }
        }

        /// <summary>
        ///     Do not use. For testing purposes ONLY.
        /// </summary>
        public Board(System.Exception e) { }

        /// <summary>
        ///     Converts the given location to that of a team's system.
        /// </summary>
        virtual public Location GetTeamLocation(Location location, TeamColour team)
        {
            if (team == TeamColour.red)
            {
                location.y -= GoalsHeight;
            }
            return location;
        }

        /// <summary>
        ///     Converts the given y coordinate to that of a team's system.
        /// </summary>
        virtual public uint GetTeamY(uint y, TeamColour team)
        {
            if (team == TeamColour.red)
            {
                return y - GoalsHeight;
            }
            return y;
        }

        /// <summary>
        ///     Returns true if a field is in the task area.
        /// </summary>
        virtual public bool IsInTaskArea(Location location)
        {
            return IsInTaskArea(location.x, location.y);
        }
        virtual public bool IsInTaskArea(uint x, uint y)
        {
            return (x >= Left && x < Right && y >= TaskBottom && y < TaskTop);
        }

        virtual public Common.Messages.GameBoard ForMessage()
        {
            return new Common.Messages.GameBoard
            {
                goalsHeight = GoalsHeight,
                tasksHeight = TasksHeight,
                width = Width
            };
        }
    }
}