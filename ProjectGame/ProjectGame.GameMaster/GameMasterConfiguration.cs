﻿using ProjectGame.Common.Configurations;
using ProjectGame.Common.Logging;
using ProjectGame.Common.Messages;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace ProjectGame.GameMaster
{
	public static class GameMasterConfiguration
	{
		public static uint KeepAliveInterval { get; set; }

		public static uint RetryRegisterGameInterval { get; set; }

        public static Common.Configurations.GoalField[] Goals { get; set; }

		public static double ShamProbability { get; set; }

		public static uint PlacingNewPiecesFrequency { get; set; }

		public static uint InitialNumberOfPieces { get; set; }

		public static int BoardWidth { get; set; }

		public static int TaskAreaLength { get; set; }

		public static int GoalAreaLength { get; set; }

		public static int NumberOfPlayersPerTeam { get; set; }

		public static string GameName { get; set; }

		public static Common.Configurations.GoalField[] BlueTeamGoals { get; set; } 

		public static Common.Configurations.GoalField[] RedTeamGoals { get; set; } 

		public static bool ParseSettings(string filePath, string _gamename)
		{
			var validator = new Common.XmlSchemaValidator($"{typeof(Configuration).Namespace}.Configuration.xsd");
			var xdoc = XDocument.Load(filePath);
			try
			{
				validator.Validate(xdoc);
			}
			catch (XmlSchemaValidationException ex)
			{
				Log.Error($"{filePath} is not a valid configuration file. Details:\n${ex.Message}");
				return false;
			}

			var deserializer = new XmlSerializer(typeof(GameMasterSettings));
			var reader = new StringReader(xdoc.ToString());
			var configuration = (GameMasterSettings)deserializer.Deserialize(reader);

			KeepAliveInterval = configuration.KeepAliveInterval;
			RetryRegisterGameInterval = configuration.RetryRegisterGameInterval;

            Goals = configuration.GameDefinition.Goals;
			ShamProbability = configuration.GameDefinition.ShamProbability;
			PlacingNewPiecesFrequency = configuration.GameDefinition.PlacingNewPiecesFrequency;
			InitialNumberOfPieces = configuration.GameDefinition.InitialNumberOfPieces;
			BoardWidth = int.Parse(configuration.GameDefinition.BoardWidth);
			TaskAreaLength = int.Parse(configuration.GameDefinition.TaskAreaLength);
			GoalAreaLength = int.Parse(configuration.GameDefinition.GoalAreaLength);
			NumberOfPlayersPerTeam = int.Parse(configuration.GameDefinition.NumberOfPlayersPerTeam);
			GameName = configuration.GameDefinition.GameName;
            if (_gamename != null)
                GameName = _gamename;
			BlueTeamGoals = configuration.GameDefinition.Goals.Where(x => x.team == Common.Configurations.TeamColour.blue).ToArray();
			RedTeamGoals = configuration.GameDefinition.Goals.Where(x => x.team == Common.Configurations.TeamColour.red).ToArray();

			Move.Delay = configuration.ActionCosts.MoveDelay;
			Discover.Delay = configuration.ActionCosts.DiscoverDelay;
			TestPiece.Delay = configuration.ActionCosts.TestDelay;
			PickUpPiece.Delay = configuration.ActionCosts.PickUpDelay;
			PlacePiece.Delay = configuration.ActionCosts.PlacingDelay;
			AuthorizeKnowledgeExchange.Delay = configuration.ActionCosts.KnowledgeExchangeDelay;

			return true;
		}
	}
}
