﻿using Quartz;
using System.Collections.Concurrent;
using ProjectGame.Common.Messages;

namespace ProjectGame.GameMaster
{
	class SendWithDelayJob : IJob
	{
		/// <summary>
		/// Function that runs when job is triggered
		/// </summary>
		/// <param name="context">Job context with passed parameters</param>
		public void Execute(IJobExecutionContext context)
		{
			JobDataMap dataMap = context.MergedJobDataMap;
			Message message = (Message)dataMap["message"];
			BlockingCollection<Message> queue = (BlockingCollection<Message>)dataMap["queue"];
			queue.Add(message);
		}
	}
}
