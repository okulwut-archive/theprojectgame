﻿using System;
using ProjectGame.Common;
using ProjectGame.Common.Messages;
using ProjectGame.Common.Logging;

namespace ProjectGame.GameMaster
{
    public class Player : IAgent
    {
        /// <summary>
		/// Reference to the game master
		/// </summary>
        private IGameMaster _gameMaster;
        public ulong Id { get; set; }
        public TeamColour Team { get; }
        public PlayerType Type { get; }

        private Guid _guid;
        public string Guid
        {
            get
            {
                return _guid.ToString();
            }
        }
        public Location Location { get; }
        public Piece Piece { get; set; }

        public Player(ulong id, TeamColour team, PlayerType type, Guid guid, IGameMaster gameMaster)
        {
            Id = id;
            Team = team;
            Type = type;
            _guid = guid;
            _gameMaster = gameMaster;
            Location = new Location();
            Piece = null;
        }

        /// <summary>
        ///     Do not use. For testing purposes ONLY.
        /// </summary>
        public Player(Location location, ulong id=0, TeamColour team=TeamColour.blue, PlayerType type=PlayerType.member,
                      Guid guid=new Guid(), IGameMaster gameMaster=null)
        {
            Id = id;
            Team = team;
            Type = type;
            _guid = guid;
            _gameMaster = gameMaster;
            Location = location;
            Piece = null;
        }

        public void UnmarkBoardPosition(Board board)
        {
            board.Fields[Location.y, Location.x].playerIdSpecified = false;
        }

        public void MarkBoardPosition(Board board)
        {
            board.Fields[Location.y, Location.x].playerIdSpecified = true;
            board.Fields[Location.y, Location.x].playerId = Id;
        }

        /// <summary>
        /// Attempts to move the player.
        /// Sets the occupied and boundary to false on a successful move.
        /// Sets occupied to true if the target field was occupied.
        /// Sets boundary to true if the target field was outside the accessible playing area.
        /// </summary>
        public void Move(Game game, MoveType direction, out bool occupied, out bool boundary, out uint targetX, out uint targetY)
        {
            Board board = game.Board;
            occupied = false;
            boundary = false;
            Location target = new Location() { x = Location.x, y = Location.y };
            switch(direction)
            {
                case MoveType.up:
                    target.y++;
                    break;
                case MoveType.right:
                    target.x++;
                    break;
                case MoveType.down:
                    target.y--;
                    break;
                case MoveType.left:
                    target.x--;
                    break;
            }
            targetX = target.x;
            targetY = target.y;
            if(target.x < board.Left || target.x >= board.Right)
            {
                boundary = true;
                return;
            }
            if(target.y < board.TeamBottom[Team] || target.y >= board.TeamTop[Team])
            {
                boundary = true;
                return;
            }
            if(board.Fields[target.y,target.x].playerIdSpecified)
            {
                occupied = true;
                return;
            }
            UnmarkBoardPosition(board);
            Location.x = target.x;
            Location.y = target.y;
            MarkBoardPosition(board);
        }

        /// <summary>
        /// Attempts to pick up a piece. Returns true on success.
        /// </summary>
        public bool PickUpPiece(Game game)
        {
            var board = game.Board;
            var field = board.Fields[Location.y, Location.x] as TaskField;
            if (field == null)
            {
                Log.Game.Warn($"Player {Id} attempted to pick up a piece from a goal field");
                return false;
            }
            if (!field.pieceIdSpecified)
            {
                Log.Game.Warn($"Player {Id} attempted to pick up a piece, but no piece was found");
                return false;
            }
			if (Piece != null)
			{
				Log.Game.Warn($"Player {Id} attempted to pick up a piece, while holding another one");
				return true;
			}
            Piece = game.Pieces[field.pieceId];
            Piece.playerIdSpecified = true;
            Piece.playerId = Id;
            field.pieceIdSpecified = false;
            game.UpdateNeighborPiece();
            return true;
        }

        /// <summary>
        /// Attempts to place a piece.
        /// </summary>
        public void PlacePiece(Game game)
        {
            Board board = game.Board;
            TaskField taskField = board.Fields[Location.y, Location.x] as TaskField;
            if (taskField != null)
            {
				if (!taskField.pieceIdSpecified)
				{
					Log.Game.Info($"Player {Id} placed a piece on a task field with coords ({Location.x}, {Location.y})");
					taskField.pieceIdSpecified = true;
					taskField.pieceId = Piece.id;
					Piece.playerIdSpecified = false;
					Piece = null;
				}
				else
				{
					Log.Game.Warn($"Player {Id} attempted to place a piece with id {Piece.id} on top of piece with id {taskField.pieceId}");
				}
                return;
            }
            GoalField goalField = board.Fields[Location.y, Location.x] as GoalField;
            if (goalField == null)
            {
                throw new Exception($"Player {Id} standing on an unidentified field");
            }
            Log.Game.Info($"Player {Id} placed a {Piece.type} piece on a goal field with coords ({Location.x}, {Location.y})");
            if (Piece.type == PieceType.sham)
            {
                game.RemovePiece(Piece.id);
                Piece = null;
                return;
            }
            game.RemovePiece(Piece.id);
            Piece = null;
            if (board.GoalFields[Location.y, Location.x])
            {
                goalField.type = GoalFieldType.goal;
                game.GoalCheck(Team);
            }
            else
            {
                goalField.type = GoalFieldType.nongoal;
            }
			game.AddDiscoveredGoal(Id, Location);
        }

        /// <summary>
        /// Attempts to test a piece. Returns true on success.
        /// </summary>
        public bool TestPiece(Game game)
        {
			return Piece.type != PieceType.sham;
        }

        public void SendMessage(Message message)
        {
            if (!(message is PlayerMessage))
            {
                Log.Protocol.Error($"Cannot send {message} to a player.");
                throw new NotSupportedException();
            }

            PlayerMessage playerMessage = (PlayerMessage)message;
            playerMessage.playerId = Id;
            _gameMaster.SendMessage(playerMessage);
        }

        public Common.Messages.Player ForMessage()
        {
            return new Common.Messages.Player
            {
                id = Id,
                team = Team,
                type = Type
            };
        }

		public void Dispose() { }
	}
}
