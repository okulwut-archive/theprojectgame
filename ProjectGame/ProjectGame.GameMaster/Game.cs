﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectGame.Common.Logging;
using ProjectGame.Common.Messages;
using Quartz;
using Quartz.Impl;
using System.Threading.Tasks;

namespace ProjectGame.GameMaster
{
    public class Game
    {
        /// <summary>
        ///     The board, on which the game is played
        /// </summary>
        public virtual Board Board { get; }

        public string Name { get; }

        public ulong Id { get; }

        public bool GameStarted { get; set; }
        public bool GameFinished { get; set; }

        /// <summary>
        ///     The players of each team.
        /// </summary>
        private readonly Dictionary<TeamColour, List<Player>> _players;

        private readonly Dictionary<ulong, TimeSpan> _responseTimes;
        private readonly Dictionary<ulong, DateTime> _lastMessageTimestamp;
        private readonly Dictionary<ulong, uint> _moveCount;

        /// <summary>
        ///     Information whether or not players have asked for a knowledge exchange.
        /// </summary>
        private Dictionary<ulong, ulong> _permissions;

        /// <summary>
        ///     The maximum allowed number of players in each team.
        /// </summary>
        private readonly Dictionary<TeamColour, int> _maxCount;

        /// <summary>
        ///     Signify the presence of leaders in the teams.
        /// </summary>
        private readonly Dictionary<TeamColour, bool> _leaderPresent;

        /// <summary>
        ///		Contains sets of goals discovered by each player
        /// </summary>
        private readonly Dictionary<ulong, HashSet<Location>> _discoveredGoals;

        /// <summary>
        ///		Contains sets of pieces tested by each player
        /// </summary>
        private readonly Dictionary<ulong, HashSet<ulong>> _testedPieces;

        /// <summary>
        ///     Pieces on the board.
        /// </summary>
        public Dictionary<ulong, Piece> Pieces;
        public ulong NextPieceId;

        /// <summary>
        ///     Game master reference.
        /// </summary>
        private readonly GameMaster _gameMaster;

        /// <summary>
        ///     Scheduler for automatic piece placing.
        /// </summary>
        private readonly IScheduler _scheduler;

        /// <summary>
        ///     Delay between pieces being randomly placed.
        /// </summary>
        private readonly int _pieceDelay;

        private readonly Random _rng = new Random();

        public Game(ulong id, GameMaster gameMaster)
        {
            Id = id;

            Board = new Board();
            GameStarted = false;
            GameFinished = false;

            _players = new Dictionary<TeamColour, List<Player>>();
            _permissions = new Dictionary<ulong, ulong>();
            _maxCount = new Dictionary<TeamColour, int>();
            _leaderPresent = new Dictionary<TeamColour, bool>();
            Pieces = new Dictionary<ulong, Piece>();
            NextPieceId = 1;
            _pieceDelay = (int)GameMasterConfiguration.PlacingNewPiecesFrequency;

            Name = GameMasterConfiguration.GameName;
            _maxCount[TeamColour.blue] = GameMasterConfiguration.NumberOfPlayersPerTeam;
            _maxCount[TeamColour.red] = GameMasterConfiguration.NumberOfPlayersPerTeam;

            _gameMaster = gameMaster;
            _scheduler = StdSchedulerFactory.GetDefaultScheduler();
            _players[TeamColour.blue] = new List<Player>();
            _players[TeamColour.red] = new List<Player>();
            _leaderPresent[TeamColour.red] = false;
            _leaderPresent[TeamColour.blue] = false;

            _discoveredGoals = new Dictionary<ulong, HashSet<Location>>();
            _testedPieces = new Dictionary<ulong, HashSet<ulong>>();

            // placing initial pieces
            for (int i = 0; i < GameMasterConfiguration.InitialNumberOfPieces; i++)
            {
                RandomPiece();
            }

            _responseTimes = new Dictionary<ulong, TimeSpan>();
            _lastMessageTimestamp = new Dictionary<ulong, DateTime>();
            _moveCount = new Dictionary<ulong, uint>();
        }

        /// <summary>
        ///     Do not use. For testing purposes ONLY.
        /// </summary>
        public Game(Exception e)
        {
            Id = 0;

            Board = new Board();
            GameStarted = true;
            GameFinished = false;

            _players = new Dictionary<TeamColour, List<Player>>();
            _permissions = new Dictionary<ulong, ulong>();
            _maxCount = new Dictionary<TeamColour, int>();
            _leaderPresent = new Dictionary<TeamColour, bool>();
            Pieces = new Dictionary<ulong, Piece>();
            NextPieceId = 1;
            _pieceDelay = (int)GameMasterConfiguration.PlacingNewPiecesFrequency;

            Name = GameMasterConfiguration.GameName;
            _maxCount[TeamColour.blue] = GameMasterConfiguration.NumberOfPlayersPerTeam;
            _maxCount[TeamColour.red] = GameMasterConfiguration.NumberOfPlayersPerTeam;

            _gameMaster = null;
            _scheduler = null;
            _players[TeamColour.blue] = new List<Player>();
            _players[TeamColour.red] = new List<Player>();
            _leaderPresent[TeamColour.red] = false;
            _leaderPresent[TeamColour.blue] = false;

            _discoveredGoals = new Dictionary<ulong, HashSet<Location>>();
            _testedPieces = new Dictionary<ulong, HashSet<ulong>>();

            // placing initial pieces
            for (int i = 0; i < GameMasterConfiguration.InitialNumberOfPieces; i++)
            {
                RandomPiece();
            }

            _responseTimes = new Dictionary<ulong, TimeSpan>();
            _lastMessageTimestamp = new Dictionary<ulong, DateTime>();
            _moveCount = new Dictionary<ulong, uint>();

            for (int i = 0; i < 200000; i++)
            {
                _responseTimes[(ulong)i] = new TimeSpan();
                _lastMessageTimestamp[(ulong)i] = new DateTime();
                _moveCount[(ulong)i] = 0;
            }

        }

        /// <summary>
        ///     Do not use. For testing purposes ONLY.
        /// </summary>
        public Game(Dictionary<TeamColour, List<Player>> players = null,
                    Dictionary<TeamColour, int> maxCount = null, Dictionary<TeamColour, bool> leaderPresent = null,
                    GameMaster gameMaster = null, IScheduler scheduler = null, int pieceDelay = 0, Board board = null, Dictionary<ulong, HashSet<Location>> discoveredGoals = null, Dictionary<ulong, HashSet<ulong>> testedPieces = null)
        {
            _players = players;
            _permissions = new Dictionary<ulong, ulong>();
            _maxCount = maxCount;
            _leaderPresent = leaderPresent;
            _gameMaster = gameMaster;
            _scheduler = scheduler;
            _pieceDelay = pieceDelay;
            Board = board;
            Pieces = new Dictionary<ulong, Piece>();
            GameStarted = true;

            _discoveredGoals = discoveredGoals == null ? new Dictionary<ulong, HashSet<Location>>() : discoveredGoals;
            _testedPieces = testedPieces == null ? new Dictionary<ulong, HashSet<ulong>>() : testedPieces;

            _responseTimes = new Dictionary<ulong, TimeSpan>();
            _lastMessageTimestamp = new Dictionary<ulong, DateTime>();
            _moveCount = new Dictionary<ulong, uint>();

            for (int i = 0; i < 200000; i++)
            {
                _responseTimes[(ulong)i] = new TimeSpan();
                _lastMessageTimestamp[(ulong)i] = new DateTime();
                _moveCount[(ulong)i] = 0;
            }
        }

        /// <summary>
        ///     Assigns team and type to player.
        /// </summary>
        /// <param name="team">Before calling - player's preferred team. After - player's assigned team.</param>
        /// <param name="type">Before calling - player's preferred type. After - player's assigned type.</param>
        private void AssignPlayer(ref TeamColour team, ref PlayerType type)
        {
            if (_players[team].Count >= _maxCount[team])
            {
                team = team.Other();
            }

            if (type == PlayerType.leader && _leaderPresent[team])
            {
                type = PlayerType.member;
            }
            // if the player wants to be a member, but there is only one place left and no leader, the player has to become a leader
            else if (type == PlayerType.member
                && !_leaderPresent[team]
                && _players[team].Count >= _maxCount[team] - 1)
            {
                type = PlayerType.leader;
            }

            if (type == PlayerType.leader)
            {
                _leaderPresent[team] = true;
            }
        }

        /// <summary>
        ///     Attempts to add player to game.
        /// </summary>
        /// <param name="player">Description of the player to add</param>
        /// <returns>True if player was registered, false otherwise</returns>
        public Message RegisterPlayer(Common.Messages.Player player)
        {
            if (Full)
            {
                return new RejectJoiningGame
                {
                    gameName = Name,
                    playerId = player.id
                };
            }


            Log.Game.Info($"Player with ID {player.id} requested to join");

            var col = player.team;
            var typ = player.type;
            AssignPlayer(ref col, ref typ);

            Log.Game.Info($"Assigned player with ID {player.id} to team {col} as {typ}");
            var p = new Player(player.id, col, typ, Guid.NewGuid(), _gameMaster);

            var message = new ConfirmJoiningGame
            {
                gameId = Id,
                playerId = p.Id,
                privateGuid = p.Guid,
                PlayerDefinition = new Common.Messages.Player
                {
                    id = p.Id,
                    team = p.Team,
                    type = p.Type
                }
            };

            _players[p.Team].Add(p);

            _discoveredGoals.Add(p.Id, new HashSet<Location>());
            _testedPieces.Add(p.Id, new HashSet<ulong>());

            _responseTimes[p.Id] = TimeSpan.Zero;
            _lastMessageTimestamp[p.Id] = DateTime.Now;
            _moveCount[p.Id] = 0;

            return message;
        }

        /// <summary>
        ///     True if the game is full, false otherwise
        /// </summary>
        public bool Full => Enum.GetValues(typeof(TeamColour)).Cast<TeamColour>().All(c => _players[c].Count >= _maxCount[c]);

        /// <summary>
        ///     Begins the game.
        /// </summary>
        public void Start()
        {
            Log.Game.Info("Starting game...");
            SendGameStarted();
            var playersInfo = new List<Common.Messages.Player>();
            foreach (TeamColour c in Enum.GetValues(typeof(TeamColour)))
            {
                playersInfo.AddRange(_players[c].ConvertAll(p => p.ForMessage()));
            }

            // placing initial pieces
            for (int i = 0; i < GameMasterConfiguration.InitialNumberOfPieces; i++)
            {
                RandomPiece();
            }
            // placing goal areas from the configuration
            foreach (Common.Configurations.GoalField source in GameMasterConfiguration.Goals)
            {
                var x = source.x;
                var y = source.y;
                if (x < Board.Left || y >= Board.Top || x >= Board.Right || y < Board.Bottom)
                {
                    Log.Game.Warn($"Invalid goal field coordinations ({x}, {y}) specified in configuration file");
                }
                Board.GoalFields[y, x] = source.type == Common.Configurations.GoalFieldType.goal;
            }

            var redLocation = new Location
            {
                x = Board.Right - 1,
                y = Board.Top - 1
            };
            var blueLocation = new Location
            {
                x = Board.Left,
                y = Board.Bottom
            };
            Log.Protocol.Info("Broadcasting game start");
            foreach (var player in _players[TeamColour.red])
            {
                player.Location.x = redLocation.x;
                player.Location.y = redLocation.y;
                redLocation.x--;
                if (redLocation.x < Board.Left)
                {
                    redLocation.x = Board.Right - 1;
                    redLocation.y--;
                }

                player.SendMessage(new Common.Messages.Game
                {
                    playerId = player.Id,
                    Board = Board.ForMessage(),
                    PlayerLocation = Board.GetTeamLocation(player.Location, TeamColour.red),
                    Players = playersInfo.ToArray()
                });
            }
            foreach (var player in _players[TeamColour.blue])
            {
                player.Location.x = blueLocation.x;
                player.Location.y = blueLocation.y;
                blueLocation.x++;
                if (blueLocation.x >= Board.Right)
                {
                    blueLocation.x = Board.Left;
                    blueLocation.y++;
                }

                player.SendMessage(new Common.Messages.Game
                {
                    playerId = player.Id,
                    Board = Board.ForMessage(),
                    PlayerLocation = Board.GetTeamLocation(player.Location, TeamColour.blue),
                    Players = playersInfo.ToArray()
                });
            }

            ScheduleRandomPiece();
            GameStarted = true;
        }

        private void SendGameStarted()
        {
            var message = new GameStarted { gameId = Id };
            _gameMaster.SendMessage(message);
        }

        /// <summary>
        ///     Returns the player object with given playerGuid.
        /// </summary>
        public Player GetPlayerByGuid(string playerGuid)
        {
            var candidates = _players.Values.Select(team => team.FirstOrDefault(player => player.Guid == playerGuid))
                .Where(player => player != null);
            return candidates.SingleOrDefault();
        }

        /// <summary>
        ///     Returns the player object with given playerId.
        /// </summary>
        public Player GetPlayerById(ulong playerId)
        {
            var candidates = _players.Values.Select(team => team.FirstOrDefault(player => player.Id == playerId))
                .Where(player => player != null);
            return candidates.SingleOrDefault();
        }

        /// <summary>
        ///     Schedules a job that will add a piece in a random location.
        /// </summary>
        public void ScheduleRandomPiece()
        {
            var dataMap = new JobDataMap { new KeyValuePair<string, object>("game", this) };
            var job = JobBuilder.Create<RandomPieceJob>()
                .UsingJobData(dataMap)
                .Build();

            var trigger = TriggerBuilder.Create()
                .StartAt(DateBuilder.FutureDate(_pieceDelay, IntervalUnit.Millisecond))
                .WithSimpleSchedule(schedule => schedule.WithInterval(TimeSpan.FromMilliseconds(_pieceDelay))
                    .RepeatForever())
                .Build();

            Log.Protocol.Debug("Scheduling piece placing");
            _scheduler.ScheduleJob(job, trigger);
        }

        /// <summary>
        ///     Adds a piece in a random location.
        /// </summary>
        /// <returns>True on success.</returns>
        public bool RandomPiece()
        {
            // assemble a list of available locations
            var fields = new List<TaskField>();
            for (var x = Board.Left; x < Board.Right; x++)
            {
                for (var y = Board.TaskBottom; y < Board.TaskTop; y++)
                {
                    var field = Board.Fields[y, x] as TaskField;
                    if (field != null)
                    {
                        fields.Add(field);
                    }
                }
            }
            if (fields.Count == 0)
                return false;

            AddPiece(fields[_rng.Next() % fields.Count]);
            return true;
        }

        /// <summary>
        ///     Adds a piece in a specified location.
        /// </summary>
        public void AddPiece(TaskField field)
        {
            var piece = new Piece { id = NextPieceId };
            NextPieceId++;
            var r = new Random();

            piece.type = r.NextDouble() < GameMasterConfiguration.ShamProbability ? PieceType.sham : PieceType.normal;
            Log.Game.Info($"Placing a {piece.type} piece with ID {piece.id} on coords ({field.x}, {field.y})");
            if (field.pieceIdSpecified)
            {
                Log.Game.Info($"Piece with ID {field.pieceId} was destroyed");
                field.pieceIdSpecified = false;
                Pieces.Remove(field.pieceId);
            }
            field.pieceId = piece.id;
            field.pieceIdSpecified = true;
            Pieces.Add(piece.id, piece);
            UpdateNeighborPiece();
        }

        /// <summary>
        ///     Removes a piece.
        /// </summary>
        public virtual void RemovePiece(ulong id)
        {
            Pieces.Remove(id);
            UpdateNeighborPiece();
        }

        private class SearchLocation
        {
            public uint X { get; }
            public uint Y { get; }
            public int Level { get; }
            public SearchLocation(uint x, uint y, int level)
            {
                X = x;
                Y = y;
                Level = level;
            }
        }
        /// <summary>
        ///     Updates closest piece data of the task fields.
        /// </summary>
        public virtual void UpdateNeighborPiece()
        {
            var pieceStack = new Stack<Location>();
            var searchQueue = new Queue<SearchLocation>();
            for (var x = Board.Left; x < Board.Right; x++)
            {
                for (var y = Board.TaskBottom; y < Board.TaskTop; y++)
                {
                    var field = Board.Fields[y, x] as TaskField;
                    if (field != null)
                    {
                        if (field.pieceIdSpecified)
                        {
                            field.distanceToPiece = 0;
                            pieceStack.Push(new Location { x = x, y = y });
                        }
                        else
                        {
                            field.distanceToPiece = -1;
                        }
                    }
                }
            }
            while (pieceStack.Count > 0)
            {
                var piece = pieceStack.Pop();
                searchQueue.Enqueue(new SearchLocation(piece.x + 1, piece.y, 1));
                searchQueue.Enqueue(new SearchLocation(piece.x - 1, piece.y, 1));
                searchQueue.Enqueue(new SearchLocation(piece.x, piece.y - 1, 1));
                searchQueue.Enqueue(new SearchLocation(piece.x, piece.y + 1, 1));
                while (searchQueue.Count > 0)
                {
                    var search = searchQueue.Dequeue();
                    if (!Board.IsInTaskArea(search.X, search.Y))
                        continue;
                    var field = (TaskField)Board.Fields[search.Y, search.X];
                    if (field.distanceToPiece <= search.Level && field.distanceToPiece >= 0)
                        continue;
                    field.distanceToPiece = search.Level;
                    searchQueue.Enqueue(new SearchLocation(search.X + 1, search.Y, search.Level + 1));
                    searchQueue.Enqueue(new SearchLocation(search.X - 1, search.Y, search.Level + 1));
                    searchQueue.Enqueue(new SearchLocation(search.X, search.Y + 1, search.Level + 1));
                    searchQueue.Enqueue(new SearchLocation(search.X, search.Y - 1, search.Level + 1));
                }
            }
        }

        /// <summary>
        ///     Checks if a team has filled in all of their goals.
        /// </summary>
        public void GoalCheck(TeamColour team)
        {
            uint y1, y2;
            if (team == TeamColour.red)
            {
                y1 = Board.Top - Board.GoalsHeight;
                y2 = Board.Top;
            }
            else
            {
                y1 = Board.Bottom;
                y2 = Board.Bottom + Board.GoalsHeight;
            }
            for (var x = Board.Left; x < Board.Right; x++)
            {
                for (var y = y1; y < y2; y++)
                {
                    var field = (GoalField)Board.Fields[y, x];
                    if (field.type == GoalFieldType.unknown && Board.GoalFields[y, x])
                    {
                        return;
                    }
                }
            }
            if (GameFinished)
            {
                return;
            }
            Log.Game.Info("=== GAME FINISHED ===");
            Log.Game.Info("");
            Log.Game.Info($"{team} team has won the game");
            Log.Game.Info("");
            Log.Game.Info("=== AVERAGE RESPONSE TIMES ==");

            foreach (var list in _players.Values)
            {
                foreach (var player in list)
                {
                    ulong id = player.Id;
                    if (_moveCount[id] == 0)
                    {
                        Log.Game.Info($"player {id} - No moves to have an average. Sad!");
                    }
                    else
                    {
                        TimeSpan average = TimeSpan.FromTicks(_responseTimes[id].Ticks / _moveCount[id]);
                        Log.Game.Info($"player {id} - {average} s");
                    }
                }
            }
            Log.Game.Info("");
            Log.Game.Info("=== WINRATES ===");
            foreach (var list in _players.Values)
            {
                foreach (var player in list)
                {
                    _gameMaster.LogGame(player.Id, team == player.Team);
                    _gameMaster.LogEvent(team == player.Team ? "Victory" : "Defeat", player.Guid,
                        list == _players.Values.Last() && player == list.Last());
                }
            }
            Log.Game.Info("");
            _scheduler.Clear();
            SendGameFinished();
            GameFinished = true;
            _gameMaster.Restart();
        }

        /// <summary>
        ///     Sends ending messages to all players.
        /// </summary>
        public void SendGameFinished()
        {
            var data = new Data
            {
                gameFinished = true,
                GoalFields = Board.Fields.OfType<GoalField>().ToArray(),
                TaskFields = Board.Fields.OfType<TaskField>().ToArray(),
                Pieces = Pieces.Values.ToArray()
            };
            Data.TimestampMessage(data);
            foreach (var list in _players.Values)
            {
                foreach (var player in list)
                {
                    data.playerId = player.Id;
                    data.PlayerLocation = player.Location;
                    _gameMaster.SendMessage(data);
                }
            }
        }

        /// <summary>
        ///     Performs a given player's Discover action.
        /// </summary>
        public Data PlayerDiscover(string playerGuid)
        {
            Log.Game.Debug($"GUID {playerGuid} requested a discovery action");
            Player player;
            if (!CanPerformMove(playerGuid, out player)) return null;

            NoteResponseReceived(player.Id);

            uint x1, x2, y1, y2;
            // coordinates to send within the global board
            if (player.Location.x > Board.Left)
                x1 = player.Location.x - 1;
            else
                x1 = Board.Left;
            if (player.Location.x < Board.Right - 1)
                x2 = player.Location.x + 1;
            else
                x2 = Board.Right - 1;
            if (player.Location.y > Board.TeamBottom[player.Team])
                y1 = player.Location.y - 1;
            else
                y1 = Board.TeamBottom[player.Team];
            if (player.Location.y < Board.TeamTop[player.Team] - 1)
                y2 = player.Location.y + 1;
            else
                y2 = Board.TeamTop[player.Team] - 1;

            var data = new Data
            {
                playerId = player.Id,
                gameFinished = false
            };

            var taskFields = new List<TaskField>();
            var goalFields = new List<GoalField>();
            var pieces = new List<Piece>();
            var playerPieces = _testedPieces.ContainsKey(player.Id) ? _testedPieces[player.Id] : null;
            var playerGoalFields = _discoveredGoals.ContainsKey(player.Id) ? _discoveredGoals[player.Id] : null;

            for (var x = x1; x <= x2; x++)
            {
                for (var y = y1; y <= y2; y++)
                {
                    var field = Board.Fields[y, x] as TaskField;
                    var fieldLocation = new Location() { x = x, y = y };
                    if (field != null)
                    {
                        taskFields.Add((TaskField)Board.Fields[y, x]);
                        if (field.pieceIdSpecified)
                        {
                            var basePiece = Pieces[field.pieceId];
                            var piece = new Piece
                            {
                                id = basePiece.id,
                                playerId = basePiece.playerId,
                                playerIdSpecified = basePiece.playerIdSpecified,
                                type = playerPieces?.Contains(basePiece.id) == true ? basePiece.type : PieceType.unknown
                            };
                            pieces.Add(piece);
                        }
                    }
                    var gField = Board.Fields[y, x] as GoalField;
                    if (gField != null)
                    {
                        var goalField = new GoalField(gField, playerGoalFields?.Contains(fieldLocation) == true ? gField.type : GoalFieldType.unknown);
                        goalFields.Add(goalField);
                    }
                }
            }
            data.TaskFields = taskFields.ToArray();
            data.GoalFields = goalFields.ToArray();
            data.Pieces = pieces.ToArray();

            return data;
        }

        private bool CanPerformMove(string playerGuid, out Player player)
        {
            if (!GameStarted || GameFinished)
            {
                player = null;
            }
            else
            {
                player = GetPlayerByGuid(playerGuid);
            }
            return player != null;
        }

        /// <summary>
        ///     Performs a given player's Move action.
        /// </summary>
        public Data PlayerMove(string playerGuid, MoveType direction)
        {
            Log.Game.Debug($"GUID {playerGuid} requested to move {direction}");
            Player player;
            if (!CanPerformMove(playerGuid, out player)) return null;

            NoteResponseReceived(player.Id);

            var data = new Data
            {
                playerId = player.Id,
                gameFinished = false
            };

            bool occupied, boundary;
            uint targetX, targetY;
            player.Move(this, direction, out occupied, out boundary, out targetX, out targetY);

            if (boundary)
            {
                Log.Game.Info($"Player {player.Id} hit the board boundary while moving {direction}");
                data.PlayerLocation = player.Location;
                return data;
            }
            Log.Game.Info($"Player {player.Id} moved {direction}");

            var field = Board.Fields[targetY, targetX];
            var tField = field as TaskField;
            var playerPieces = _testedPieces.ContainsKey(player.Id) ? _testedPieces[player.Id] : null;
            var playerGoalFields = _discoveredGoals.ContainsKey(player.Id) ? _discoveredGoals[player.Id] : null;
            var fieldLocation = new Location() { x = targetX, y = targetY };

            if (tField != null)
            {
                data.TaskFields = new TaskField[1];
                data.TaskFields[0] = tField;
                if (tField.pieceIdSpecified)
                {
                    var basePiece = Pieces[tField.pieceId];
                    var piece = new Piece
                    {
                        id = basePiece.id,
                        playerId = basePiece.playerId,
                        playerIdSpecified = basePiece.playerIdSpecified,
                        type = playerPieces?.Contains(basePiece.id) == true ? basePiece.type : PieceType.unknown
                    };
                    data.Pieces = new[] { piece };
                }
            }
            var gField = field as GoalField;
            if (gField != null)
            {
                var goalField = new GoalField(gField, playerGoalFields?.Contains(fieldLocation) == true ? gField.type : GoalFieldType.unknown);
                data.GoalFields = new[] { goalField };
            }
            data.PlayerLocation = player.Location;
            return data;
        }

        /// <summary>
        ///     Performs a given player's PickUpPiece action.
        /// </summary>
        public Data PlayerPickUpPiece(string playerGuid)
        {
            Log.Game.Debug($"GUID {playerGuid} requested to pick up a piece");
            Player player;
            if (!CanPerformMove(playerGuid, out player)) return null;

            NoteResponseReceived(player.Id);

            var data = new Data
            {
                playerId = player.Id,
                gameFinished = false
            };

            if (player.PickUpPiece(this))
            {
                var playerPieces = _testedPieces.ContainsKey(player.Id) ? _testedPieces[player.Id] : null;
                var piece = new Piece
                {
                    id = player.Piece.id,
                    playerId = player.Piece.playerId,
                    playerIdSpecified = player.Piece.playerIdSpecified,
                    type = playerPieces?.Contains(player.Piece.id) == true ? player.Piece.type : PieceType.unknown
                };

                var field = Board.Fields[player.Location.y, player.Location.x] as TaskField;
                var pieceOnField = field.pieceIdSpecified ? new Piece
                {
                    id = field.pieceId,
                    playerIdSpecified = false,
                    type = playerPieces?.Contains(field.pieceId) == true ? Pieces[field.pieceId].type : PieceType.unknown
                } : null;

                var pieces = pieceOnField == null ? new[] { piece } : new[] { piece, pieceOnField };
                data.Pieces = pieces;
                UpdateNeighborPiece();
                return data;
            }
            data.PlayerLocation = player.Location;
            return data;
        }

        /// <summary>
        ///     Performs a given player's PlacePiece action.
        /// </summary>
        public Data PlayerPlacePiece(string playerGuid)
        {
            Log.Game.Debug($"GUID {playerGuid} requested to place a piece");
            Player player;
            if (!CanPerformMove(playerGuid, out player)) return null;

            NoteResponseReceived(player.Id);

            var data = new Data { playerId = player.Id };

            if (player.Piece != null)
            {
                player.PlacePiece(this);
                if (GameFinished)
                {
                    return null;
                }
                data.gameFinished = GameFinished;
                var playerGoalFields = _discoveredGoals.ContainsKey(player.Id) ? _discoveredGoals[player.Id] : null;

                if (player.Piece == null)
                {
                    var taskField = Board.Fields[player.Location.y, player.Location.x] as TaskField;
                    if (taskField != null)
                    {
                        data.TaskFields = new[] { taskField };
                    }
                    var goalField = Board.Fields[player.Location.y, player.Location.x] as GoalField;
                    if (goalField != null)
                    {
                        var gField = new GoalField(goalField, playerGoalFields?.Contains(player.Location) == true ? goalField.type : GoalFieldType.unknown);
                        data.GoalFields = new[] { gField };
                    }
                }
                else
                {
                    var playerPieces = _testedPieces.ContainsKey(player.Id) ? _testedPieces[player.Id] : null;
                    var piece = new Piece
                    {
                        id = player.Piece.id,
                        playerId = player.Piece.playerId,
                        playerIdSpecified = player.Piece.playerIdSpecified,
                        type = playerPieces?.Contains(player.Piece.id) == true ? player.Piece.type : PieceType.unknown
                    };
                    data.Pieces = new[] { piece };
                }
                return data;
            }
            data.gameFinished = false;
            data.PlayerLocation = player.Location;
            return data;
        }

        /// <summary>
        ///     Performs a given player's TestPiece action.
        /// </summary>
        public Data PlayerTestPiece(string playerGuid)
        {
            Log.Game.Debug($"GUID {playerGuid} requested to test a piece");
            Player player;
            if (!CanPerformMove(playerGuid, out player)) return null;

            NoteResponseReceived(player.Id);

            var data = new Data
            {
                playerId = player.Id,
                gameFinished = false
            };

            if (player.Piece != null)
            {
                var piece = new Piece
                {
                    id = player.Piece.id,
                    playerId = player.Piece.playerId,
                    playerIdSpecified = true,
                    type = player.Piece.type
                };
                Log.Game.Info($"Player {player.Id} tested a piece and got {piece.type} as a result");
                if (_testedPieces.ContainsKey(player.Id))
                {
                    _testedPieces[player.Id].Add(player.Piece.id);
                }
                var pieces = new[] { piece };
                data.Pieces = pieces;
                return data;
            }
            data.PlayerLocation = player.Location;
            return data;
        }

        /// <summary>
        /// Generates a response to a given player's AuthorizeKnowledgeExchange message.
        /// </summary>
        public Message PlayerAuthorizeKnowledgeExchange(string playerGuid, ulong targetId)
        {
            Log.Game.Debug($"GUID {playerGuid} requested a knowledge exchange");
            Player player;
            if (!CanPerformMove(playerGuid, out player)) return null;

            ulong sourceId = player.Id;

            Player target = GetPlayerById(targetId);
            if (target == null)
            {
                RejectKnowledgeExchange permReject = new RejectKnowledgeExchange();
                permReject.senderPlayerId = targetId;
                permReject.playerId = sourceId;
                permReject.permanent = true;
                return permReject;
            }

            if (_permissions.ContainsKey(targetId) && _permissions[targetId] == sourceId)
            {
                AcceptExchangeRequest acceptReq = new AcceptExchangeRequest();
                acceptReq.playerId = targetId;
                acceptReq.senderPlayerId = sourceId;

                //Merge players knowledge
                _discoveredGoals[targetId].UnionWith(_discoveredGoals[sourceId]);
                _discoveredGoals[sourceId].Clear();
                _discoveredGoals[sourceId].UnionWith(_discoveredGoals[targetId]);

                _testedPieces[targetId].UnionWith(_testedPieces[sourceId]);
                _testedPieces[sourceId].Clear();
                _testedPieces[sourceId].UnionWith(_testedPieces[targetId]);

                _permissions.Remove(targetId);

                return acceptReq;
            }

            if (_permissions.ContainsKey(player.Id))
            {
                _permissions.Remove(player.Id);
            }
            _permissions.Add(player.Id, targetId);
            KnowledgeExchangeRequest exReq = new KnowledgeExchangeRequest();
            exReq.senderPlayerId = sourceId;
            exReq.playerId = targetId;
            Task.Delay(TimeSpan.FromMilliseconds(2 * AuthorizeKnowledgeExchange.Delay)).ContinueWith((Task task) =>
            {
                if (_permissions.ContainsKey(player.Id) && _permissions[player.Id] == targetId)
                {
                    _permissions.Remove(player.Id);
                }
            });

            return exReq;
        }


        /// <summary>
        /// Removes disconnected player from game
        /// </summary>
        /// <param name="playerId">Id of player to be removed</param>
        public void RemovePlayer(ulong playerId)
        {
            foreach (var team in _players.Values)
            {
                if (team.RemoveAll(x => x.Id == playerId) > 0)
                {
                    return;
                }
            }
        }

        /// <summary>
        /// Marks specified goal location as uncovered by player 
        /// </summary>
        /// <param name="playerId">Id of a player who uncovered goal</param>
        /// <param name="location">Location of goal</param>
        public void AddDiscoveredGoal(ulong playerId, Location location)
        {
            if (_discoveredGoals.ContainsKey(playerId))
            {
                _discoveredGoals[playerId].Add(new Location { x = location.x, y = location.y });
            }
        }

        /// <summary>
        /// Starts measuring response time
        /// </summary>
        virtual public void NoteMessageSent(ulong id)
        {
            _lastMessageTimestamp[id] = DateTime.Now;
        }

        /// <summary>
        /// Stops measuring response time
        /// </summary>
        public void NoteResponseReceived(ulong id)
        {
            _responseTimes[id] += DateTime.Now - _lastMessageTimestamp[id];
            _moveCount[id]++;
        }
    }
}
