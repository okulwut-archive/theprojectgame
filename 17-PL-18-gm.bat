@echo off
set scriptPath=%~dp0
set "verbose="
:initial
if "%1"=="--port" goto portValue
if "%1"=="--address" goto addressValue
if "%1"=="--conf" goto confValue
if "%1"=="--verbose" goto verboseValue
if "%1"=="--game" goto gameValue
if "%1"=="" goto done
goto error

:portValue
shift
set port=%1
shift
goto initial

:addressValue
shift
set address=%1
shift
goto initial

:confValue
shift
set conf=%1
shift
goto initial

:gameValue
shift
set game=%1
shift
goto initial

:verboseValue
set verbose=true
shift
goto initial

:error
echo %0 usage error
echo 17-PL-18-gm --address [ip] --port [port] --conf [configuration file path] --verbose (optional)
goto eof

:done
set conf=%conf:"=%
(echo %conf%|findstr /R "^[A-Z]:.*$" || set conf=%scriptPath%%conf%)>NUL
start "GM %conf%" "%scriptPath%ProjectGame\ProjectGame.GameMaster\bin\Release\ProjectGame.GameMaster.exe" port=%port%^|host=%address%^|conf=%conf%^|verbose=%verbose%^|game=%game%

:eof