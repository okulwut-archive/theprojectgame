# The Project Game (Team 18 (2017-PL))

## Core project developers

* Jakub Czyżniejewski
* Bartłomiej Dach
* Dariusz Dulka
* Jacek Dziwulski (project lead)
* Jakub Mazurkiewicz

## Additional strategies developer

* Michał Okulewicz

## Specification
* [Game and communication protocol description](Documentation/TheProjectGameSystemSpec.pdf)

##Sample game setup
```
start 17-PL-18-cs --port 9000 --conf ProjectGame\CommunicationServerSettings.xml
timeout 3
start 17-PL-18-gm --port 9000 --address localhost --conf ProjectGame\GM2x6x10QuickBlueVsThoughtfulRed.xml
timeout 2
start 17-PL-18-pl --port 9000 --address localhost --conf ProjectGame\QuickPlayerSettings.xml --game "Gm1" --role leader --team red
timeout 1
start 17-PL-18-pl --port 9000 --address localhost --conf ProjectGame\QuickPlayerSettings.xml --game "Gm1" --role leader --team red
timeout 1
start 17-PL-18-pl --port 9000 --address localhost --conf ProjectGame\QuickPlayerSettings.xml --game "Gm1" --role leader --team red
timeout 1
start 17-PL-18-pl --port 9000 --address localhost --conf ProjectGame\ThoughtfulPlayerSettings.xml --game "Gm1" --role leader --team blue
timeout 1
start 17-PL-18-pl --port 9000 --address localhost --conf ProjectGame\ThoughtfulPlayerSettings.xml --game "Gm1" --role leader --team blue
timeout 1
start 17-PL-18-pl --port 9000 --address localhost --conf ProjectGame\ThoughtfulPlayerSettings.xml --game "Gm1" --role leader --team blue

```