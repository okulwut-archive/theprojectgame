# Project Game - Game phase
# Design document draft

## Requirements and goals

The goal of this phase is to implement a distributed information system, referred to colloquially as a *game*.
The *game data* is stored and modified on a master node -- here, the *game master*.
The data is consumed and analysed by slave nodes -- here, the *player*.
Synchronization is performed by the game master only, by timestamping responses with relevant data.
The child and master nodes are grouped into *games*.

### Features

* The system should be able to start and complete games.
* The server should be able to maintain multiple games.
* The game properties should be configurable on the game master.

## Design philosophy and description

### General

While in the previous phase some code was technical and therefore subject to a less rigorous object-oriented design,
this phase contains object relationships and dependencies, so care should be taken to make sure that the application
interface is well-defined and clean.
Testability should also be a primary focus for this phase.

### Establishing games

The process of game establishing is started by the *game master*, by registering a game on the server.
Upon registering, a `Game` object should be created on the server, and the *game master* that registered it should have
a reference to that object.

When a *player* requests to be assigned to a game created on the server, the *server* should also assign a reference
to the `Game` object to the player.

The `Game` object should contain a list of all players in the given game, but not expose it.
Convenience methods, such as `SendBroadcast()` should be provided instead.
While this introduces a reference cycle, it is more efficient due to the reduced lookup time.

When the player wants to join a game, a loop should be introduced.
As the game name is configured on the *player* instance, the player should regularly send `GetGames` messages to check
for game existence and then attempt to join upon finding their game.

### Game master

The `IGameMaster` interface should have methods for all the possible moves of the *players*.
They will act as a facade -- they should not contain game logic by themselves, but they should forward the call to
a proper class (possibly called `Game`, but the name selection is up to the implementor).

The *game master* should have a list of `Player`s, and use the instances of that class to send responses to messages.
Each `Player` has the same backing socket; the purpose of the instances is to use them as an abstraction and as a context.
That way, the `Player` instance can be passed to the `HandleMessage` method and aid in filling out player details,
such as the ID or GUID.

Pieces shall be dropped at random, and at the interval specified by the user.
The dropping of pieces will be managed by scheduling Quartz jobs.

When it comes to scheduling responses and enforcing desired delays, two possible implementations are suggested:

 *  Adding a `Delay` property to all message partial classes in the `Common` project, and then passing the delays,
    set at configuration-time by the game master, to a `SendMessage` routine in the game master implementation in
    the `Handle()` method.
 *  Adding a generic decorator for the received requests.
    The decorator will have a static factory method of signature `ResponseScheduler<T> Create(T request)`.
    The decorator instances shall have a `ScheduleResponse` method, that will generate the response from a given request
    and then schedule it for sending using Quartz.

The choice of solution is up to the implementor.

### Player moves

After joining a game, the method responsible for joining the game (e.g. `JoinPreferredGame`) should return an instance
of a `Board`.
The method should construct the `Board` instance such that the `CommunicationClient` is passed to the `Board` instance
via constructor or factory method.

To make actions on the board, the *player* should call methods of a `Board` class.
The `Board` class will contain methods for all the available moves at any given time.
Calling the methods should encapsulate the actual sending of the messages (calls to `CommunicationClient`).
In that way, implementation of the strategy will boil down just to making those calls.

In this phase, the simple request-response model assumed for the purposes of the communication phase will have to be
scrapped, in favour of a sender-listener model already present in the other components.
Sending will be "active", as in it will correspond to a function call, while receiving will be "passive" -- a separate
thread will be reading all incoming messages and handling them as they come.
This model is necessary for the implementation of broadcast messages as well as the knowledge exchange mechanism.

### Information exchange request

To implement the exchange request, players will have to listen for incoming `KnowledgeExchangeRequest`s
and `AcceptExchangeRequest`s, and be able to follow them up with a direct response with a `Data` message.
To that end, the `Board` class should be able to create a `Board` message which is ready for serialization.

The following is an outline of the exchange request routine, assuming delays of 1200ms:

#### Request accepted

 *  (T = 0ms) Player #1 sends a `AuthorizeKnowledgeExchange` message.
    The game master receives it through the server, and delays sending a `KnowledgeExchangeRequest` to player #2
    for 1200ms.
 *  (T = 1200ms) Player #2 receives the forwarded request.
    They immediately and directly send their board state to Player #1, and follow it up with another
    `AuthorizeKnowledgeExchange`, and the response -- an `AcceptExchangeRequest` -- is delayed again by the game master.
 *  (T = 2400ms) Player #1 receives a delayed `AcceptExchangeRequest`, and responds immediately and directly
    to Player #2.

This way, both parties receive the other party's data within 1200ms of initiating/learning of the exchange request.

#### Request denied

The `AuthorizeKnowledgeExchange` message sent by Player #1 is delayed by the game master and then met with a
`RejectKnowledgeExchange` response from Player #1, which is forwarded immediately and directly without delay.
The penalty paid by the first player is equal to one delay -- by default, 1200ms.
