# Project Game - Communication phase
# Design document draft

## Requirements and goals

The goal of this phase is to implement a system of communication between many distributed nodes, referred to as *Players* and *Game Masters*, joined by a common
core component called the *Communications Server*.

### Features

* All communication passes through Communication Server and uses TCP sockets.
* All the components should be able to run a in a text-only mode.
* Verbose mode and logging should be implemented in all components.
* Messages should be passed between nodes, in accordance to the messaging protocol detailed
  [in the official project specification](https://se2.mini.pw.edu.pl/17-results/17-results/blob/master/Definition/TheProjectGameSystemSpec.pdf).
  They do not need to make sense, but the kinds and order of messages should be preserved for each message exchange flow.
  All components should generate fake data to respond with.
* Gameplay is __not__ a part of this phase. Games do not need to be established and played.

### Performance

* Communication Server and Game Master should be able to serve smoothly at least 64 players.

### Technical settings

* The interval between keep-alive bytes should be a parameter in all components.

### Game settings

* Intervals between requests and responses should be configured and enforced by Game Masters.

### Run scripts

* The application should be able to start using parameters described in the aforementioned specification document.

## Design philosophy and description

### General

As the whole system should not consist of more than around 70 concurrently running components, performance should be acceptable regardless of selected
philosophy. However, for maximum efficiency, the following model is assumed (at least for now):

* _Players_ are single-threaded applications. Sending a request does not block the Player. Received responses are only logged, unless further action is required.
* _Game Masters_ are multi-threaded applications, with two or three threads of execution. One thread is responsible for communication, one for deserialization 
  and response generation, and the other two will be used for scheduling responses.
* _Communication Server_ is a multi-threaded application. `select` or `pselect` will be used on one thread to query the currently open sockets.
  The read data shall be forwarded (or discarded, if they are keep-alive packets). The other threads are used for keep-alive and scheduling
  (e.g. closing sockets upon reaching a time-out).

Message classes should be auto-generated from 
[the supplied xsd](https://se2.mini.pw.edu.pl/17-results/17-results/blob/master/Definition/SchemaTests/SchemaTests/SchemaTests/TheProjectGameCommunication.xsd)
and added to a common `Core` project. Each component should extend these classes and add their desired behaviour (e.g. preparing a response) as methods.
This is an implementation of the command design pattern.

#### Player

The following sequence diagram represents the projected control flow in the Player node.

![Player control flow sequence diagram](svg/player_sequence.svg)

* The preferences to be read by the `PreferenceReader` class are as follows:
  * Communication server host and port, used by `CommunicationClient`
  * Game name, team colour, preferred role (unused, only logged at this stage)
  * A setting for the `MockRequestProvider` that determines the way requests should be generated - one of the following:
    * __random__ - the type of request made next is randomly selected.
    * __sequential__ - all the available types of requests are made sequentially in a loop. This is used for testing purposes.

#### Game Master

An incoming request should be handled as follows:

1. The communication thread maintains two queues: one for incoming data and one for outgoing data.
   When a request arrives, it is enqueued on to the incoming data queue.
2. The deserialization thread converts the string content of the packet into a message object. The message object is passed to a `MockResponseGenerator`
   object.
3. After a mock response is generated, it is passed to the scheduler thread, to get a response delay.
4. Once the delay completes, the scheduler thread places the message, as a string, in the communication thread's outgoing queue.
   The communication thread cycles between reading into the incoming queue and writing using the outgoing queue.
   Reading is only performed if the outgoing queue is empty.

#### Communication Server

The Communication Server is a key part of the system, and as such the order of handling messages is extremely important. Below is the outline of the
message handling routine:

* The messages are handled in a loop.
* The messages are handled in the following sequence:
	* Responses sent in by the Game Masters.
	* Requests sent in by the Players.
* In the future, requests of active Game Masters and Players (who are in games that are in progress) shall be prioritised over the requests of
  Game Masters registering games and Players querying for available games.

The server should not maintain raw sockets. Sockets should be wrapped in objects; these objects should expose:

* the socket,
* the agent type,
* the agent priority.

The above objects should be placed in a priority queue. This priority queue should be able to return a list of sockets and a `FD_SET` for convenience purposes.

Multiple game handling may or may not be developed within the scope of this stage.

#### Generating mock data

Players have a `MockRequestProvider` class, that is responsible for generating mock requests. For ease of extensibility, the class should contain a collection of
types implementing `IRequestGenerator`. Each class implementing this interface will generate only one type of message.

![Class diagram for MockRequestProvider](svg/mock_request_generator.svg)

A similar mechanism can be used in the Game Master to provide responses, with the exception that the `IGenerator` interface will have to be expanded, adding the
type of request a given `IGenerator` instance can respond to. (for example, a `RequestType` property that returns a `Type` object)

### Libraries

The suggested libraries to use for this stage are:

* [log4net](https://logging.apache.org/log4net/) for logging and verbose mode,
* [Quartz.net](https://www.quartz-scheduler.net/) or [hangfire.](http://hangfire.io/) for time-based scheduling.
