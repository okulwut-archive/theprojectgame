# Software Engineering 2 - Initiation phase documentation

## Group members

 * Jakub Czyżniejewski (tester)
 * Bartłomiej Dach (system architect)
 * Dariusz Dulka  (system architect)
 * Jacek Dziwulski (leader)
 * Jakub Mazurkiewicz (developer)
 
## Roles and responsibilities

  * System architects - Architects provide some kind of description of the system in each phase. They define what needs to be done and how it should be done.        Developers and tester have to make sure, that their work is compliant with architect's design. Due to the small size of our team, they also work on smaller             programming tasks.
  * Leader - organizes work of the whole team. It is his responsibility to create tasks, and assign them to team members. Leader has to make sure, that all projects are     finished before deadlines. In case of some misunderstandings, he is also responsible for interaction with client. If developers have any problems, leader also          should be the first one to help. In addition to that, he works on "medium-sized" (more difficult, but not most time-consuming of all) tasks.
  * Tester - responsible of testing the whole code, that includes unit and integration tests. During development phase, he is responsible for doing code-review of all      finished tasks, to ensure quality of our product.
  * Developer - works on most time-consuming tasks. He does not have to be involved in planning or testing phases.


## Specification - errors and ambiguities

While reviewing the documentation, our group has reported the following issues and questions:

 * [Picking up pieces missing in list of possible moves (#24)](https://se2.mini.pw.edu.pl/17-results/17-results/issues/24)
 * [Board state to be displayed by players (#25)](https://se2.mini.pw.edu.pl/17-results/17-results/issues/25)
 * [Calculating cost of information exchange (#26)](https://se2.mini.pw.edu.pl/17-results/17-results/issues/26)
 * [Schema issues (#27)](https://se2.mini.pw.edu.pl/17-results/17-results/issues/27)
 * [Additional validation (#28)](https://se2.mini.pw.edu.pl/17-results/17-results/issues/28)
 * [Communication request refusal (#29)](https://se2.mini.pw.edu.pl/17-results/17-results/issues/29)
 * [Allowing use of TCP keep alive (#30)](https://se2.mini.pw.edu.pl/17-results/17-results/issues/30)
 * [Result of checking a sham piece (#31)](https://se2.mini.pw.edu.pl/17-results/17-results/issues/31)

Additionally, we agree with the following key issues reported by other teams:

 * [What happens when a node disconnects? (#1)](https://se2.mini.pw.edu.pl/17-results/17-results/issues/1)

   The existing documentation is severely lacking when it comes to any sort of failure scenario. For there to be any chance of cooperation
   between teams' agents in case of errors, the specification should contain at least the desired behaviour of the agents.

 * [Timestamp missing timezone field (#10)](https://se2.mini.pw.edu.pl/17-results/17-results/issues/10)

   While the lack of timezone in the timestamps is unlikely to cause problems, as the only agent that performs timestamping in each game
   is the game master, marking the timestamps as UTC resolves any potential daylight savings' time issues, and simultaneously does not
   radically inflate the size of messages.

 * [Knowledge exchange (#11)](https://se2.mini.pw.edu.pl/17-results/17-results/issues/11)

   In Software Engineering 1 meetings, it was made clear that communication between players should bypass the game master entirely,
   which is not the case in the new version. We would like to clarify whether or not the requirements have changed.

 * [Subsequent messages before a reply from game master arrives (#12)](https://se2.mini.pw.edu.pl/17-results/17-results/issues/22)

   It is important to specify whether the request-response exchange on the Player's side is blocking or not, as both approaches are
   significantly different and therefore will be key to make design changes along the way.

 * [GameFinished is never used (#77)](https://se2.mini.pw.edu.pl/17-results/17-results/issues/77)

 * [PlayerID is not unique, PlayerMessage requires gameID to be delivered (#79)](https://se2.mini.pw.edu.pl/17-results/17-results/issues/79)

   If player IDs aren't globally unique (i.e. they're only unique within the scope of a single game), a game ID is also required in the messages
   for the Communication Server to properly address them.

 * [Coordinate orientation not specified (#81)](https://se2.mini.pw.edu.pl/17-results/17-results/issues/81)

   Clarifying the coordinate orientation will ensure interoperability with other teams' agents.

## Software development methodology

Due to the fact that the entire project is already mostly planned out through the semester and deadlines have been defined,
 we will be using an iterative waterfall approach.

 * The stages of the project are as outlined in [the following presentation](http://mini.pw.edu.pl/~okulewiczm/downloads/io2/SE2_IntroductionAndRules.pdf),
   as they seem to be natural and reasonable milestones. Each stage is approximately 3 weeks long.
 * At the beginning of each stage, the project members list the tasks for the particular stage, which should ideally be completed by week 2 of the stage
   (the testing labs). Afterwards, the project leader assigns the tasks to each member.
 * After the first and second week, a team meeting is called to ascertain the progress of all assigned tasks. 
 * The third week of the stage is devoted to finishing up any tasks that didn't meet the second week deadline and fixing bugs that have arisen during
   the testing labs.
 * Due to the fact that our group does not have experience developing such systems, there will be no formal specification at the start of each phase.
   Some guidelines regarding implementations might be agreed upon in the phase start meeting, but they are not absolute and might be subject to change
   in case of difficulty.

### Technical details

 * The Git repository will use the `git-flow` branching model, with a protected release-only `master` branch and the working `develop` branch.
 * Releases will be pushed to the `master` branch upon completion of every project phase.
 * There will be two types of issues:
    * feature requests, with branches named `feature/[issue-number]`,
    * bugfixes, with branches named `bugfix/[issue-number]`.
 * All changes on feature and bugfix branches will be merged by pull requests, with mandatory code reviews for each pull request.
   A minimum of 2 approvals are required to merge in the branch.
 * All code should contain tests, that check whether the specification requirements are met. Code without tests will not be merged to `develop`.
 * All tests must be passing before merging in a `feature` or `bugfix` branch.

## Software technology and version

The project will be developed using the .NET Framework platform, using the 4.5.2 version; C# will be the language used.

## Project meeting presentation

### Phase start meeting

This meeting takes place in the first week of a phase.

1. Team members list the issues to be completed during the specified phase.
2. The team leader estimates the time requirement of each task, splits them into multiple ones if necessary, and assigns them
   to particular members.
3. The team members formulate comments and raise concerns about the assignments made by the leader.
4. The leader considers the above requests and adjusts the schedule or rejects the comments.

### Progress update meetings

This meeting takes place every week of the phase after the phase start.

1. The team leader requests progress reports from all team members. Developers should report: how far alog their tasks are and
   what problems and complications have arisen during task completion.
2. In the case of complications the leader can split tasks into multiple, smaller ones or reassign them to another member.

## Project schedule

### Tasks for first stage (11.03-31.03)

* Autogenerating message classes from provided XML Schema
* Implementation of sending messages (with no delay) and receiving for the following components:
  * Game master,
  * Player,
  * Communications server.
* Implementation of verbose mode for all components
* Adding delayed responses to Game Master,
* Forwarding messages by Communications Server.
* Mocking player's actions
* Adding meaningful responses to received messages by:
  * Game master,
  * Player.
* Integration tests (passing messages between components)
* Create bash/Powershell script starting all components

### Tasks for second stage (01.04-28.04)

* Allowing players to join selected game
* Implementation of game sequence, for example: starting and ending a game
* Implementation of player's actions and basic player's strategy
* Implementation of player's actions effects in Game Master
* Allowing server to maintain multiple games
* Creating XML file with game properties
* Integration tests

### Tasks for third stage (29.04-02.06)

* Confirming full compliance with specification
* Testing our components with other groups

### Tasks for fourth stage

* Implementation of efficient player's strategy

### Personal schedules

Personal schedules will be maintained and updated using GitLab.

* The list of tasks to be done will appear on the [Issue Tracker](https://se2.mini.pw.edu.pl/17-pl-18/17-pl-18/issues).
* Personal schedules will be indicated by task assignments and issue due dates.
* Current work progress will be available via the [Issue Board](https://se2.mini.pw.edu.pl/17-pl-18/17-pl-18/boards).

The following table contains preliminary task assignments and deadlines for the first phase for each team member. Both
may change over time; the updated versions will appear on the links mentioned above.

Team member         | Issue name                                                                                                           | Deadline
--------------------|----------------------------------------------------------------------------------------------------------------------|-----------
Jakub Czyżniejewski | [Integration tests - stage 1 (#8)](https://se2.mini.pw.edu.pl/17-pl-18/17-pl-18/issues/8)                            | Mar 26
Bartłomiej Dach     | [Mocking player actions (#6)](https://se2.mini.pw.edu.pl/17-pl-18/17-pl-18/issues/6)                                 | Mar 22
                    | [Forwarding messages by Communication Server (#5)](https://se2.mini.pw.edu.pl/17-pl-18/17-pl-18/issues/5)            | Mar 21
Dariusz Dulka       | [Verbose mode and logging (#3)](https://se2.mini.pw.edu.pl/17-pl-18/17-pl-18/issues/3)                               | Mar 24
Jacek Dziwulski     | [Adding responses to messages (#7)](https://se2.mini.pw.edu.pl/17-pl-18/17-pl-18/issues/7)                           | Mar 22
                    | [Implementation of response delay on Game Master's side (#4)](https://se2.mini.pw.edu.pl/17-pl-18/17-pl-18/issues/4) | Mar 23
                    | [Initialization scripts (#9)](https://se2.mini.pw.edu.pl/17-pl-18/17-pl-18/issues/9)                                 | Mar 24
Jakub Mazurkiewicz  | [Generating message classes from XML Schema (#1)](https://se2.mini.pw.edu.pl/17-pl-18/17-pl-18/issues/1)             | Mar 20
                    | [Sending and receiving messages (#2)](https://se2.mini.pw.edu.pl/17-pl-18/17-pl-18/issues/2)                         | Mar 21

The deadlines were selected so that no task is impossible to complete because of missing prerequisite tasks (if the previous deadlines are met).
At this early a stage of development, it was impossible to decouple the tasks completely. The workload should be roughly split evenly throughout
the whole stage.