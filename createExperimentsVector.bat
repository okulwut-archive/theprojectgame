@echo off
setlocal EnableDelayedExpansion
set n=0
for %%a in (qoct qt qoct) do (
   set advanced[!n!]=%%a
   set /A n+=1
)

set n=0
for %%a in (Thoughtful QuickTesting Thoughtful) do (
   set advancednames[!n!]=%%a
   set /A n+=1
)

set n=0
for %%a in (q q qoc) do (
   set base[!n!]=%%a
   set /A n+=1
)

set n=0
for %%a in (Quick Quick QuickOnlyCommunication) do (
   set basenames[!n!]=%%a
   set /A n+=1
)

set n=0
for %%a in (no low med) do (
   set experiments[!n!]=%%a
   set /A n+=1
)

set n=0
for %%a in (No Low Med) do (
   set experimentsbig[!n!]=%%a
   set /A n+=1
)

set n=9100
(for /L %%j in (0,1,2) do (
	(for /L %%i in (0,1,2) do (
		echo start 17-PL-18-cs --port !n! --conf ProjectGame\CommunicationServerSettings.xml
		echo timeout 5
		echo start 17-PL-18-gm --port !n! --address localhost --conf ProjectGame\GM6x6x6!experimentsbig[%%j]!RiskRB.xml --game "Gm6x6x6 !experiments[%%j]! risk rb r!advanced[%%i]!b!base[%%i]!"
		echo timeout 5
		(for /L %%k in (0,1,2) do (
			echo start 17-PL-18-pl --port !n! --address localhost --conf ProjectGame\!advancednames[%%i]!PlayerSettings.xml --game "Gm6x6x6 !experiments[%%j]! risk rb r!advanced[%%i]!b!base[%%i]!" --team red --role leader
			echo timeout 3
			echo start 17-PL-18-pl --port !n! --address localhost --conf ProjectGame\!basenames[%%i]!PlayerSettings.xml --game "Gm6x6x6 !experiments[%%j]! risk rb r!advanced[%%i]!b!base[%%i]!" --team blue --role leader
			echo timeout 3
			))
		echo start 17-PL-18-gm --port !n! --address localhost --conf ProjectGame\GM6x6x6!experimentsbig[%%j]!RiskRB.xml --game "Gm6x6x6 !experiments[%%j]! risk br b!advanced[%%i]!r!base[%%i]!"
		echo timeout 5
		(for /L %%k in (0,1,2) do (
			echo start 17-PL-18-pl --port !n! --address localhost --conf ProjectGame\!advancednames[%%i]!PlayerSettings.xml --game "Gm6x6x6 !experiments[%%j]! risk br b!advanced[%%i]!r!base[%%i]!" --team blue --role leader
			echo timeout 3
			echo start 17-PL-18-pl --port !n! --address localhost --conf ProjectGame\!basenames[%%i]!PlayerSettings.xml --game "Gm6x6x6 !experiments[%%j]! risk br b!advanced[%%i]!r!base[%%i]!" --team red --role leader
			echo timeout 3
			))
		set /A n+=1
	) > experimentFinalMissing!experimentsbig[%%j]!Risk_!advancednames[%%i]!_!basenames[%%i]!.cmd
	) 
))